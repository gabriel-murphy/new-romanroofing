
//LightSlider
$("#lightSlider").lightSlider({
    vertical:true,
    gallery:true,
    item:1,
    vThumbWidth: 200,
    thumbItem: 6,
    galleryMargin: 10,
    thumbMargin: 10,
    responsive:[
        {
            breakpoint:768,
            settings: {
                verticalHeight:270,
                thumbItem: 5,
                vThumbWidth: 100,
            }
        }
    ]
});
//LightSlider End
