$( document ).ready(function() {

    $('.banner-carousel.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        autoplay:1000,
        smartSpeed:1000,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.testimonials-carousel.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        autoplay:1000,
        smartSpeed:1000,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.awards-carousel.owl-carousel').owlCarousel({
        loop:true,
        margin:70,
        autoplay:1000,
        smartSpeed:1000,
        nav:true,
        responsive:{
            0:{
                items:3
            },
            768:{
                items:4
            },
            1000:{
                items:7
            }
        }
    });

    $('.news-carousel.owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        slideBy:2,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:2
            }
        }
    });

    $('.team-carousel.owl-carousel').owlCarousel({
        loop:true,
        margin:15,
        autoplay:1000,
        smartSpeed:1000,
        slideBy:4,
        stagePadding:5,
        nav:true,
        navText:["<i class=\"fal fa-long-arrow-left\"></i>","<i class=\"fal fa-long-arrow-right\"></i>"],
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });

    $('.city-reviews-carousel.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        smartSpeed:1000,
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })


    $('.stat-count').counterUp({
        delay: 20,
        time: 2000
    });



    $(window).on('scroll', function(){

        if(window.location.pathname == '/'){
            var secondSection = $(".about-us").position();
            secondSection = Math.ceil(secondSection.top);
            var docPos = $(document).scrollTop();

            if(docPos > secondSection){
                $('#js_menu').slideDown("slow");
                $('.messenger-link').css("opacity","1");
            }

            else if(docPos < secondSection){
                $('#js_menu').slideUp("slow");
                $('.messenger-link').css("opacity","0");
            }
        }

        else{
            var docPos = $(document).scrollTop();
            var topBarHeight = $('.top-bar').outerHeight();
            if(docPos > topBarHeight){
                $('.main-menu-secondary').css("top","0");
                $('.main-menu-secondary').addClass("fixed-top");
            }
            else if(docPos < topBarHeight){
                $('.main-menu-secondary').css("top","34px");
                $('.main-menu-secondary').removeClass("fixed-top");
            }

            if ( $('.main-menu-secondary').hasClass("fixed-top")){
                $('main').css("padding-top","99px");
            }

            else{
                $('main').css("padding-top","0");
            }
        }

    });

    //Make the whole list Clickable

            $('.main-menu .navbar ul li, .main-menu-secondary .navbar ul li').on('click', function () {
                window.location = $('a', this).attr('href');
            });

    //Make the whole list Clickable End

    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();
    //Tooltion End


    //Jobs Page JS

    var applyBtn="";

    $(this).find('#job_card .apply-now-btn').on('click', function () {
        if($('#job_card div.collapse').hasClass('collapse')){
            $(this).closest('#job_card .apply-now-btn.collapsed').addClass('d-none')
        }
        applyBtn = $(this).attr('id');
        applyBtn = "#"+applyBtn;
    });

    $(this).find('#job_card .submit-job-form').on('click', function () {
        $('#job_card .apply-now-btn').removeClass('d-none');
        $(this).find('#job_card .apply-now-btn').html('Applied');
        $('#job_card .collapse').removeClass('show');
        $(applyBtn).html('Applied<i class="fal fa-check-circle ml-2"></i>').addClass('disabled');
    });

    $(this).find('#job_card .cancel-job-form').on('click', function () {
        $('#job_card .apply-now-btn').removeClass('d-none');
        $('#job_card .collapse').removeClass('show');
    });

    //Jobs Page JS End

    //Job Form Submission

    $(this).find('.submit-job-form').on('click', function () {
       var jobForm = "#"+($(this).parent('form').attr('id'));
        $(jobForm).submit(function (ev) {
            ev.preventDefault();
            var form = $(this);
            var url = baseUrl + "/jobs/add";

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function (data) {
                    console.log(data)
                },
                error: function (err) {
                    console.log(err)
                }
            })
        });
    });


    //Job Form Submission End


    //Animated Hamburger

    $('#animated_hamburger').on('click', function () {
        $('#animated_hamburger').toggleClass('is-active','collapsed');
    });
    $('#animated_hamburger-fixed').on('click', function () {
        $('#animated_hamburger-fixed').toggleClass('is-active','collapsed');
    });

    //Animated Hamburger End


});


