// $(document).ready(function () {
    function dropIcon(title,lat,lng){
        var markers = [
            {
                "title":title,
                "lat": lat,
                "lng": lng,
                "description": 'Verify the marker, is at the right property',
            }
        ];
        var initMap = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), initMap);

        var data = markers[0]
        var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        var icon = {
            url: mapIcon, // url
            scaledSize: new google.maps.Size(45, 45), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: icon,
            title: data.title,
            animation: google.maps.Animation.DROP
        });
        (function (marker, data) {
            google.maps.event.addListener(marker, "mouseover", function (e) {
                infoWindow.setContent(data.description);
                infoWindow.open(map, marker);
            });
        })(marker, data);


    }


// });
