$(document).ready(function() {
    var totalStepCount = $('.step-nav-tabs').children().length;
    var progressBarWidth = 100/totalStepCount;
    var dynamicProgressBarWidth = progressBarWidth;
    console.log(dynamicProgressBarWidth)
    var progressBar = $('.progress-bar');
    progressBar.css('width', progressBarWidth+'%')

    $('.btnNext').click(function() {
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
        dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
        progressBar.css('width', dynamicProgressBarWidth+'%')
    });

    $('.btnPrevious').click(function() {
        $('.nav-tabs .active').parent().prev('li').find('a').trigger('click');
        dynamicProgressBarWidth = dynamicProgressBarWidth - progressBarWidth;
        console.log(dynamicProgressBarWidth)
        progressBar.css('width', dynamicProgressBarWidth+'%');
    });
});

