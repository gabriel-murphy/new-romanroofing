$(document).ready(function(){

    var totalStepCount = $('.step-nav-tabs').children().length;
    var progressBarWidth = 100/totalStepCount;
    var dynamicProgressBarWidth = progressBarWidth;
    var progressBar = $('.progress-bar');
    progressBar.css('width', progressBarWidth+'%')

    $('.btnPrevious').click(function() {
        $('.nav-tabs .active').parent().prev('li').find('a').trigger('click');
        dynamicProgressBarWidth = dynamicProgressBarWidth - progressBarWidth;
        progressBar.css('width', dynamicProgressBarWidth+'%');
    });

    $("#step_1_next").click(function(){
        let form = $("#evaluation_form_1");
        form.validate({
            rules: {
                address: {
                    required: true,
                },
                phone: {
                    required: true,
                }, extension: {
                    required: true,
                },
            },
            messages: {
                address: {
                    required: "Address field is required",
                },
                phone: {
                    required: "Phone number is requried",
                },
                extension: {
                    required: "Extension is required",
                },
            },
            errorPlacement: function(error, element) {
                console.log()
                //Custom position: first name
                if (element.attr("name") == "address" ) {
                    $("#address_error").html(error[0]);
                }
                //Custom position: second name
                else if (element.attr("name") == "phone" ) {
                    $("#phone_error").html(error[0]);
                }

                else if (element.attr("name") == "extension" ) {
                    $("#extension_error").html(error[0]);
                }
                // Default position: if no match is met (other fields)
                else {
                    error.append($('.errorTxt span'));
                }
            }
        });
        if (form.valid() == true){
            $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
            dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
            progressBar.css('width', dynamicProgressBarWidth+'%')
        }
    });
    $("#step_2_next").click(function(){
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
        dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
        progressBar.css('width', dynamicProgressBarWidth+'%')

    });

    $("#step_3_next").click(function(){
        $("#evaluation_form_3").validate({
            rules: {
                'structure[needing_evaluation][]': {
                    required: true,

                }
            } ,
            messages: {
                'structure[needing_evaluation][]': {
                    required: "You must select at least one",
                }
            },
            errorPlacement: function(error, element) {
                console.log()
                //Custom position: first name
                if (element.attr("name") == "structure[needing_evaluation][]" ) {
                    $("#structure_error").html(error[0]);
                }
                else {
                    error.append($('.errorTxt span'));
                }
            }
        });

        if ($("#evaluation_form_3").valid){
            $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
            dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
            progressBar.css('width', dynamicProgressBarWidth+'%')
        }
    });

    $("#step_4_next").click(function(){


        let form = $("#evaluation_form_4");
        form.validate({
            rules: {
                first_name: {
                    required: true,

                },
                last_name: {
                    required: true,

                },
                email: {
                    required: true,

                },
            } ,
            messages: {
                first_name: {
                    required: "First name is required",
                },
                last_name: {
                    required: "Last name is required",
                },
                email: {
                    required: "Email is required",
                },
            },
            errorPlacement: function(error, element) {
                console.log(error[0])
                //Custom position: first name
                if (element.attr("name") == "first_name" ) {
                    $("#first_name_error").html(error[0]);
                }
                //Custom position: second name
                else if (element.attr("name") == "last_name" ) {
                    $("#last_name_error").html(error[0]);
                }

                else if (element.attr("name") == "email" ) {
                    $("#email_error").html(error[0]);
                }
                // Default position: if no match is met (other fields)
                else {
                    error.append($('.errorTxt span'));
                }
            },
        });
        if (form.valid()==true){
            $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
            dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
            progressBar.css('width', dynamicProgressBarWidth+'%')
        }
    });

    $("#step_5_next").click(function(){
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
        dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
        progressBar.css('width', dynamicProgressBarWidth+'%')

    });

    $("#step_6_next").click(function(){
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
        dynamicProgressBarWidth = dynamicProgressBarWidth + progressBarWidth;
        progressBar.css('width', dynamicProgressBarWidth+'%')

    });
    $("#finish_evaluation").click(function(e){
        e.preventDefault();
        e.stopPropagation();


        let active_leak = 0;
        let include_solar_checkbox = 0;
        if($('#active_leak').is(':checked')){
            let active_leak = $('#active_leak').val();
        }
        if($('#include_solar_checkbox').is(':checked')){
            let include_solar_checkbox = $('#include_solar_checkbox :checked').val();
        }
        let structure_needing_evalaution = [];
        $('input.needing_evaulation:checkbox:checked').each(function () {
            structure_needing_evalaution.push($(this).val());
        });
        let roofing_service = [];
        $('input.roofing_service:checkbox:checked').each(function () {
            roofing_service.push($(this).val());
        });
        let materials_interest = [];
        $('input.materials_interest:checkbox:checked').each(function () {
            materials_interest.push($(this).val());
        });
        let related_services = [];
        $('input.related_services:checkbox:checked').each(function () {
            related_services.push($(this).val());
        });

        let active_leak_type = null;
        let radioValue = $("input[name='active-leak']:checked").val();
        if(radioValue){
            active_leak_type  = $("input[name='active-leak']:checked").val();
        }

        $.ajax({
            type: 'POST',
            url: "/evaluation",
            headers: {'X-CSRF-TOKEN': token },
            dataType: 'JSON',
            data:{
                address:$('#address').val(),
                formatted_address: $('#formatted_address').val(),
                city: $('#city').val(),
                state: $('#state').val(),
                county: $('#county').val(),
                country: $('#country').val(),
                zip_code: $('#zip_code').val(),
                street: $('#street').val(),
                latitude: $('#latitude').val(),
                longitude: $('#longitude').val(),
                country_code: $('#country_code').val(),
                phone: $('#phone').val(),
                extension: $('#extension').val(),
                active_leak: active_leak,
                include_solar_checkbox: include_solar_checkbox,
                structure_needing_evalaution: structure_needing_evalaution,
                active_leak_type: active_leak_type,
                first_name: $('#first_name').val(),
                last_name: $('#last_name').val(),
                email: $('#email').val(),
                roofing_service: roofing_service,
                related_services: related_services,
                materials_interest: materials_interest,
            }
        }).then(function (data) {
            console.log('success');
            location.reload();
        });


    });

});

