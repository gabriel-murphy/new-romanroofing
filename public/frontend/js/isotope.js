/*Isotope*/

$('.grid').isotope();
$('.filter-list-menu button').on('click', function () {
    $value = $(this).attr('data-name');
    $('.grid').isotope({
        // options
        itemSelector: '.grid-item',
        layoutMode: 'fitRows',
        filter: $value
    });

});

$(".filter-list-menu li button").click(function () {
    $(".filter-list-menu li button").removeClass("active");
    $(this).addClass("active");
});


/*Isotope End*/
