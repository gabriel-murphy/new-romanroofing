@extends('layout')
@section('title','Jobs @ Roman Roofing')
@section('content')
    <body id="about" class="inner">
@extends('navigation')
	  <div id="pageArea">
            <section id="showcase" class="showcase-sm">
                <div class="content">
                    <h1>Employment @ Roman</h1>
                </div>
            </section>
            <section id="employment" class="container">
                <div class="row">
                    <div class="col-md-9">
                        <article class="job">
                            <h3>New Construction Estimator</h3>
                            <p class="text-muted">Posted:  August 28, 2019</p>
                            <p>Roman Roofing seeks an experienced roofing professional with experience in reading and interpreting blueprints of major construction
                            projects along with considerable cost estimating of complex commercial roofing projects.</p>
                            <div class="collapse" id="job1">
                                <p>The ideal candidate will maintain excellent communication skills in representing the Roman brand to builders and key vendors, which will be the point-of-contact for major commercial estimations which the selected candidate will endevor. &nbsp;The perfect candidate will maintain moderate to good computer skills with experience with takeoff software including Active Takeoff and references from previous employers. &nbsp;A valid drivers license and dependable transportation, along with the aformentioned job experience, is required for this position.</p>
                                <p>Interested in applying to this position?  Please submit your application by clicking the button below.</p>
                            </div>
                            <a href="#job1" data-toggle="collapse"  class="btn btn-outline-secondary">Read full description.</a>
                        </article>                      
                        </div>
                        <div class="col-md-3">
                            <h5>Summary:</h5>
                            <ul class="list-group">
                              <li class="list-group-item">
                                Hours: 8-5
                              </li>
                              <li class="list-group-item">
                                Salary with experience
                              </li>
                            </ul>
                            <a class="btn btn-danger" href="mailto:help@roman-roofing.com">Email to apply</a>
                            <br><br>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-9">
                        <article class="article">
                            <h3>Scheduling Coordinators</h3>
                            <p class="text-muted">August 22, 2019</p>
                            <p>Roman Roofing has immediate openings for full-time Scheduling Coordinators to join our growing family of professionals. &nbsp;As those who work at Roman know, we are not your typical roofing company. &nbsp;Aside from a family-oriented culture and relaxed dress policy, you will benefit from an in-place commission structure for just doing your job – we believe in sharing our success! </p>
                            <div class="collapse" id="job2">
                                <p>More importantly, we provide continuing educational assistance in a vibrant and fun atmosphere.  Selected candidates will possess the following characteristics:
                                <ul class="list-group list-group-flush">
                                  <li class="list-group-item">Charismatic on the telephone as an advocate for our craft with a strong command of the English language in both spoken and written form</li>
                                  <li class="list-group-item">Vastly articulate, confident, with a natural ability to think critically and independently while also multi-tasking with ease</li>
                                  <li class="list-group-item">Fluency in Spanish in written and spoken form is ideal, but not required</li>
                                  <li class="list-group-item">Ability to interpret web-based calendars to schedule the proper estimator to meet with the homeowner at the parties’ availability and the ability to easily handle data entry of customer information into web-based forms</li>
                                  <li class="list-group-item">Basic to moderate understanding of Microsoft Word and Excel</li>
                                  <li class="list-group-item">Strong desire to learn the basics of our trade and highly career oriented.</li>
                                  </ul>
Our Scheduling Coordinators are groomed for career advancement and report directly to our Chief Financial Officer, Ms. Lindsey Dopfer.   Selected candidates will also enjoy and benefit from ongoing training services via Roman University in disciplines including digital marketing and managerial accounting.
                                </p>
                            </div>
                            <a href="#job2" data-toggle="collapse"  class="btn btn-outline-secondary">Read full description.</a>
                        </article>                      
                        </div>
                        <div class="col-md-3">
                            <h5>Summary:</h5>
                            <ul class="list-group">
                              <li class="list-group-item">
                                Hours: 8-5 
                              </li>
                              <li class="list-group-item">
                                40 hours per week
                              </li>
                            </ul>
                            <a class="btn btn-danger" href="mailto:help@roman-roofing.com">Email to apply</a>
                            <br><br>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-9">
                        <article class="article">
                            <h3>Commercial Roofing Crew & Labors Needed</h3>
                            <p class="text-muted">April 27, 2019</p>
                            <p>Roman Roofing - recently named Southwest Florida's fastest growing roofing contractor - has immediate openings for a commercial roofing crew, which consists of four (4) team members to form a crew to join the dozens of crews and 100+ hard-working team members in the Roman Roofing family.</p>
                            <div class="collapse" id="news3">
                              <p>Wimps need not apply - we are looking for individuals or an entire crew of up to four (4) persons, one of which must have a minimum of five (5) years of experience in commercial roofing.  All of the four (4) individuals must be willing to:
                                  <ul class="list-group list-group-flush">
                                  <li class="list-group-item">Work throughout the day on roofs of various pitches in the hot, Florida sun, 5-6 days per week;</li>
                                    <li class="list-group-item">Be able to consistently lift 100 pounds; and</li>
                                    <li class="list-group-item">Must have valid drivers license and reliable transportation to and from jobsites.</li>
                                  </ul>
                                  If you or a commercial roofing crew think you meet the above requirements, please email your resume or background details to employment@roman-roofing.com. 
                                </p>
                            </div>
                            <a href="#news3" data-toggle="collapse"  class="btn btn-outline-secondary">Read full description.</a>
                        </article>                      
                        </div>
                        <div class="col-md-3">
                            <h5>Summary:</h5>
                            <ul class="list-group">
                              <li class="list-group-item">
                                Hours: 8-5 plus overtime
                              </li>
                              <li class="list-group-item">
                                Hourly pay
                              </li>
                            </ul>
                            <a class="btn btn-danger" href="mailto:help@roman-roofing.com">Email to apply</a>
                            <br><br>
                        </div>
                    </div>
            </section>
@endsection
