@extends('layout')
@section('title','Contact Roman Roofing')
@section('content')
    <body id="contact" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-sm">
                <div class="content">
                    <h1>Contact Roman</h1>
                </div>
            </section>
            <section class="container" id="contact-form">
                <div class="row">
                    <div class="col-md-6">

                        {!! Form::open() !!}

                          <div class="form-row">
                            <div class="form-group col-md-6">
                              {!! Form::label('name', 'Full Name:') !!}
                              {!! Form::text('name', null, ['class' => 'form-control']) !!}
                              <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="form-group col-md-6">
                              {!! Form::label('telephone', 'Phone Number:') !!}
                              {!! Form::text('telephone', null, ['class' => 'form-control']) !!}  
                              <span class="text-danger">{{ $errors->first('telephone') }}</span>                            
                          </div>
                          </div>
                          <div class="form-group">
                              {!! Form::label('email_address', 'Email Address:') !!}
                              {!! Form::text('email_address', null, ['class' => 'form-control']) !!}
                              <span class="text-danger">{{ $errors->first('email_address') }}</span>
                            </div>
                          <div class="form-group">
                              {!! Form::label('comment', 'Question, comment or request:') !!}
                              {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 7]) !!}
                              <span class="text-danger">{{ $errors->first('comment') }}</span>
                          </div>
                          {!! Form::submit('Send Message', ['class' => 'btn btn-warning btn-lg']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-6 sidebar">
                      <h5>What can Roman do for Your Roofing Project?</h5>
                      <h4><a href="tel:2394587663"><i class="fad fa-phone-square"></i> (239) 458-7663</a></h4>
                      <h4><a href="mailto:help@roman-roofing.com"><i class="fad fa-envelope-square"></i> help@roman-roofing.com</a></h4>
                      <p>805 NE 7th Terrace, Cape Coral, Florida, 33909</p>
                      <a class="btn btn-outline-secondary" href="https://maps.google.com/maps?ll=26.666203,-81.956743&z=16&t=m&hl=en&gl=US&mapclient=embed&daddr=Roman%20Roofing%20805%20NE%207th%20Terrace%20Cape%20Coral%2C%20FL%2033909@26.666203,-81.956743" target="_blank">Get directions</a>
                    </div>
                </div>
            </section>
        
            <section class="container-fluid">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3565.446371097138!2d-81.95893704877332!3d26.66620298315214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88db4401079b8b9d%3A0x6df9a6db3c65a02e!2sRoman+Roofing!5e0!3m2!1sen!2sus!4v1565644959645!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </section>
@endsection