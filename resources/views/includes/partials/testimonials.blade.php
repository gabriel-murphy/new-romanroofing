<div class="feedback-text-box bg-grey">
    <span class="quote-icon"><i class="fas fa-quote-left"></i></span>
    <div class="customer-feedback">
        <h3 class="heading-md primary-black">Google</h3>
        <p class="section-paragraph fs-16">
            They did a great job. Cleaned up each day, answered whatever questions you had, very
            affordable. Ask for Clint when you call. Never thought getting a new roof could be so
            easy.
            They will not shuck and jive you, straightforward and upfront about all they can do for
            your
            roof.
        </p>
        <a class="read-more-review-btn"
           href="https://www.google.com/search?q=roman%20roofing&amp;npsic=0&amp;rflfq=1&amp;rlha=0&amp;rllag=32870511,-88277631,905483&amp;tbm=lcl&amp;rldimm=7924548479876440110&amp;ved=2ahUKEwiA283_s5zkAhURWq0KHSGTCrkQvS4wAHoECAoQIA&amp;rldoc=1&amp;tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:14#lrd=0x88db4401079b8b9d:0x6df9a6db3c65a02e,1,,,&amp;rlfi=hd:;si:7924548479876440110;mv:!1m2!1d39.819338099999996!2d-81.1978577!2m2!1d25.9216859!2d-95.3637174;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:14"
           target="_blank">
            Read more reviews on Google</a>
    </div>
    <div class="text-right">
        <span class="quote-icon ml-auto"><i class="fas fa-quote-right"></i></span>
    </div>
</div>
<div class="customer-info">
    <div class="media">
        <img src="{{asset('frontend/images/testimonials/feedback-bbb.jpg')}}"
             class="align-self-start mr-3">
        <div class="media-body">
            <h4 class="customer-name mb-1">Bobby C</h4>
            <div class="rating-stars">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
            </div>
            <div class="feedback-date">
                <p class="paragraph-sm primary-grey">August 11, 2019</p>
            </div>
        </div>
    </div>
</div>
