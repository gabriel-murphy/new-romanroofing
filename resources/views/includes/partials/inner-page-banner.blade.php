<div class="inner-page-banner inner-page-banner-gradient" style="background-image: url('{{asset('frontend/images/backgrounds/bg-inner-page.jpg')}}')">
    <div class="container">
        <div class="banner-title">{{ $banner_title }}</div>
        <div class="banner-description fw-100 m-0">{{ $banner_description }}</div>
    </div>
</div>
