<div id="js_menu" class="main-menu-fixed">
    <div class="container">
        <div class="menu-wrapper">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#">
                    <img class="wd-150" src="{{asset('frontend/images/logos/logo.png')}}" alt="roman-roofing-logo">
                </a>
                <button id="animated_hamburger-fixed" class="hamburger hamburger--stand-r collapsed" type="button" data-toggle="collapse" data-target="#jsNavDropdown">
                          <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                          </span>
                </button>
                <div class="collapse navbar-collapse" id="jsNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('home') }}">HOME <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="about_dropdown_menu" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ABOUT
                            </a>
                            <div class="dropdown-menu" aria-labelledby="about_dropdown_menu">
                                <ul>
                                    <li class="dropdown-item">
                                        <i class="fas fa-history"></i>
                                        <a href="{{ route('history') }}">Our History</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <i class="fas fa-trophy"></i>
                                        <a href="{{ route('portfolio') }}">Portfolio</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <i class="fas fa-newspaper"></i>
                                        <a href="{{ route('news') }}">Press Room</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <a href="{{ route('charity') }}"><i class="fas fa-hand-holding-usd"></i> Roman Charity</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <i class="fas fa-briefcase"></i>
                                        <a href="{{ route('jobs') }}">jobs@roman</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="services_dropdown_menu" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                SERVICES
                            </a>
                            <div class="dropdown-menu" aria-labelledby="services_dropdown_menu">
                                <ul>
                                    <li class="dropdown-item">
                                        <i class="fas fa-house-damage"></i>
                                        <a href="{{ route('service') }}">Re-Roofing Projects</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <i class="fas fa-truck"></i>
                                        <a href="{{ route('service') }}">New Construction</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <i class="fas fa-tint-slash"></i>
                                        <a href="{{ route('service') }}">Roof Maintenance</a>
                                    </li>
                                    <li class="dropdown-item">
                                        <i class="fas fa-tools"></i>
                                        <a href="{{ route('service') }}">Leaks & Repairs</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('products') }}">PRODUCTS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('testimonials') }}">TESTIMONIALS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('financing') }}">FINANCING</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('contact') }}">CONTACT</a>
                        </li>
                        <li class="nav-item d-md-none">
                            <a class="nav-link" href="{{ route('estimate') }}">Free Estimate</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="menu-no-cost-btn d-none d-lg-block">
                <a class="btn-primary-roman" href="{{ route('estimate') }}">Free Estimate</a>
            </div>
        </div>
    </div>
</div>
