@php($pageUrl = request()->route()->uri() === '/' ? 'main-menu' : 'main-menu-secondary')

<header>
    <div class="top-bar bg-primary-maroon">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-options">
                        <ul>
                            <li>
                                <a href="tel:2394587663">
                                    <i class="fas fa-phone-alt"></i>
                                    (239) 458-7663
                                </a>
                            </li>
                            <li>
                                <a href="mailto:help@roman-roofing.com">
                                    <i class="fas fa-envelope"></i>
                                    help@roman-roofing.com
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="top-bar-reviews-link">
                        <a href="{{ route('testimonials') }}">
                            <span class="mr-3">See All 547 Reviews</span>
                            <span class="top-bar-star">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star-half"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="{{$pageUrl}}">
        <div class="container">
            <div class="menu-wrapper">
                <nav class="navbar navbar-expand-xl navbar-light">
                    <a class="navbar-brand" href="{{ route('home') }}">
                        <img class="wd-150" src="{{asset('frontend/images/logos/logo.png')}}" alt="roman-roofing-logo">
                    </a>

                    <button id="animated_hamburger" class="hamburger hamburger--stand-r collapsed" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                          <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                          </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{ route('home') }}">HOME <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="//" id="about_dropdown_menu" role="button"
                                   data-toggle="dropdown" aria-expanded="false">
                                    ABOUT
                                </a>
                                <div class="dropdown-menu" aria-labelledby="about_dropdown_menu">
                                    <ul>
                                        <li class="dropdown-item">

                                            <a href="{{ route('history') }}"> <i class="fas fa-history"></i> Our History</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('portfolio') }}"><i class="fas fa-trophy"></i> Portfolio</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('news') }}"><i class="fas fa-newspaper"></i> Press Room</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('charity') }}"><i class="fas fa-hand-holding-usd"></i> Roman Charity</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('jobs') }}"><i class="fas fa-briefcase"></i> jobs@roman</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="//" id="services_dropdown_menu" role="button"
                                   data-toggle="dropdown" aria-expanded="false">
                                    SERVICES
                                </a>
                                <div class="dropdown-menu" aria-labelledby="services_dropdown_menu">
                                    <ul>
                                        <li class="dropdown-item">
                                            <a href="{{ route('service') }}"><i class="fas fa-house-damage"></i> Re-Roofing Projects</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('service') }}"><i class="fas fa-truck"></i> New Construction</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('service') }}"><i class="fas fa-tint-slash"></i> Roof Maintenance</a>
                                        </li>
                                        <li class="dropdown-item">
                                            <a href="{{ route('service') }}"><i class="fas fa-tools"></i> Leaks & Repairs</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('products') }}">PRODUCTS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('testimonials') }}">TESTIMONIALS</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('financing') }}">FINANCING</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('contact') }}">CONTACT</a>
                            </li>
                            <li class="nav-item d-md-none">
                                <a class="nav-link" href="{{ route('estimate') }}">Free Estimate</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="menu-no-cost-btn d-none d-xl-block">
                    <a class="btn-primary-roman" href="{{ route('estimate') }}">Free Estimate</a>
                </div>
            </div>
        </div>
    </div>
</header>
