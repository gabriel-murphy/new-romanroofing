<div class="news-right-top m-b-20">
    <div class="news-bar m-0 m-b-20">
        <h6>Editor's Pick</h6>
    </div>
    <div class="editors-choice">
        <div class="editors-choice-img gradient-black-light m-b-20">
            <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-1.jpg') }}" alt="news-banner-1.jpg">
        </div>
        <div class="editors-choice-content">
            <h6 class="primary-black fw-600">An EF-1 tornado riped through Northwest Cape Coral Northwest Cape Coral</h6>
            <p class="primary-grey fs-14"><span class="mr-2"><i class="far fa-clock"></i></span> August
                31, 2020</p>
            <p class="mb-4 primary-grey text-justify fs-14 news-overview">At almost 7 am on Saturday, October 19, an EF-1 tornado riped
                through Northwest Cape Coral, leaving a mile-long path of destruction.
                Unlike hurricanes, tornados can literally drop from the sky, with no notice
                to homeowners to seek cover. Fortunately, Roman was on the scene within
                hours, helping rescue victims of this tragedy and providing volunteer
                services for clean-up of the path of destruction.</p>
            <a class="btn-primary-roman p-x-15 p-y-5 fs-14" href="#">Read More</a>
        </div>
    </div>
</div>

<div class="news-right-bottom">
    <div class="news-bar m-0 m-b-20">
        <h6>Roman is Social</h6>
    </div>
    <div class="social-links">
        <ul>
            <li>
                <a class="bg-blue-fb" href="#">
                    <span class="icon clr-blue-fb"><i class="fab fa-facebook-f"></i></span>
                    <span class="likes">1854 Likes</span>
                    <span class="invitation">Like Our Page</span>
                </a>
            </li>
            <li>
                <a class="bg-gradient-instagram" href="#">
                    <span class="icon"><i class="fab fa-instagram clr-gradient-instagram"></i></span>
                    <span class="likes">1854 Followers</span>
                    <span class="invitation">Follow Us</span>
                </a>
            </li>
            <li>
                <a class="bg-blue-twitter" href="#">
                    <span class="icon clr-blue-twitter"><i class="fab fa-twitter"></i></span>
                    <span class="likes">1854 Followers</span>
                    <span class="invitation">Follow Us</span>
                </a>
            </li>
            <li>
                <a class="bg-red-youtube" href="#">
                    <span class="icon clr-red-youtube"><i class="fab fa-youtube"></i></span>
                    <span class="likes">1854 Subscribers</span>
                    <span class="invitation">Subscribe</span>
                </a>
            </li>
        </ul>
    </div>
</div>
