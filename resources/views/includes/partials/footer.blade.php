<footer>
    <div class="footer-top">
        <img class="img-fluid" src="{{asset('frontend/images/bg-quality.jpg')}}" alt="bg-quality.jpg">
    </div>
    <div class="footer-mid bg-grey p-y-30">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h1 class="primary-black fw-600 m-0 text-center text-md-left">Get Your Roofing Project Started Today!</h1>
                </div>
                <div class="col-md-3">
                    <a href="#" class="btn-primary-roman d-block text-center">GET A FREE ESTIMATE</a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom section bg-primary-black">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-bottom-left">
                        <img class="wd-150 mb-4" src="{{asset('frontend/images/logo-white.png')}}" alt="logo-white.png">
                        <p class="text-white text-justify mb-5">A family owned roofing company backed by crews who are true
                            craftsmen of the trade, Roman is the name to trust for your roofing project - small or large,
                            residential or commercial.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-bottom-mid">
                        <h4>Contact Us</h4>
                        <ul class="footer-contact">
                            <li>
                                <a href="tel:2394587663">
                                    <i class="fas fa-phone-alt" aria-hidden="true"></i>
                                    (239) 458-7663
                                </a>
                            </li>
                            <li>
                                <a href="mailto:help@roman-roofing.com">
                                    <i class="fas fa-envelope" aria-hidden="true"></i>
                                    help@roman-roofing.com
                                </a>
                            </li>
                            <li>
                                <i class="fas fa-map-marker-alt"></i>
                                <p class="m-0">921 NE 27th Ln, <span class="d-block">Cape Coral, FL 33909</span></p>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-bottom-right">
                        <h4>Need Help?</h4>
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Full Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Phone Number">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your Email">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary mt-4">Submit</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>

    <div class="footer-bottom-bar bg-black p-y-10">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 order-2 order-lg-1 my-auto">
                    <div class="copyrights">
                        <ul>
                            <li><p class="mb-0 fs-14"><i class="fal fa-copyright"></i> 2015-2019 Roman Roofing, Inc. :: All Rights Reserved.</p></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-lg-6 order-1 order-lg-2">
                    <ul class="footer-social-icons">
                        <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-youtube"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fab fa-linkedin-in"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

   <div class="messenger-link">
       <p class="msg-us m-0">Message Us</p>
       <div class="msg-btn-wrapper">
           <a class="msg-btn" href="http://m.me/RomanRoofingInc">
               <i class="fab fa-facebook-messenger" aria-hidden="true"></i>
           </a>
       </div>
   </div>
</footer>
