	<div id="nav-contact">
                    <a class="btn btn-sm" href="http://m.me/RomanRoofingInc"><i class="fab fa-facebook-messenger"></i> Message Us</a>
                    <a class="btn btn-sm" href="mailto:help@roman-roofing.com"><i class="fad fa-envelope"></i> Email Us</a>
                    <a href="tel:2394587663" class="phone-link"><i class="fad fa-phone-square-alt"></i>(239) 458-7663</a>
                    <!-- <span class="ml-auto"><i class="fad fa-clock"></i> Opens Mon. @ 8:00 a.m.</span>  -->
                </div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="topnav">
                <div class="logo navbar-brand">
                    <a href="/" title="Home"><img src="/images/logo.png" alt="Roman Roofing"/></a>
                </div>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav mx-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="/"><i class="fa fa-home"></i>&nbsp; <span class="sr-only">(current)</span></a>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      About
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="/about"> <i class="fad fa-history"></i> &nbsp; Our History</a>
                      <a class="dropdown-item" href="/about/portfolio"> <i class="fad fa-newspaper"></i> &nbsp; Portfolio</a>
                      <a class="dropdown-item" href="/about/news"> <i class="fad fa-newspaper"></i> &nbsp; Press Room</a>
                      <a class="dropdown-item" href="/about/jobs"> <i class="fad fa-toolbox"></i> &nbsp; Jobs @ Roman</a> 
                    </div>
                  </li>                  

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Services
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="/residential-roofing"> <i class="fad fa-home"></i> &nbsp; Residential Roofing</a>
                      <a class="dropdown-item" href="/commercial-roofing"> <i class="fad fa-warehouse"></i> &nbsp; Commercial Roofing</a>
                      <a class="dropdown-item" href="/re-roof"> <i class="fad fa-hard-hat"></i> &nbsp; Re-Roofing Projects</a>
                      <a class="dropdown-item" href="/new-roof"> <i class="fad fa-truck-container"></i> &nbsp; New Construction</a>
                      <a class="dropdown-item" href="/roof-maintenance"> <i class="fad fa-tint-slash"></i> &nbsp; Roof Maintenance</a>
                      <a class="dropdown-item" href="/roof-repairs"> <i class="fad fa-house-flood"></i> &nbsp; Leaks & Repairs</a>
                    </div>
                  </li>

                <li class="nav-item">
                    <a class="nav-link" href="/financing">Financing</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/contact">Contact</a>
                  </li>
                </ul>
                <div class="navbar-right ml-auto">
                        <a href="/estimate" class="btn estimate-btn"> <i class="fad fa-clipboard-list-check"></i> &nbsp;No Cost Evaluation</a>
                    </div>
              </div>
            </nav>
