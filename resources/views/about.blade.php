@extends('layout')
@section('title','About Roman Roofing')
@section('content')
    <body id="about" class="inner">
@extends('navigation')
        <div id="pageArea">

            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>About Us</h1>
                    <hr/> 
                    <p>Roman is Southwest Florida's trusted experts at roofing - residential or commerical projects and of all sizes!</p>
                </div>
            </section>

            <section id="best" class="container-fluid">
                <div class="row">
                    <div class="col-md-6 image">
                            <img src="images/award-collage.jpg" />
                    </div>
                    <div class="col-md-6">
                        <div class="text">
                            <p class="red">We are proud to announce that...</p>
                            <h2>Roman Roofing is the Fastest Growing Contractor in Florida!</h2>
                            <p>Ranked in the top 3% of the fastest growing, privately held companies in America, Roman is the call to make for your roofing project - small or large.  Our growth is a consequence of our committment to project management excellence and consistent training.  Roman is also one of a few reciepients of GAF's Triple Excellence Award and President Club recipeints in Southwest Florida.</p>
                            <a class="btn btn-outline-secondary" href="/estimate">Get a No Cost Estimate</a>
                            </div>
                    </div>
                </div>
            </section>

            <section id="roman">
                <div class="left">
                    <div class="content">
                        <hr class="red"/>
                        <h2>Family Owned and Operated</h2>
                        <p>
                            Those in the Roman family appreciate the generosity of the ownership, and subscribe to the vision of its co-founder and Chief
                            Executive Officer, Norm Dopfer.  Named after his youngest son pictured to the right, Roman has quickly became a brand synonimous with trust
                            and excellence in the rendition of roofing services in the Southwest Florida area.  Mrs. Dopfer is the firms Chief Financial Officer and mother of two.  With their Midwest-driven hustle and drive for perfection, they engage with and lead the management and crews of Roman Roofing, Inc. 
                        </p>
                    </div> 
                </div>
                <div class="right"></div>        
                <div class="clearer"></div>
            </section>

            <section id="dogs">
                <div class="right">
                    <div class="content">
                        <hr class="red"/>
                        <h2>We Love Our Roofing Crews - Especially the Four-Legged Ones</h2>
                        <p>
                            While the Roman family and our crews are fanatic about roofing perfection, we are also fond of our #roofers. &nbsp;Starting in April, the Roman 
                            family was overwhelmed with contract work and needing more #roofers - some of our furry-friends were spotted around Cape Coral and Nort Fort Myers.  Recently, a smoked bar-b-que competition drew the attention of our newest #roofer - a stray who came upon our new headquarters and was quickly adopted by the Roman family.  &nbsp;Our corporate generosity extends beyond our endearment for our #roofers - see what the Roman family does for humans also on our sponsorship page!
                        </p>
                    </div>                  
                </div>
                <div class="left"></div>
                <div class="clearer"></div>
            </section>

            <section id="trust">
                <div class="left">
                    <div class="content">
                        <hr class="red"/>
                        <h2>Roman, A Name You Can Trust</h2>
                        <p>
                            The Roman management consists of subject matter experts of the roofing trade - the crews which they lead consists of highly trained
                            and experienced craftsman who take pride in their work and consistently demonstrate the upmost respect for our customers' property. &npsp;
                            Indeed, with over 100 employees and dozens of crews, our name carries over 170 years of year-round roofing experience.  It's just one of the many reasons why homeowners, property owners and realtors put their name trust in Roman for anything roofing.  
                        </p>
                    </div>                  
                </div>
                <div class="right"></div>
            </section>

            <section id="team">
                <p class="red">Meet Our Top Notch Team of</p>
                <h2>Expert Roofers</h2>
                <div class="fourCol">
                    <div class="col one">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Norm D.</h4>
                        <p class="red">Co-Founder & CEO</p>
                    </div>

                    <div class="col two">
                        <img class="pic" src="images/team/lindsey.png"/>
                        <h4><a href="https://www.linkedin.com/in/lindsey-dopfer-39531285/" target="_new"><i class="fab fa-linkedin"></i></a> &nbsp;Lindsey D.</h4>
                        <p class="red">Co-Founder & CFO</p>
                    </div>

                    <div class="col three">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><i class="fad fa-paw"></i></a> &nbsp;George</h4>
                        <p class="red">Chief #Roofer</p>
                    </div>

                    <div class="col four">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>John B.</h4>
                        <p class="red">Chief Operations Officer</p>
                    </div>

                    <div class="col one">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Alison D.</h4>
                        <p class="red">Scheduling Manager</p>
                    </div>

                    <div class="col two">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Jake S.</h4>
                        <p class="red">Crew Manager</p>
                    </div>

                    <div class="col three">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Allen C.</h4>
                        <p class="red">Quality Control</p>
                    </div>

                    <div class="col four">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Devon H.</h4>
                        <p class="red">Production Manager</p>
                    </div>

                    <div class="col one">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Clint X.</h4>
                        <p class="red">Residential Estimator</p>
                    </div>

                    <div class="col two">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Gabriel M.</h4>
                        <p class="red">Marketing & Technology</p>
                    </div>

                    <div class="col three">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Mark S.</h4>
                        <p class="red">Residential Estimator</p>
                    </div>

                    <div class="col four">
                        <img class="pic" src="images/team-temp.jpg"/>
                        <h4><a href="#"><img src="images/icon-linkedin.jpg" /></a>Jeff S.</h4>
                        <p class="red">Permitting Specialist</p>
                    </div>
                </div>


            </section>

            <section id="stats">
                <div class="threeCol">

                    <div class="col one">
                        <div class="icon"></div>
                        <div class="stats">
                            <h4 class="count" data-count="15691">0</h4>
                            <p>Liters of Gatorade Consumed</p>
                        </div>
                    </div>

                    <div class="col two">
                        <div class="icon"></div>
                        <div class="stats">
                            <h4 class="count" data-count="3155">0</h4>
                            <p>Roofs Installed</p>
                        </div>
                    </div>

                    <div class="col three">
                        <div class="icon"></div>
                        <div class="stats">
                            <h4 class="count" data-count="7382">0</h4>
                            <p>Smiles Brought to Faces</p>
                        </div>
                    </div>

                </div>
            </section>
@endsection
