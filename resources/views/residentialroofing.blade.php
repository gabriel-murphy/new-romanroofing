@extends('layout')
@section('title','Residential Roofing by Roman Roofing')
@section('content')
    <body id="residential" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>Residential Roofing</h1>
                    <hr/> 
                    <p>Roman has successfully completed thousands of residential re-roof and new construction projects in South Florida in the past several years alone.</p>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h4>Only The Finest Quality Materials Applied & Craftsmanship Practiced Here</h4>
                        <p>Whether constructing your new home or in need of a new roof on your residence, you can count on Roman to exceed your expectations. &nbsp;In recent years, our expert roofers have adorned Southwest Florida with over 14 million square feet of only the highest quality roofing materials available on the market.  &nbsp;Indeed, homeowners serious about enhancing their property value through this investment trust in us to oversee this high-risk, complex process.  

                        </p>
                        <h5>We offer four main varities of roofing materials for residential application in a diverse array of patterns and colors:</h5>
                         <p>Dimensional / Architectual Shingles</p>
                         <p>Metal Varities</p>
                         <p>Tile & Clays</p>
                         <p>Stone-Coated Steel</p>
                         <p>Solar Tiles</p>
                    </div>

                    <div class="col-md-5" id="sidebar">
                      <section id="easy-residential">
                        <div class="content">
                            <p class="red">Roman Makes Residential</p>
                            <h4>Homeowners So Happy</h4>
                                <div class="one step">
                                    <i class="fad fa-clipboard-list-check fa-5x"></i>
                                    <h5>Expert Evaluations</h5>
                                </div>

                                <div class="two step">
                                    <i class="fad fa-money-check-edit-alt fa-5x"></i>
                                    <h5>Financing Options</h5>
                                </div>

                                <div class="three step">
                                    <i class="fad fa-user-hard-hat fa-5x"></i>
                                    <h5>English Speaking Crews</h5>
                                </div>
                                <a class="btn btn-outline-secondary" href="/estimate">Get No Cost Estimate</a>
                                <br>
                                <br>
                            </div>
                            </section>
                        </div>
                </div>
            </section>

            <section id="problem">
                <div class="content">
                    <img src="/images/van.png"/>
                    <h1>It's Roman Roofing to the Rescue!</h1>
                    <p>
                        Picking the wrong roofing company can lead to major problems for you and your property. &nbsp;It will lead to substantial water damage to your property and added stress. &nbsp;As our customers know, Roman Roofing completes the job right the first time and on time - licensed, insured and a satisfaction guarantee for every job. &nbsp;Call Roman to rescue your roofing project today!
                    </p>
                    <br><br>
                    <p><a class="btn btn-primary btn-lg" href="/estimate">No Cost Estimate</a></p>
                </div>
            </section>
        

            <section id="problem">
                <div class="content">
                    <img src="/images/portfolio/7.jpg"/>
                    <h1>An Assortment of High Quality Roofing Materials</h1>
                    <p>
                        Asphalt Shingle 
                        At Roman, we only invest and furnish the highest quality roofing materials avaialble on the market.  &nbsp;This includes dimentional and architectural asphalt shigles manufactured by GAF and Ownens Corning.  Our Timerline HD series is a popular option and is 55% thicker than the traditional aslphalt shingle.  This top-grade aslphalt shingle includes stain gaurd alge protection plus technology, and with a 25-year Golden Pledge Warranty through Roman, it is an obvious choice for your residential asphalt shingle roofing project.<br><br>

                        Metal Varieties 
                        Metal roofing is specifically engineered to last decated longer than any other roofing material - such is the reason many homeowners elect to invest
                        in a metal roof in spite of its higher cost. &nbsp;A standing seam metal roof from Roman - properly maintained - can surpass a 60 year lifespan. &nbsp;Our 26 gauge metal is thicker than industry standard, reducing the incidence of waving and bending. &nbsp;Trendy metal shingles have become a popular option
                        for the homeowner looking to strike the balance of the natural look of tile along with the longevity offered by metal.
                        <br><br>

                        Tile & Clays
                        Southwest Florida is well-known as a region that has historically embrased tile and clay roofing materials in both residential and commercial projects
                        alike. &nbsp;Most recently with the shifting marketing trends, Roman has embrased a trendy group of high-quality manufacturers of natural looking tile and clay roofing materials, including Eagle. &nbsp;If you are looking for a roof that is exquisite, eye-catching and unlike anything else available, then our Artisan Tapered Slate Collection is for you! Featuring rich, traditional slate tones in charcoal, green, burgundy, black and copper, our Tapered Slate tiles flaunt vibrant hues and beautiful color transitions. See for yourself in our showroom<br><br>

                        Stone-Coated Steel
                        Our Decra line of stone-coated steel combines the superior performance of steel with the classic beauty, elegance and architectural detail of an old world Italian tile. &nbsp;Durable and lightweight, Villa tile requires little to no maintenance and is walkable. 

                    </p>
                    <br><br>
                    <p><a class="btn btn-primary btn-lg" href="/estimate">Get a FREE Estimate</a></p>
                </div>
            </section>
@endsection
