@extends('layout')
@section('title','New Roof')
@section('content')
    <body id="newroof" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>New Construction Roofing</h1>
                    <hr/> 
                    <p>The best new construction roofer in South Florida</p>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h4>Roman Knows New Construction Roofing</h4>
                        <p>
                        Regardless if you have a residential new home construction or a commercial new construction roofing project, Roman Roofing performs outstanding work. We work with contractors all over South Florida to deliver top-notch new construction roofing projects. No project is too big or too small.
                        </p>
                        <img src="/images/new-construction-roof.jpg" class="content-photo">
                    </div>
                    <div class="col-md-4 sidebar">
                      <h4>New Construction Roofing</h4>
                        <div class="card">
                          <img src="/images/apartments.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">Apartments & Condo Roofs</h5>
                          </div>
                        </div>
                        <div class="card">
                          <img src="/images/new-house-roof.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">New Construction Residential</h5>
                          </div>
                        </div>
                        <br><br>
                        <a class="btn btn-outline-secondary btn-block" href="/estimate">Get No Cost Estimate</a>
                      
                    </div>
                </div>
            </section>
@endsection
