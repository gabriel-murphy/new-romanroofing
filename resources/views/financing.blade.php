@extends('layout')
@section('title', 'Roman Roofing :: Contact Us!')
@section('content')
    <body id="financing" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>Financing Your Roofing Project</h1>
                    <hr/> 
                    <p>Roman offers a suite of financing options to make your upcoming roofing project a reality.</p>
                </div>
            </section>

            <section id="finance" class="twoCol">
                <div class="contentWide">
                    <div class="left">
                        <h2>Reasonable Financing Options from Roman</h2>
                        <p>
                            Roman has strong relationships with finance companies offering affordable financing solutions for your roofing project, including YGrene and other top lenders.  Schedule a no cost evaluation with one of our scheduling coordinators now by calling Roman at <a href="tel:2394587663">(239) 458-7663</a>.
                        </p>
                    </div>

                    <div class="right">
                        <img src="images/calculator.jpg" />
                    </div>
                </div>
            </section>

            <section id="easy">
                <div class="content">
                    <p class="red">Roman Roofing Provides</p>
                    <h5>financing options that allow property owners to leverage their home's equity with fast qualification and payments customized to your income and lifestyle.</h5>
                    <div class="threeCol">

                        <div class="col one">
                            <i class="fad fa-home fa-5x"></i>
                            <h4>Use Property Equity</h4>
                        </div>

                        <div class="col two">
                            <i class="fad fa-money-check fa-5x"></i>
                            <h4>Fast Qualifiaction</h4>
                        </div>

                        <div class="col three">
                            <i class="fad fa-ballot-check fa-5x"></i>
                            <h4>Easy Payments</h4>
                            </div>
                        </div>
                    </div>
            </section>

            <section id="more">
                <div class="contentWide">
                    <p class="red">Let's not forget</p>
                    <h2>Your Roof Is An Investment</h2>
                    <div class="twoCol">

                        <div class="col one">
                            <img src="images/more-new-construction.jpg" />
                            <h4>Attractive Returns</h4>
                            <p>Putting money into your home can be an excellent investment with an attractive return. 
                                The National Association of REALTORS indicates that a roofing investment can return 105% of the value of the project upon sale of a home.*</p>
                        </div>

                        <div class="col two">
                            <img src="images/more-maintenance-repairs.jpg" />
                            <h4>Low Monthly Payments</h4>  
                            <p>As an example, an unsecured home improvement loan of $10,000 with a term of 60 months with an APR of 10% monthly payment could be as low as $212.47** for applicants with excellent credit. 
                                Talk to your contractor about the specifics of your project financing.</p>
                        </div>

                    </div>
                </div>
            </section>
@endsection