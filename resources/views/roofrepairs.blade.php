@extends('layout')
@section('title','Maintenance')
@section('content')
    <body id="repairs" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>Roofing Repairs</h1>
                    <hr>
                    <p>Roman is the name to call in South Florida for leaks and repairs - whether large or small!</p>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Leaks & Repair</h3>
                        <p>Often times is the case, roof systems fail because of water inflitration of the roofing structure, due to a combination of aging or windstorm damage.  Employing subject matters experts at leak detection and adequate repair is just the first step to ensuring your roof is restored before
                        compromising all of the contents underneith it.  Enter the experts at Roman - we are available 24/7 for your emergency repairs.</p>
                        <div class="content-photo">
                            <img src="/images/roof-maintenance.jpg" style="width:100%;height:300px">
                        </div>
                        <p>Florida Statute 627.70132, enacted on June 1, 2011, subjects homeowners to a three year period from the date in which the damage was sustained (the hurricane made 'landfall') in order to file a claim. &nbsp;With less than a year remaining to assert windstorm-related damage caused by Hurricane Irma,
                        homeowners who have deferred bringing their claim should outsource the headache to a windstorm claim specialist at Roman. &nbsp;Aside from detecting windstorm damage (curling shingles, warped flashing and drip edge), we have a strong track record of protecting the home owner by being proactive in working with insurance carriers.</p>
                    </div>
                    <div class="col-md-4 sidebar">
                      <section id="easy-leaks">
                        <div class="content">
                            <p class="red">Roman Fixes Leaky Roofs</p>
                            <h4>Simply and Affordably</h4>
                                <div class="one step">
                                    <i class="fad fa-clipboard-list-check fa-5x"></i>
                                    <h5>Expert Evaluations</h5>
                                </div>

                                <div class="two step">
                                    <i class="fad fa-money-check-edit-alt fa-5x"></i>
                                    <h5>Financing Options</h5>
                                </div>

                                <div class="three step">
                                    <i class="fad fa-user-hard-hat fa-5x"></i>
                                    <h5>English Speaking Crews</h5>
                                </div>
                                <a class="btn btn-outline-secondary" href="/estimate.html">Get No Cost Estimate</a>
                                <br>
                                <br>
                            </div>
                            </section>
                    </div>
                </div>
            </section>
@endsection
