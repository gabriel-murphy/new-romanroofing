@extends('layout')
@section('title','New Roof')
@section('content')
    <body id="portfolio" class="inner">
@extends('navigation')
	  <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <p>We Are Expert Craftsman of the Roofing Trade</p>
                    <h1 style="color:#fff;">Roofing Portfolio & Case Studies</h1>
                    </div>
                </div>
            </section>
            <section id="video-gallery" class="container">
              <div class="row">
                <div class="col centered portfolio-header">
                  <i class="fab fa-youtube fa-5x"></i>
                  <p>The crews of Roman Roofing have adorned over 14,000,000 square feet of Southwest Florida with only the top of the line roofing materials on the market. &nbsp;We have assembled a small collection of our notable roofing projects, which we feature as our job site of the month and the materials selected range from dimensional shingle to tile, metal and other varieties. &nbsp;Take a look at our portfolio of roofing projects below.</p>
                  </div>
              </div>
                <div class="row" style="margin-bottom:40px;">
                    <div class="col-md-12">
                        <iframe width="100%" height="600" src="https://www.youtube.com/embed/7hCsuWT1wCo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="row" style="margin-bottom:40px;">
                    <div class="col-md-12">
                        <iframe width="100%" height="600" src="https://www.youtube.com/embed/cF25lPzEpXU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <iframe width="100%" height="600" src="https://www.youtube.com/embed/NPlmi0bAwaE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </section>
            <section class="container">
              <div class="row">
                <div class="col portfolio-header centered">
                  <i class="fas fa-images fa-5x"></i>
                  <p>Our featured projects below include stone-coated steel application in Sanibel Island, concrete tile in Cape Coral, and a TPO application on a flat roof for a church in Punta Gorda.</p>
                </div>
              </div>
            </section>
              <section id="gallery" class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div id="light-gallery" class="light-gallery">
                          </a>
                          <a target="_blank" href="/images/portfolio/13.jpg">
                              <img src="/images/portfolio/13.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/14.jpg">
                              <img src="/images/portfolio/14.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/15.jpg">
                              <img src="/images/portfolio/15.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/16.jpg">
                              <img src="/images/portfolio/16.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/17.jpg">
                              <img src="/images/portfolio/17.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/18.jpg">
                              <img src="/images/portfolio/18.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/19.jpg">
                              <img src="/images/portfolio/19.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/20.jpg">
                              <img src="/images/portfolio/20.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/21.jpg">
                              <img src="/images/portfolio/21.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/22.jpg">
                              <img src="/images/portfolio/22.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/23.jpg">
                              <img src="/images/portfolio/23.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/24.jpg">
                              <img src="/images/portfolio/24.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/25.jpg">
                              <img src="/images/portfolio/25.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/26.jpg">
                              <img src="/images/portfolio/26.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/27.jpg">
                              <img src="/images/portfolio/27.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/28.jpg">
                              <img src="/images/portfolio/28.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/29.jpg">
                              <img src="/images/portfolio/29.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/30.jpg">
                              <img src="/images/portfolio/30.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/31.jpg">
                              <img src="/images/portfolio/31.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/32.jpg">
                              <img src="/images/portfolio/32.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/1.jpg">
                              <img src="/images/portfolio/1-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/2.jpg">
                              <img src="/images/portfolio/2-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/3.jpg">
                              <img src="/images/portfolio/3-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/4.jpg">
                              <img src="/images/portfolio/4-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/5.jpg">
                              <img src="/images/portfolio/5-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/6.jpg">
                              <img src="/images/portfolio/6-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/7.jpg">
                              <img src="/images/portfolio/7-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/8.jpg">
                              <img src="/images/portfolio/8-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/9.jpg">
                              <img src="/images/portfolio/9-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/10.jpg">
                              <img src="/images/portfolio/10-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/11.jpg">
                              <img src="/images/portfolio/11-thumb.jpg" />
                          </a>
                          <a target="_blank" href="/images/portfolio/12.jpg">
                              <img src="/images/portfolio/12-thumb.jpg" />
                        </div>

                    </div>  
                </div>
            </section>
@endsection
