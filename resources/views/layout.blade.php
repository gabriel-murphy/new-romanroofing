<!DOCTYPE html>
<html>
{{ $title ?? ''}}
<head>
<title>@yield('title','Roman Roofing :: Southwest Florida\'s Most Trusted Roofing Contractor')</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="HandheldFriendly" content="true"></meta>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>


        <script src="https://kit.fontawesome.com/946e522d43.js"></script>
        <link href="/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="/css/lightgallery.css" rel="stylesheet" type="text/css"/>
        <link href="/css/fontawesome.min.css" rel="stylesheet"/>
        <link href="/css/swiper.min.css" rel="stylesheet"/>
        <link href="/css/styles.css" rel="stylesheet" type="text/css"/>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-21413580-2"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtsoWEUduR1G8IylC7M1YefQO1b4REHNc&libraries=places"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-21413580-2');
        </script>
    </head>

@yield('content')

            <!-- Footer for Laravel View -->
            <section id="quality">
                <div class="content">
                    <img src="/images/quality.png" />
                </div>
            </section>

            <section id="getQuote">
                <div class="content">
                    <h4>Get Your Roofing Project Started Today!</h4>
                    <a href="/estimate" class="button outline">Get a No Cost Quote</a>
                </div>
            </section>

            <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4" id="footer-left">
                    <img src="/images/logo-white.png" id="footer-logo"/ alt="Roman Roofing">
                    <p>A family owned roofing company backed by crews who are true craftsmen of the trade, Roman is the name to trust for your roofing project - small or large, residential or commercial. </p>
                    <div id="footer-social">
                        <a href="https://www.facebook.com/RomanRoofingInc/" target="_blank">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        <a href="https://www.instagram.com/roman_roofing_inc/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        <a href="https://www.youtube.com/channel/UCAR787EurNOGkaaPuHCEObw" target="_blank">
                                <i class="fab fa-youtube-square"></i>
                            </a>
                     <a href="https://linkedin.com/company/romanroofing" target="_blank">
                                <i class="fab fa-linkedin"></i>
                            </a>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <ul class="links">
                        <li><a href="/about"><i class="fad fa-address-card"></i> &nbsp; About Roman</a></li>
                        <li><a href="/estimate"><i class="fad fa-pencil-alt"></i> &nbsp; No Cost Evaluation</a></li>
                        <li><a href="/residential-roofing"><i class="fad fa-home"></i> &nbsp; Residential Roofing</a></li>
                        <li><a href="/commercial-roofing"><i class="fad fa-warehouse"></i> &nbsp; Commercial Roofing</a></li>
                        <li><a href="/re-roof"><i class="fad fa-hard-hat"></i> &nbsp; Re-Roof Projects</a></li>
                        <li><a href="/roof-maintenance"><i class="fad fa-tint-slash"></i> &nbsp; Roof Maintenance</a></li>
                        <li><a href="/roofing-materials"><i class="fad fa-clipboard-list"></i> &nbsp; Roofing Materials</a></li>
                        <li><a href="/roofing-financing"><i class="fad fa-money-check-edit-alt"></i> &nbsp; Financing Options</a></li>
                        <li><a href="/repairs"><i class="fad fa-house-flood"></i> &nbsp; Leaks & Repairs</a></li>
                        <li><a href="/contact"><i class="fad fa-phone-office"></i> &nbsp; Contact Roman</a></li>
                    </ul>
                </div>
                    <div class="col-sm-2 footer-logos">
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank">
                                <img src="/images/logos/gaf.png">
                            </a>
                            <a href="https://www.angieslist.com/companylist/us/fl/cape-coral/roman-roofing-inc-reviews-8810457.htm" target="_blank">
                                <img src="/images/logos/angies.png">
                            </a>
                    </div>
                    <div class="col-sm-2 footer-logos">
                        <a href="https://www.capecoralchamber.com/" target="_blank">
                                <img src="/images/logos/chamber.png">
                            </a>
                     <a href="https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736" target="_blank">
                                <img src="/images/logos/bbb.png">
                            </a>
                            <a href="https://www.yelp.com/biz/roman-roofing-cape-coral" target="_blank">
                                <img src="/images/logos/yelp.png">
                            </a>
                    </div>
                </div>
                <div class="row" id="bottom-bar">
                    <div class="col-md-12">
                        <p class="center"><i class="fad fa-copyright"></i> &nbsp;2015-2019 Roman Roofing, Inc. :: All Rights Reserved.</p>
                    </div>
                </div>
            </div>
            </footer>
        </div>
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/swiper.min.js"></script>
        <script>
        // var a = 0;
        //     $(window).scroll(function() {
        //
        //         var oTop = $('#stats').offset().top - window.innerHeight;
        //         if (a == 0 && $(window).scrollTop() > oTop) {
        //             $('.count').each(function() {
        //                 var $this = $(this),
        //                         countTo = $this.attr('data-count');
        //                 $({
        //                     countNum: $this.text()
        //                 }).animate({
        //                     countNum: countTo
        //                 },
        //                 {
        //                     duration: 3000,
        //                     easing: 'swing',
        //                     step: function() {
        //                         $this.text(Math.floor(this.countNum));
        //                     },
        //                     complete: function() {
        //                         $this.text(this.countNum);
        //                         //alert('finished');
        //                     }
        //
        //                 });
        //             });
        //             a = 1;
        //         }
        //
        //     });
        //                 $(document).ready(function() {
        //                     var galleryThumbs = new Swiper('.gallery-thumbs', {
        //       spaceBetween: 0,
        //       slidesPerView: 6,
        //       freeMode: true,
        //       watchSlidesVisibility: true,
        //       watchSlidesProgress: true,
        //     });
        //     var galleryTop = new Swiper('.gallery-top', {
        //       spaceBetween: 10,
        //       autoplay:true,
        //       navigation: {
        //         nextEl: '.swiper-button-next',
        //         prevEl: '.swiper-button-prev',
        //       },
        //       thumbs: {
        //         swiper: galleryThumbs
        //       }
        //     });
        //                 });
        </script>

        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script>

        <script>
            function openReview(reviewName) {
                var i;
                var x = document.getElementsByClassName("review");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById(reviewName).style.display = "block";
            }
        </script>
    </body>
</html>
