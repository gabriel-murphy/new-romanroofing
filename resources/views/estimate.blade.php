@extends('layout')
@section('title','Obtain A Free Evaluation on Your Roof')
@section('content')
    <body id="estimate" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-sm">
                <div class="content">
                    <h1>No Cost Roof Evaluation</h1>
                </div>
            </section>
            <section class="container" id="estimate-form">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h4 class="centered">Tell Roman About Your Roofing Project</h4>
                        <form method="post" action="/estimate" class="form" enctype="multipart/form-data" id="estimateForm">
                          @csrf
                          <div class="form-row">
                            <div class="form-group col-md-6">
                              <label for="name">Your Name:</label>
                              <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="email">Email Address:</label>
                              <input type="text" class="form-control" id="email_address" name="email_address" required>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="address">Project Address:</label>
                            <input type="text" class="form-control" id="address" name="address" required>
                          </div>
                          <div class="form-row">
                            <div class="form-group col-md6">
                              <label for="city">City:</label>
                              <input type="text" class="form-control" id="city" name="city" required>
                            </div>
                            <div class="form-group col-md-2">
                              <label for="zip">Zip Code:</label>
                              <input type="number" class="form-control" id="zip" name="zip" required>
                            </div>
                            <div class="form-group col-md-4">
                              <label for="phone">Phone Number:</label>
                              <input type="number" class="form-control" id="telephone" name="telephone" required>
                            </div>
                          </div>
                          <div class="form-row">
                          <div class="form-group col-md-6">
                            <h5>Type of Roofing Project:</h5>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="type" value="New Construction" id="new_construction">
                              <label class="form-check-label" for="new_construction">
                                New Construction
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" value="Re-Roof" name="type"  id="reroof">
                              <label class="form-check-label" for="reroof">
                                Re-roof Project
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" value="Repair" name="type" id="roof_repair">
                              <label class="form-check-label" for="roof_repair">
                                Roof Repair
                              </label>
                            </div>
                          </div>
                            <div class="form-group col-md-6">
                              <h5>Type of Roofing Materials:</h5>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" value="shingles" name="material" id="shingles">
                                <label class="form-check-label" for="shingles">
                                  Dimensional / HD Asphalt Shingles
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" value="tile" name="material" id="tile">
                                <label class="form-check-label" for="tile">
                                  Concrete Tile or Clay
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" value="metal" name="material" id="metal">
                                <label class="form-check-label" for="metal">
                                  Metal Varieties
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" value="solar" name="material" id="solar">
                                <label class="form-check-label" for="solar">
                                  Solar Roofing Options
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="centered"><button type="submit" class="btn btn-warning btn-lg">Request No Cost Evaluation</button></div>
                        </form>
                    </div>
                </div>
            </section>

            <section class="container">
              <h4 class="centered">Average of 4.8 star reviews across multiple platforms</h4>
              <!-- Accolade Bar -->
            <section class="container-fluid" id="accolade-bar">
                <div class="row">
                    <div class="col">
                        <a href="https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736" target="_blank"><img src="/images/logos/bbb.png"></a>
                        <a href="needs_url"><img src="/images/logos/inc5000.png"></a>
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank"><img src="/images/logos/gaf.png"></a>
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank"><img src="/images/logos/gaftriple.png"></a>
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank"><img src="/images/logos/gahelite.png"></a>
                        <a href="https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736" target="_blank"><img src="/images/logos/angies.png"></a>
                        <a href="https://www.capecoralchamber.com/" target="_blank"><img src="/images/logos/chamber.png"></a>
                    </div>
                </div>
            </section>
            </section>

            <section id="stats">
                <div class="threeCol">

                    <div class="col one">
                        <div class="icon"></div>
                        <div class="stats">
                            <h4 class="count" data-count="15691">0</h4>
                            <p>Liters of Gatorade Consumed</p>
                        </div>
                    </div>

                    <div class="col two">
                        <div class="icon"></div>
                        <div class="stats">
                            <h4 class="count" data-count="3155">0</h4>
                            <p>Roofs Installed</p>
                        </div>
                    </div>

                    <div class="col three">
                        <div class="icon"></div>
                        <div class="stats">
                            <h4 class="count" data-count="7382">0</h4>
                            <p>Smiles Brought to Faces</p>
                        </div>
                    </div>

                </div>
            </section>
@endsection