<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style type="text/css">
        body{
            margin: 0;
            background-color: #EFEFF0;

        }
        table{
            border-spacing: 0;
        }
        td{
            padding: 0;
        }
        img{
            border: 0;
        }

    </style>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;background-color: #EFEFF0;" role="presentation">
    <tr>
        <td align="center">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;background-color: #FFFFFF;padding: 20px;font-family: 'Lato', sans-serif;" role="presentation">
                {!! $content !!}




            </table>
        </td>
    </tr>
</table>
</body>
</html>
