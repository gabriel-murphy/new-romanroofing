@extends('layout')
@section('title','Re-Roof')
@section('content')
    <body id="reroof" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>Re-Roof Projects</h1>
                    <hr/> 
                    <p>Re-Roofing projects for Fort Meyers</p>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Over 2,000 Re-Roofing Projects Completed</h4>
                        <p>
                            Roman crews have successfully completed over 2,000 re-roofing projects in Southwest Florida over the past several years - you could say it is our bread-and-butter. &nbsp;Re-roofing projects are complex construction projects which require constant project oversight to ensure timely completion. &nbsp;The proper re-roofing project can greatly enhance your property's curbside appeal, while boosting its residual value at the same time.
                        </p>
                        <!--<img src="/images/group-photo.jpg" class="content-photo">-->
                        <h5>Tear Offs, Engineering & Application</h5>
                        <p>One of the most glaring differences between commercial and residential building owners lies in the roofing systems available to them. Residential roofing systems are typically asphalt shingle, occasionally concrete tile, depending on the climate, and some other less popular materials. Asphalt shingle is far and away the most popular roofing option for residential roofing systems. &nbsp;Indeed, decision makers have substantially more options for the roofing system if they own or manage a commercial building.  &nbsp;Commercial roofing systems consist of sprayed polyurethane foam, restoration coatings, single-ply (TPO, PVC, EPDM), modified bitumen, concrete, built-up, tar and gravel, and many others. The bottom line, there are significantly more commercial roofing systems available than there are residential, which means you as a commercial building owner need to do more research before making purchase or repair decisions regarding your roof.</p>
                    </div>
                    <!--<div class="col-md-4">
                      <h4>Commercial Roofing</h4>
                        <div class="card">
                          <img src="/images/placeholder.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">Apartments & Condos</h5>
                            <p class="card-text">See our Case Study of Briarwood Village in Estero, Florida</p>
                          </div>
                        </div>
                        <div class="card">
                          <img src="/images/placeholder.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">Churches</h5>
                            <p class="card-text">See our Case Study of Grace Church in Port Charlotte, Florida</p>
                          </div>
                        </div>
                        <div class="card">
                          <img src="/images/placeholder.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">Multi-family</h5>
                            <p class="card-text">See our Case Study of Tropical Snooner in Naples, Florida</p>
                          </div>
                        </div>
                        <br><br>
                        <a class="btn btn-outline-secondary btn-block" href="/estimatel.html">Get No Cost Estimate</a>
                      
                    </div>
                  -->
                </div>
            </section>
@endsection
