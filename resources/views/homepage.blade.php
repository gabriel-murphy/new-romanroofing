@extends('layout')
@section('title', 'Roman Roofing :: Southwest Florida\'s Most Trusted Roofing Contractor')
@section('content')
   <body id="home">
@extends('navigation')
        <div id="pageArea">
            <div id="home-swiper-container">
              <div class="swiper-container gallery-top">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image:url(/images/bg-showcase.jpg)">
                    <div class="content">
                            <img src="/images/pro-roofing.png">
                            <hr>
                            <p>Over 200+ years of roofing experience stands behind the leadership of the Roman name.</p>
                            <a href="/estimate" class="button estimate">No-Cost Estimate</a>
                            <a href="/portfolio" class="button outline">Case Studies</a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image:url(/images/bg-residential.jpg)">
                        <div class="content">
                            <h1>Residential<br>Roofing</h1>
                            <hr>
                            <p>Roman has completed thousands of residential new and re-roof projects in just the past several years.</p>
                            <a class="button outline" href="/residential-roofing">Read More</a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image:url(/images/bg-commercial.jpg)">
                    <div class="content">
                            <h1>Commercial<br>Roofing</h1>
                            <hr>
                            <p>Roman specializes in complex roofing projects including high-rise, schools and steep-slope.</p>
                            <a class="button outline" href="/commercial-roofing">Read More</a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image:url(/images/bg-waterproofing.jpg)">
                    <div class="content">
                            <h1>Re-Roof<br>Projects</h1>
                            <hr>
                            <p>Roman is the company to call, even for the smallest roof repair work - our crews have seen it all!</p>
                            <a class="button outline" href="/roof-repairs">Read More</a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image:url(/images/bg-cleaning.jpg)">

                    <div class="content">
                            <h1>Roof<br>Maintenance</h1>
                            <hr>
                            <p>Preventative maintenance performed by Roman crews is the key to extending the lifetime of your roof.</p>
                            <a class="button outline" href="/roof-maintenance">Read More</a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="background-image:url(/images/bg-repair.jpg)">

                    <div class="content">
                            <h1>Leaks &amp;<br>Repairs</h1>
                            <hr>
                            <p>Our repairs division consists of subject matters experts in fixing roof leaks for good.</p>
                            <a class="button outline" href="/roof-repairs'">Read More</a>
                        </div>
                    </div>
            </div>
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
                <div class="swiper-pagination"></div>
            </div>
                <div class="swiper-container gallery-thumbs">
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><i class="fad fa-address-card"></i><p>Experience</p></div>
                    <div class="swiper-slide"><i class="fad fa-home"></i><p>Residential</p></div>
                    <div class="swiper-slide"><i class="fad fa-warehouse"></i><p>Commercial</p></div>
                    <div class="swiper-slide"><i class="fad fa-hard-hat"></i><p>Re-roofs</p></div>
                    <div class="swiper-slide"><i class="fad fa-tint-slash"></i><p>Maintenance</p></div>
                    <div class="swiper-slide"><i class="fad fa-house-flood"></i><p>Repairs</p></div>
                </div>
              </div>
            </div>
            <!-- Accolade Bar -->
            <section class="container-fluid" id="accolade-bar">
                <div class="row">
                    <div class="col">
                        <a href="https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736" target="_blank"><img src="/images/logos/bbb.png"></a>
                        <a href="needs_url"><img src="/images/logos/inc5000.png"></a>
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank"><img src="/images/logos/gaf.png"></a>
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank"><img src="/images/logos/gaftriple.png"></a>
                        <a href="https://www.gaf.com/en-us/roofing-contractors/residential/roman-roofing-inc-1108453" target="_blank"><img src="/images/logos/gahelite.png"></a>
                        <a href="https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736" target="_blank"><img src="/images/logos/angies.png"></a>
                        <a href="https://www.capecoralchamber.com/" target="_blank"><img src="/images/logos/chamber.png"></a>
                    </div>
                </div>
            </section>
            <!-- Rescue Section -->
            <section id="problem">
                <div class="content">
                    <img src="images/van.png"/>
                    <h1>It's Roman to the Rescue</h1>
                    <p>
                        Picking the wrong roofing contractor will inevitably lead to major problems for you and your property. &nbsp;It may lead to substantial water damage to your property and subsequent costly roofing repairs. &nbsp;As our customers know, Roman Roofing completes the job right the first time and on time. &nbsp; We are licensed, insured and most importantly a trusted, proven name. &nbsp;Our roofing jobs are backed by our satisfaction guarantee. &nbsp;Call Roman to rescue your roofing project today!
                    </p>
                </div>
            </section>

            <section id="stats">
                <div class="threeCol">
                    <div class="col one">
                        <i class="fad fa-glass fa-5x"></i>
                        <div class="stats">
                            <h4 class="count" data-count="1569861">0</h4>
                            <p>Zephyrhills Water<br>Bottles Consumed</p>
                        </div>
                    </div>
                    <div class="col two">
                        <i class="fad fa-home-lg-alt fa-5x"></i>
                        <div class="stats">
                            <h4 class="count" data-count="3155">0</h4>
                            <p>Re-Roofs Managed<br> on Schedule as Promised</p>
                        </div>
                    </div>
                    <div class="col three">
                        <i class="fad fa-comment-smile fa-5x"></i>
                        <div class="stats">
                            <h4 class="count" data-count="1837">0</h4>
                            <p>Delighted Customers<br> in 2019 Alone!</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="services" class="">
                <div class="contentWide">
                    <div class="threeCol">
                        <div class="col one">
                            <h3>Residential Roofing</h3>
                            <img src="images/services-residential-thumb.jpg"/> 
                            <div class="list">
                                <ul>
                                    <li><img src="images/icon-new-roofing.jpg"/> New Roofing</li>
                                    <li><img src="images/icon-roof-repairs.jpg"/> Roof Repairs</li>
                                    <li><img src="images/icon-roof-repairs.jpg"/> Roof Repairs</li>
                                </ul>
                            </div>
                            <img class="" src="images/col-shadow.png"/>
                            <p>Over 4,000 happy homeowners in Southwest Florida trust us for their roofing needs.</p>
                            <a href="/residential-roofing">Learn More</a>
                        </div>

                        <div class="col two">  <!-- why is there empty space here?? -->
                            <h3>Commercial Roofing</h3>
                            <img src="images/services-commercial-thumb.jpg"/> 
                            <div class="list">
                                <ul>
                                    <li><img src="images/icon-new-roofing.jpg"/> New Roofing</li>
                                    <li><img src="images/icon-roof-repairs.jpg"/> Roof Repairs</li>
                                    <li><img src="images/icon-maintenance.jpg"/> Maintenance</li>
                                </ul>
                            </div>
                            <img class="" src="images/col-shadow.png"/>
                            <p>From duplexes, churches and multi-story - we gave been there and done that.</p>
                            <a href="/commercial-roofing">Learn More</a>
                        </div>

                        <div class="col three">
                            <h3>Repairs & Maintenance</h3>
                            <img src="images/services-maintenance-thumb.jpg"/> 
                            <div class="list">
                                <ul>
                                    <li><img src="images/icon-residential-roofing.jpg"/> Residential</li>
                                    <li><img src="images/icon-commercial-roofing.jpg"/> Commercial</li>
                                    <li><img src="images/icon-industrial-roofing.jpg"/> Industrial</li>
                                </ul>
                            </div>
                            <img class="" src="images/col-shadow.png"/>
                            <p>Leaking roofs from deferred maintenance is a common scenario our trained technicans can identify and quickly resolve.</p>
                            <a href="/roof-repairs">Learn More</a>
                        </div>
                        <div class="clearer"></div>
                    </div>

                    <div class="guarantee">
                        <img src="images/roman-guarantee.png" />
                        <p>If you're not happy, we're not happy.</p>
                        <a href="/about">Read more</a>
                    </div>
                </div>
            </section>

            <section id="trusted" class="twoCol">
                <div class="contentWide">
                    <div class="left">
                        <img src="images/roman-instagram.png"/>
                    </div>

                    <div class="right">
                        <h2>We Are Southwest Florida's Go-To Roofing Contractor</h2>
                        <p>
              A family owned roofing company backed by crews who are true craftsmen of the trade, Roman is the name to trust for your roofing project - small or large, residential or commercial. &nbsp; services.  If there are issues, our Customer Guarantee ensures that we will resolve them to your satisfaction.
                        </p>
                        <p>Our 5-star reputation with the BBB and on-line review sites ensure you are working with only the best.</p>
                    </div>
                </div>
            </section>

            <section id="experience" class="twoCol">
                <div class="contentWide">
                    <div class="right">
                        <img src="images/jeff.png"/>
                    </div>

                    <div class="left">
                        <h2>200+ Years of Roofing Experience</h2>
                        <p>                           
              The Roman team consists of subject matter experts in roofing materials, engineering and applications. &nbsp;Unlike other contractors, our business is devoted exclusively to perfecting the art of roofing. &nbsp;We take exceptional pride in our craft - our customer reviews and accolades validate that Roman is Southwest Florida's go-to roofing expert.
                        </p>
                    </div>

                </div>
            </section>

            <section id="financing" class="twoCol">
                <div class="contentWide">
                    <div class="left">
                        <img src="images/finance.png"/>
                    </div>

                    <div class="right">
                        <h2>Finance Your Roofing Project with Roman</h2>
                        <p>
                            Let's face it...a new roof isn't always a luxury. More often than not, replacing your old roof ends up being a necessity or even an emergency, 
                            and due to budgets and cash flow, you might put off getting your roof done. 
                            Roman Roofing offers a variety of financing options tailored to fit your budget and lifestyle.
                        </p>
                        <a href="/financing">Read More</a>
                    </div>
                </div>
            </section>

            <section id="portfolio">
                <div class="gallery">
                    <a href="/portfolio" class="col one">
                        <img src="images/portfolio-thumb1.jpg" class="image "  alt="image" />
                        <div class="thumbnail">
                            <p class="search-icon"><i class="fa fa-search-plus"></i></p><br/>
                            <p class="bold-text">SEXY SPANISH SHINGLES</p><br/>
                            <p class="normal-text">CAPE CORAL MIAMI FLORIDA</p>
                        </div>
                    </a>

                    <a href="/portfolio" class="col two">
                        <img src="images/portfolio-7.jpg" class="image "  alt="image" />
                        <div class="thumbnail">
                            <p class="search-icon"><i class="fa fa-search-plus"></i></p><br/>
                            <p class="bold-text">SEXY SPANISH SHINGLES</p><br/>
                            <p class="normal-text">CAPE CORAL MIAMI FLORIDA</p>
                        </div>
                    </a>

                    <a href="/portfolio" class="col three">
                        <img src="images/portfolio-thumb3.jpg" class="image "  alt="image" />
                        <div class="thumbnail">
                            <p class="search-icon"><i class="fa fa-search-plus"></i></p><br/>
                            <p class="bold-text">SEXY SPANISH SHINGLES</p><br/>
                            <p class="normal-text">CAPE CORAL MIAMI FLORIDA</p>
                        </div>
                    </a>

                    <a href="images/portfolio-6.jpg" class="col four">
                        <img src="images/portfolio-thumb4.jpg" class="image "  alt="image" />
                        <div class="thumbnail">
                            <p class="search-icon"><i class="fa fa-search-plus"></i></p><br/>
                            <p class="bold-text">SEXY SPANISH SHINGLES</p><br/>
                            <p class="normal-text">CAPE CORAL MIAMI FLORIDA</p>
                        </div>
                    </a>
                </div>
                <div class="full">
                    <h3>View Our Full Portfolio</h3>
                    <a href="#" class="button outline">View Portfolio</a>
                </div>
            </section>

            <section id="reviews">
                <div class="content">
                    <h2>Our Customers Love The Roman Experience</h2>
                    <p>Read what your delighted neighbors are saying about why #RomanRocks in recent on-line reviews and throughout the social media stratosphere.</p>
                    <ul class="nav nav-justified nav-pills" id="tab-reviews" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="google-tab" data-toggle="tab" href="#google" role="tab" aria-controls="google" aria-selected="true">Google</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="bbb-tab" data-toggle="tab" href="#bbb" role="tab" aria-controls="profile" aria-selected="false">BBB</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="homeadvisor-tab" data-toggle="tab" href="#homeadvisor" role="tab" aria-controls="homeadvisor" aria-selected="false">Home Advisor</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="nextdoor-tab" data-toggle="tab" href="#nextdoor" role="tab" aria-controls="nextdoor" aria-selected="false">NextDoor</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="facebook-tab" data-toggle="tab" href="#facebook" role="tab" aria-controls="facebook" aria-selected="false">Facebook</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="yelp-tab" data-toggle="tab" href="#yelp" role="tab" aria-controls="yelp" aria-selected="false">Yelp</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="tab-reviews-content">
                      <div class="tab-pane fade show active" id="google" role="tabpanel" aria-labelledby="google-tab">
                        <div class="review">
                            <img src="/images/reviews/google-review.jpg">
                            <p class="name">Bobby C</p>
                            <div class="stars"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="date">August, 2019</p>
                            <blockquote>
                                They did a great job. Cleaned up each day, answered whatever questions you had, very affordable.  Ask for Clint when you call. Never thought getting a new roof could be so easy. They will not shuck and jive you, straightforward and upfront about all they can do for your roof.
                            </blockquote>
                            <a href="https://www.google.com/search?q=roman%20roofing&npsic=0&rflfq=1&rlha=0&rllag=32870511,-88277631,905483&tbm=lcl&rldimm=7924548479876440110&ved=2ahUKEwiA283_s5zkAhURWq0KHSGTCrkQvS4wAHoECAoQIA&rldoc=1&tbs=lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:14#lrd=0x88db4401079b8b9d:0x6df9a6db3c65a02e,1,,,&rlfi=hd:;si:7924548479876440110;mv:!1m2!1d39.819338099999996!2d-81.1978577!2m2!1d25.9216859!2d-95.3637174;tbs:lrf:!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:14" target="_blank">
                                Read more reviews on Google</a>
                        </div>
                      </div>

                      <div class="tab-pane fade show" id="bbb" role="tabpanel" aria-labelledby="bbb-tab">
                        <div class="review">
                            <img src="/images/reviews/bbb-review.png">
                            <p class="name">Tariq H.</p>
                            <div class="stars"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="date">August 6, 2019</p>
                            <blockquote>
                                Very happy with their work done would surely give them work again when required my roof looks very nice job done on time every thing went smooth staff was very help full they keep you in touch about job via phone and email before the work start.
                            </blockquote>
                            <a href="https://www.bbb.org/us/fl/cape-coral/profile/roofing-contractors/roman-roofing-inc-0653-90222736/customer-reviews" target="_blank">
                                Read more reviews on the Better Business Bureau Website</a>
                        </div>
                      </div>

                      <div class="tab-pane fade show" id="homeadvisor" role="tabpanel" aria-labelledby="homeadvisor-tab">
                        <div class="review">
                            <img src="/images/reviews/bbb-review.png">
                            <p class="name">Tom M. in Port Charlotte</p>
                            <div class="stars"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="date">May 1, 2019</p>
                            <blockquote>
                                I had Affordable Pavers seal my patio pavers, the crew did a great job. Second job by them, great company to work with. &nbsp;I would recommend them to everyone.
                            </blockquote>
                            <a href="https://www.homeadvisor.com/rated.RomanRoofingInc.67732579.html" target="_blank">
                                Read more reviews on HomeAdvisor</a>
                        </div>
                    </div>
                      <div class="tab-pane fade" id="homeadvisor" role="tabpanel" aria-labelledby="homeadvisor-tab">...</div>
                      <div class="tab-pane fade" id="nextdoor" role="tabpanel" aria-labelledby="nextdoor-tab">...</div>
                      <div class="tab-pane fade" id="facebook" role="tabpanel" aria-labelledby="facebook-tab">...</div>
                      <div class="tab-pane fade" id="yelp" role="tabpanel" aria-labelledby="yelp-tab">...</div>
                    </div>

                </div>
            </section>
@endsection
