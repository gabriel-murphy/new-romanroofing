@extends('layout')
@section('title','Maintenance')
@section('content')
    <body id="maintenance" class="inner">
@extends('navigation')
        <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>Roof Maintence & Inspections</h1>
                    <hr>
                    <p>Roman offers maintenance programs and comprensive inspections to avoid future water infiltrations in South Florida.</p>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h3>Roof Maintenance is Preventative Maintenance</h3>
                        <p>Wise property owners appreciate that proper maintenance of the systems of a property - including the part of the property that keeps everythign else dry and protected from the elements - is necessary to avoid future water leaks and costly repairs. &nbsp;A roof system is arguably the most vulnerable part of a building's exterior. Ultraviolet radiation, wind, rain, hail, snow, and sleet all affect a roof system's performance. &nbsp;Performance is based on good design, quality materials, proper installation, and a preventive-maintenance program. &nbsp;Roof maintenance is critical to preventing roof problems and keeping the roof in watertight condition. &nbsp;Early identification by Roman and repair of any roof issues will help provide a long-lasting roof system for your property.</p>
                        <div class="content-photo">
                            <img src="/images/roof-maintenance.jpg" style="height:300px;width:100%;">
                        </div>
                        <p>When a member of the Roman team inspects your roofing system, you can expect specific components of your roof will be examined close, including the flashing - which is the most common problematic area and most likely the cause for future roof problems. &nbsp;Roman crews are trained to conduct a 360 degree inspection of all flashing sites, such as skylights, perimeters, walls, penetrations, equipment curbs, and drain pipes. &nbsp;We do the same for the field of the roof, where we will look for curling, cracked and the (obvious) missing shingles or tiles. &nbsp;Less detectible is missing granules and peeling flashing, which is undetectable from the curb and requires direct inspection. &nbsp;Likewise with damaged drip edge, missing or exposed fasteners, and a number of other issues the Roman team is trained to identify and remedy. &nbsp;Finally, our maintenance program includes looking not just on top of the roof, but below it as well. &nbsp;The Roman crews are trained to get in the attic of the residence to aid in the detection of problematic areas and suggest ventalation options to reduce energy costs while extending the life of the roof by allowing it to breathe with proper ventalation.</p>
                    </div>
                    <div class="col-md-4" id="sidebar">
                        <div class="grey">
                            <p class="red">Roof Maintenance 101 </p>
                            <h4>Maintaining Your Roof Prevents Costly Roofing Repairs</h4>
                            <p>In the aftermath of Hurrican Irma, a roof maintenance program makes more sense than ever!</p>
                            <a class="btn btn-outline-secondary" href="/estimate.html">Get a No Cost Estimate</a>
                            <div class="panel">
                                <img src="images/roman-guarantee.png" />
                                <p>If you're not happy, we're not happy.</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>
@endsection
