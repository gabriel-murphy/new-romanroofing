@extends('layout')
@section('title','Roman Rescues Victims of Torando Touchdown in Northwest Cape Coral')
@section('content')
    <body id="about" class="inner">
@extends('navigation')
	  <div id="pageArea">
            <section id="showcase" class="showcase-sm">
                <div class="content">
                    <h1>Roman Rescues Victims of Torando Touchdown in Northwest Cape Coral</h1>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-9">
                        <article class="article">
                            <h3>Roman Rescues Victims of Torando Touchdown in Northwest Cape Coral</h3>
                            <p class="text-muted">October 20, 2019</p>
                            <p>At almost 7 am on Saturday, October 19, an EF-1 tornado riped through Northwest Cape Coral, leaving
                            a mile-long path of destruction.  Unlike hurricanes, tornados can drop from the sky - literally - with no
                            notice to homeowners to take cover. &nbsp;Fortunately, Roman was on the scene within hours, helping rescue
                            victims of this tragedy.
                          </p>
                            <a href="/about/news/2019/october/roman-rescues-homeowners-incurring-windstorm-damage-in-northwest-cape-coral" class="btn btn-outline-secondary">Read the Complete Story</a><br><br><br>
                        </article>
                        </div>
                        <div class="col-md-3">
                            <h5>Featured on:</h5>
                            <div class="list-group">
                              <a href="#" class="list-group-item list-group-item-action">
                                Fox 4 Florida <i class="fad fa-external-link float-right"></i>
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">Cape Coral News <i class="fad fa-external-link float-right"></i></a>
                              <a href="#" class="list-group-item list-group-item-action">Google Press Release <i class="fad fa-external-link float-right"></i></a>
                              <a href="#" class="list-group-item list-group-item-action">Another Source <i class="fad fa-external-link float-right"></i></a></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-9">
                        <article class="article">
                            <img src="/images/logos/inc5000.png">
                            <h3>Roman Named #152 on Inc. 5000 List of Fasting Growing Companies</h3>
                            <p class="text-muted">August 15, 2019</p>
                            <p>Inc. Magazine has released its annual list of the nation’s 5,000 fastest-growing private companies for 2018. &nbsp;Among them and ranked number 152 is Roman Roofing, Inc of Cape Coral, Florida. &nbsp;This coveted list represents a unique look at the most successful companies within the American economy’s most dynamic segment— its independent small businesses.</p>
                            <a href="/about/news/2019/august/roman-roofing-named-fastest-growing-roofing-contractor" class="btn btn-outline-secondary">Read the Complete Story</a><br><br><br>
                        </article>
                        </div>
                        <div class="col-md-3">
                            <h5>Featured on:</h5>
                            <div class="list-group">
                              <a href="#" class="list-group-item list-group-item-action">
                                Fox 4 Florida <i class="fad fa-external-link float-right"></i>
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">Cape Coral News <i class="fad fa-external-link float-right"></i></a>
                              <a href="#" class="list-group-item list-group-item-action">Google Press Release <i class="fad fa-external-link float-right"></i></a>
                              <a href="#" class="list-group-item list-group-item-action">Another Source <i class="fad fa-external-link float-right"></i></a></a>
                            </div>
                        </div>
                    </div>
            </section>
@endsection
