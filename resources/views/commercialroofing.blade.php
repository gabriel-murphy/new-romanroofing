@extends('layout')
@section('title','Commercial Roofing')
@section('content')
  <body id="commercial-page" class="inner">
@extends('navigation')
  <div id="pageArea">
            <section id="showcase" class="showcase-md">
                <div class="content">
                    <h1>Commercial Roofing</h1>
                    <hr/> 
                    <p>Re-Roofs and New Construction in South Florida</p>
                </div>
            </section>
            <section class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Roman Knows Commercial Roofing</h4>
                        <p>
                        The systems available to commercial building owners, the way these systems are installed vary from residential roofing applications. &nbsp;The problems that commercial building owners face and the solutions to their commercial roofing problems also vary from residential roofing applications. Many people don’t realize the differences in residential and commercial roofing applications, but it is important to realize these two industries are not the same, there are differences between them, and they should be handled differently.
                        </p>
                        <img src="/images/roman-roofing-commercial.jpg" class="content-photo">
                        <h5>Commercial versus Residential Applications</h5>
                        <p>One of the most glaring differences between commercial and residential building owners lies in the roofing systems available to them. Residential roofing systems are typically asphalt shingle, occasionally concrete tile, depending on the climate, and some other less popular materials. Asphalt shingle is far and away the most popular roofing option for residential roofing systems. &nbsp;Indeed, decision makers have substantially more options for the roofing system if they own or manage a commercial building.</p>

                          <p>Commercial roofing systems consist of sprayed polyurethane foam, restoration coatings, single-ply (TPO, PVC, EPDM), modified bitumen, concrete, built-up, tar and gravel, and many others. The bottom line, there are significantly more commercial roofing systems available than there are residential, which means you as a commercial building owner need to do more research before making purchase or repair decisions regarding your roof.</p>
                    </div>
                    <!--<div class="col-md-4 sidebar">
                      <h4>Commercial Roofing</h4>
                        <div class="card">
                          <img src="/images/apartments.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">Apartments & Condos</h5>
                            <p class="card-text">See our Case Study of Briarwood Village in Estero, Florida</p>
                          </div>
                        </div>
                        <div class="card">
                          <img src="/images/church.jpg" class="card-img-top" style="height:150px">
                          <div class="card-body">
                            <h5 class="card-title">Churches</h5>
                            <p class="card-text">See our Case Study of Grace Church in Port Charlotte, Florida</p>
                          </div>
                        </div>
                        <br><br>
                        <a class="btn btn-outline-secondary btn-block" href="/estimate">Get No Cost Estimate</a>
                      
                    </div>
                  -->
                </div>
            </section>
@endsection
