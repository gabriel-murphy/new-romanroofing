@extends('layouts.frontend')

@section('content')
    @include('includes.partials.inner-page-banner')

    <div class="about-us section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ About Us ]</h6>
                <h1 class="section-heading text-center">Who We Are</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center m-b-60">
                Roman is Southwest Florida's trusted experts at roofing - residential or commercial projects and of all
                sizes. We are a multi year award recipient from the most trusted and reputable roofing manufacturers in
                North America, including GAF and Owens Corning, to name a few. Our growth is a consequence of our
                commitment to project management excellence and consistent training. Roman is also one of a few
                recipients of <strong>GAF's Triple Excellence Award and President Club recipients in Southwest
                    Florida.</strong>
            </p>
        </div>
    </div>

    <div class="accolades section">
        <div class="container-fluid">
            <div class="section-header m-b-70">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Our Accolades ]</h6>
                <h1 class="section-heading text-center">Exceeding Expectations Since 2015</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="accolade-images full-width">
                <div class="row no-gutters">
                    @for($i = 1; $i <= 3; $i++)
                        <div class="col-md-4">
                            <img class="img-fluid w-100"
                                 src="{{asset('frontend/images/about-us/accolade-'."{$i}".'.jpg')}}"
                                 alt="accolade-{{$i}}.jpg">
                        </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>

    <div class="trust">
        <div class="container-fluid">
            <div class="section-header m-b-70">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Trust ]</h6>
                <h1 class="section-heading text-center">Tagline Needs to be Added</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="family-owned-trust full-width">
                <div class="row no-gutters">
                    <div class="col-12 col-lg-8 order-2 order-lg-1">
                        <div class="sub-section center-vertically section-paragraph bg-grey">
                            <div class="content-wrapper">
                                <h3 class="heading-md primary-black mb-4">Family Owned and Operated</h3>
                                <p>
                                    Those in the Roman family appreciate the generosity of the ownership, and subscribe
                                    to the
                                    vision of its co-founder and Chief Executive Officer, Norm Dopfer. Named after his
                                    youngest
                                    son pictured to the right, Roman has quickly became a brand synonimous with trust
                                    and
                                    excellence in the rendition of roofing services in the Southwest Florida area. Mrs.
                                    Dopfer
                                    is the firms Chief Financial Officer and mother of two. With their Midwest-driven
                                    hustle and
                                    drive for perfection, they engage with and lead the management and crews of Roman
                                    Roofing,
                                    Inc.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 order-1 order-lg-2">
                        <img class="img-full-width-height object-fit-cover"
                             src="{{asset('frontend/images/about-us/kid.jpg')}}" alt="kid.jpg">
                    </div>
                </div>
                <div class="row flex-row-reverse no-gutters">
                    <div class="col-12 col-lg-8 order-2 order-lg-1">
                        <div class="sub-section center-vertically section-paragraph">
                            <div class="content-wrapper">
                                <h3 class="heading-md primary-black mb-4">Roman, A Name You Can Trust</h3>
                                <p>
                                    The Roman management consists of subject matter experts of the roofing trade - the
                                    crews which they lead consists of highly trained and experienced craftsman who take
                                    pride in their work and consistently demonstrate the upmost respect for our
                                    customers' property. Indeed, with over 100 employees and dozens of crews, our
                                    name carries over 170 years of year-round roofing experience. It's just one of the
                                    many reasons why homeowners, property owners and realtors put their name trust in
                                    Roman for anything roofing.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 order-1 order-lg-2">
                        <img class="img-full-width-height object-fit-cover"
                             src="{{asset('frontend/images/about-us/crew.jpg')}}" alt="crew.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="founding-history section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Founding Partners ]</h6>
                <h1 class="section-heading text-center">Tagline Needs to be Added</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aut cum dicta ducimus
                eligendi excepturi id laudantium minima molestiae, nam praesentium quaerat ullam. Accusantium aliquam
                atque beatae, consequatur consequuntur eos facere labore maxime necessitatibus quis, quo ratione saepe
                sint suscipit veniam vitae voluptas. Accusantium aperiam, delectus ea libero quia quo repudiandae sit.
                Animi, consequuntur maxime reprehenderit sit veniam vitae.
            </p>
        </div>
    </div>

    <div class="founders section">
        <div class="container">
           <div class="mb-5">
               <div class="row">
                   <div class="col-md-4">
                       <div class="founder-img mb-4 mb-md-0 text-center text-md-left">
                           <img src="{{asset('frontend/images/about-us/Team/norm.png')}}" alt="norm.png">
                       </div>
                   </div>
                   <div class="col-md-8">
                       <div class="founder-info">
                           <h3 class="heading-md primary-black">Norm Dopfer ("ND")</h3>
                           <p>Co-Founder & CEO</p>
                           <p class="section-paragraph">
                               Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias aliquid animi cumque
                               delectus deleniti dignissimos distinctio et ex facere facilis fugiat incidunt laudantium
                               magni minima nam, nisi obcaecati optio pariatur porro quia tenetur unde ut. Eaque eum
                               ipsam placeat?
                           </p>
                       </div>
                   </div>
               </div>
           </div>
            <div class="row">
                <div class="col-md-8 order-2 order-md-1">
                    <div class="founder-info">
                        <h3 class="heading-md primary-black">Lindsey Dopfer ("LD")</h3>
                        <p>Co-Founder & CEO</p>
                        <p class="section-paragraph">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias aliquid animi cumque
                            delectus deleniti dignissimos distinctio et ex facere facilis fugiat incidunt laudantium
                            magni minima nam, nisi obcaecati optio pariatur porro quia tenetur unde ut. Eaque eum
                            ipsam placeat?
                        </p>
                    </div>
                </div>
                <div class="col-md-4 order-1 order-md-2">
                    <div class="founder-img mb-4 mb-md-0 text-center text-md-right">
                        <img src="{{asset('frontend/images/about-us/Team/lindsey.png')}}" alt="norm.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="team section bg-grey">
        <div class="container">
            <div class="section-header m-b-70">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Our Team ]</h6>
                <h1 class="section-heading text-center">Meet Our Top Notch Team of Expert Roofers</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="team-carousel owl-carousel owl-theme">
               @foreach($api_response as $response)
                    <div class="item">
                        <div class="card">
                            <img class="card-img-top" src="@if(isset($response['avatar_image_location'])){{ $response['avatar_image_location'] }}@else {{ asset('images/avatars/man-1.svg') }} @endif" alt="employee-{{$i}}.png">
                            <div class="card-body">
                                <h4 class="card-title primary-black heading-sm">{{ $response['first_name'] }} {{ $response['last_name'] }}</h4>
                                <p class="card-text primary-grey">{{ $response['title'] }}</p>
                            </div>
                        </div>
                    </div>
               @endforeach
            </div>
        </div>
    </div>


@endsection
