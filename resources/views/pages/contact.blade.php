@extends('layouts.frontend')

@section('content')
    @include('includes.partials.inner-page-banner')

    <div class="contact section">
        <div class="container">
            <div class="contact-form-wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-us-left">
                            <h1 class="heading-md primary-black mb-5">Let's Talk Together</h1>
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="full_name">Full Name</label>
                                            <input type="text" class="form-control" placeholder="" id="full_name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone_number">Phone Number</label>
                                            <input type="text" class="form-control" placeholder="" id="phone_number">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="email">Email Address</label>
                                            <input type="email" class="form-control" placeholder="" id="email">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="comment">Question, Comment or Request</label>
                                            <textarea class="form-control resize-none" rows="8" id="comment"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn-primary-roman mt-5">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-us-right">
                            <div class="map mb-3">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3565.446371097138!2d-81.95893704877332!3d26.66620298315214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88db4401079b8b9d%3A0x6df9a6db3c65a02e!2sRoman+Roofing!5e0!3m2!1sen!2sus!4v1565644959645!5m2!1sen!2sus"
                                    width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                            </div>
                            <div class="contact-options">
                                <div class="d-flex justify-content-between">
                                    <div class="contact-phone mb-3">
                                        <a href="#"><i class="fas fa-phone-alt"></i>(239) 458-7663</a>
                                    </div>
                                    <div class="contact-email mb-3">
                                        <a href="#"><i class="fas fa-envelope"></i>help@roman-roofing.com</a>
                                    </div>
                                </div>
                                <div class="contact-address">
                                    <a href="#"><i class="fas fa-map-marker-alt"></i>921 NE 27th Ln, Cape Coral, FL 33909</a>
                                </div>
                            </div>
                            <div class="map-directions-btn">
                                <a class="btn-primary-roman" href="https://maps.google.com/maps?ll=26.666203,-81.956743&amp;z=16&amp;t=m&amp;hl=en&amp;gl=US&amp;mapclient=embed&amp;daddr=Roman%20Roofing%20805%20NE%207th%20Terrace%20Cape%20Coral%2C%20FL%2033909@26.666203,-81.956743" target="_blank">Get directions</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
