@extends('layouts.frontend')

@section('content')

    @include('includes.partials.inner-page-banner')
    <div class="estimate section">
        <div class="container">
            <form>
                <h1 class="heading-md primary-black mb-5 text-center">Tell Roman About Your Roofing Project</h1>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary-roman w-100 dropdown-toggle" data-toggle="dropdown">
                                Type of Roofing Project
                            </button>
                            <div class="dropdown-menu text-center">
                                <a class="dropdown-item" href="#">New Construction</a>
                                <a class="dropdown-item" href="#">Re-Roof Project</a>
                                <a class="dropdown-item" href="#">Roof Repair</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="dropdown">
                            <button type="button" class="btn btn-primary-roman w-100 dropdown-toggle" data-toggle="dropdown">
                                Type of Roofing Materials
                            </button>
                            <div class="dropdown-menu text-center">
                                <div class="roof-material">
                                    <div class="material-type">
                                        <input id="shingle" type="checkbox">
                                        <label for="shingle">Shingle</label>
                                    </div>
                                </div>
                                <div class="roof-material">
                                    <div class="material-type">
                                        <input id="metal" type="checkbox">
                                        <label for="metal">Metal</label>
                                    </div>
                                </div>
                                <div class="roof-material">
                                    <div class="material-type">
                                        <input id="concrete" type="checkbox">
                                        <label for="concrete">Concrete</label>
                                    </div>
                                </div>
                                <div class="roof-material">
                                    <div class="material-type">
                                        <input id="solar" type="checkbox">
                                        <label for="solar">Solar</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" placeholder="" id="first_name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" placeholder="" id="last_name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" placeholder="" id="email">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control" placeholder="" id="phone_number">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="project_address">Project Address</label>
                            <input type="text" class="form-control" placeholder="" id="project_address">
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn-primary-roman mt-5">Request No Cost Evaluation</button>
                </div>
            </form>
        </div>
    </div>

@endsection
