@extends('layouts.frontend')

@section('content')
    @include('includes.partials.inner-page-banner')

    <div class="jobs section">
        <div class="container">
            <div class="jobs-wrapper">
                <div id="job_card">
                    @for($i = 0; $i < 4; $i++)
                        <div class="card">
                            <div class="card-header">
                                <div class="job-description">
                                    <h3 class="primary-black fw-600">Commercial Roofing Crew & Labors Needed</h3>
                                    <p class="section-paragraph mb-5">
                                        Roman Roofing - recently named Southwest Florida's fastest growing roofing
                                        contractor - has immediate openings for a commercial roofing crew, which consists of
                                        four (4) team members to form a crew to join the dozens of crews and 100+
                                        hard-working team members in the Roman Roofing family.
                                    </p>
                                </div>
                                <button id="apply-btn-{{$i}}" class="btn-primary-roman apply-now-btn collapsed" data-toggle="collapse" data-target="#collapse-{{$i}}"
                                        aria-expanded="false" aria-controls="collapse-{{$i}}">
                                    Apply Now
                                </button>
                            </div>
                            <div id="collapse-{{$i}}" class="collapse" data-parent="#job_card">
                                <div class="card-body">
                                    <form id="job_form-{{$i}}" method="POST">
                                        @csrf
                                        <h3 class="primary-black fw-600 mb-5">Fill Out the Form Below</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="f_name" class="form-control mb-3" placeholder="First Name" required>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="l_name" class="form-control mb-3" placeholder="Last Name" required>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="email" name="user_email" class="form-control mb-3" placeholder="Email Address" required>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="user_number" class="form-control mb-3" placeholder="Phone Number" required>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary-roman submit-job-form mt-3 mr-3">Submit</button>
                                        <button type="button" class="btn btn-primary-roman cancel-job-form bg-secondary border-secondary mt-3">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endfor

                </div>
            </div>
        </div>
    </div>
@endsection
