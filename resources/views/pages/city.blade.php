@extends('layouts.frontend')

@section('content')
    @include('includes.partials.inner-page-banner')
    <div class="city-intro section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Roman in Sanibel ]</h6>
                <h1 class="section-heading text-center">Best Roofing Contractor in Sanibel</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="city-service-info">
                <div class="row">
                    <div class="col-md-4">
                        <div class="logo-sanibel">
                            <img class="img-fluid" src="{{ asset('images/logos/logo-sanibal.png') }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <p class="section-paragraph m-b-60">
                            Roman Roofing has operated in Sanibel, FL since 2015. Since then, we have provided Sanibel
                            residents
                            with unsurpassed roofing expertise season after season. We specialize in tile roofs, shingle
                            roofs,
                            metal roofs, and commercial roofs.With humble beginnings, we fully understand that replacing
                            a roof in
                            rainy Southwest Florida is a large undertaking. Roman Roofing is here to help make your
                            roofing
                            experience as seamless as possible. Our roofing experts have completed countless reroofs,
                            repairs, and
                            commercial roofing projects for homeowners, business owners, and everyone in between.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="city-background">
        <div class="parallax-img-city"
             style="background-image: url('{{asset('frontend/images/backgrounds/map-sanibel.png')}}')"></div>
    </div>

    <div class="city-roofing-types section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Roofing Types in Sanibel ]</h6>
                <h1 class="section-heading text-center">Sanibel Roofing Types</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="mb-5">
                <h2 class="heading-md primary-black mb-4">Commercial Roofing</h2>
                <p class="section-paragraph">
                    We have a commercial department that is dedicated to managing all of our commercial projects, so you
                    can
                    worry about what matters most, your business. Roman Roofing is proud to help the Sanibel business
                    community with all of their roofing needs.
                </p>
            </div>
            <h2 class="heading-md primary-black mb-4">Residential Roofing</h2>
            <p class="section-paragraph">
                Our flagship service is the most popular choice for any aging roof. Our accolades speak volumes and we
                are so proud to protect you from the elements. We are proud to offer our residential roofing services to
                the great community of Sanibel.
            </p>
        </div>
    </div>

    <div class="city-roofing-varieties section">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Roofing Varieties in Sanibel ]</h6>
                <h1 class="section-heading text-center">Sanibel Roofing Varieties</h1>
                <div class="fancy-border mb-4"></div>
                <p class="section-paragraph text-center">
                    Roman Roofing is happy to offer a variety of roofing options for Sanibel homeowners and business
                    owners.
                    Take a look for yourself!
                </p>
            </div>

        </div>
        <div class="city-roofing-variety-cards">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="{{asset('frontend/images/city/type-tile.jpg')}}"
                                 alt="type-tile.jpg">
                            <div class="card-body">
                                <h5 class="card-title heading-md">Tile Roof</h5>
                                <p class="service-card-description">
                                    Tile roofs are a popular choice in Sanibel and often come in many shapes, sizes, and
                                    color options. From new installation to repairs we can service your tile roof in all
                                    facets.
                                </p>
                                <div class="notes">
                                    <p class="service-card-description fw-600">Things to consider:</p>
                                    <ul class="reset-list">
                                        <li>Color options</li>
                                        <li>Lead times</li>
                                        <li>Tile Manufacturer</li>
                                        <li>Finishes</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="{{asset('frontend/images/city/type-shingle.jpg')}}"
                                 alt="type-shingle.jpg">
                            <div class="card-body">
                                <h5 class="card-title heading-md">Shingle Roof</h5>
                                <p class="service-card-description">
                                    Roman Roofing is fully equipped to repair or replace your shingle roof. We have a
                                    large selection of quality shingles boasting amazing warranties to give you peace of
                                    mind.
                                </p>
                                <div class="notes">
                                    <p class="service-card-description fw-600">Things to consider:</p>
                                    <ul class="reset-list">
                                        <li>Shingle Color</li>
                                        <li>Shingle Style</li>
                                        <li>Shingle Manufacturer</li>
                                        <li>Shingle Warranty</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="{{asset('frontend/images/city/type-metal.jpg')}}"
                                 alt="type-metal.jpg">
                            <div class="card-body">
                                <h5 class="card-title heading-md">Metal Roof</h5>
                                <p class="service-card-description">
                                    We have installed metal roofs all over Southwest Florida. From 5V to standing seam,
                                    colors or no colors, we can order the perfect metal package that protects your home
                                    and looks great!
                                </p>
                                <div class="notes">
                                    <p class="service-card-description fw-600">Things to consider:</p>
                                    <ul class="reset-list">
                                        <li>Metal Color</li>
                                        <li>Metal Style</li>
                                        <li>Metal Manufacturer</li>
                                        <li>Metal Warranty</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="{{asset('frontend/images/city/type-solar.jpg')}}"
                                 alt="type-metal.jpg">
                            <div class="card-body">
                                <h5 class="card-title heading-md">Solar Roof</h5>
                                <p class="service-card-description">
                                    We have installed metal roofs all over Southwest Florida. From 5V to standing seam,
                                    colors or no colors, we can order the perfect metal package that protects your home
                                    and looks great!
                                </p>
                                <div class="notes">
                                    <p class="service-card-description fw-600">Things to consider:</p>
                                    <ul class="reset-list">
                                        <li>Solar Quality</li>
                                        <li>Solar Quality</li>
                                        <li>Solar Quality</li>
                                        <li>Solar Quality</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="city-services section bg-grey">
        <div class="section-header">
            <h6 class="fw-600 clr-primary-maroon text-center">[ Available Services in Sanibel ]</h6>
            <h1 class="section-heading text-center">Check Out Our Sanibel Roofing Services</h1>
            <div class="fancy-border"></div>
        </div>
        <div class="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="service-type">
                            <a href="{{ route('service') }}">
                                <i class="fas fa-home-lg" aria-hidden="true"></i>
                                <span>Reroofing</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="service-type">
                            <a href="{{ route('service') }}">
                                <i class="fas fa-hammer" aria-hidden="true"></i>
                                <span>Roof Repair</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="service-type">
                            <a href="{{ route('service') }}">
                                <i class="fas fa-tools" aria-hidden="true"></i>
                                <span>Maintenance</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="service-type">
                            <a href="{{ route('service') }}">
                                <i class="fas fa-house-return" aria-hidden="true"></i>
                                <span>New Roofing</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="city-service-reviews section">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Reviews in Sanibel ]</h6>
                <h1 class="section-heading text-center">Sanibel Customers Love Our Work</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="city-reviews-carousel owl-carousel owl-theme">
                @for($i=0; $i < 5; $i++)
                    <div class="item">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="city-job-bio">
                                    <div class="city-job-img">
                                        <img class="img-fluid"
                                             src="{{ asset('frontend/images/city/reviews/review-solar.jpg') }}"
                                             alt="review-solar.jpg">
                                    </div>
                                    <div class="city-manufacturer-info">
                                        <div class="product-manufacturer mb-3">
                                            <img src="{{asset('frontend/images/city/reviews/gaf.png')}}" data-toggle="tooltip" data-placement="top" title="GAF" alt="gaf.png">
{{--                                            <span class="primary-grey ml-2">GAF</span>--}}
                                        </div>
                                        <div class="product-type mb-3">
                                            <img class="icon-timberline" data-toggle="tooltip" data-placement="top" title="TimberlineHDZ"
                                                 src="{{asset('frontend/images/city/reviews/icon-timberlineHDZ.png')}}"
                                                 alt="icon-timberlineHDZ.png">
{{--                                            <span class="primary-grey ml-2">TimberlineHDZ</span>--}}
                                        </div>
                                        <div class="product-color">
                                            <img src="{{asset('frontend/images/city/reviews/dummy-shingle.jpg')}}" data-toggle="tooltip" data-placement="top" title="Brown"
                                                 alt="dummy-shingle.png">
{{--                                            <span class="primary-grey ml-2">Brown</span>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="primary-black fw-600">Murphy Job - 1323 37th Southeast Florida</h4>
                                <p class="section-paragraph mb-5">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium adipisci aliquam
                                    amet at aut, consequatur doloremque ducimus eius enim est eveniet exercitationem facilis
                                    fuga ipsa ipsum laborum libero maiores natus nihil nostrum perspiciatis, placeat
                                    possimus quasi quidem ratione reprehenderit repudiandae saepe tempore ullam ut vero
                                    voluptatibus voluptatum. Deserunt, officia.
                                </p>
                                <div class="reviewer-info">
                                    <div class="media">
                                        <img src="{{ asset('frontend/images/city/reviews/dummy-user.jpg') }}" alt="dummy-user.jpg">
                                        <div class="media-body">
                                            <h4 class="primary-black fw-600 mt-0 mb-1">Reviewer Name</h4>
                                            <h5 class="primary-grey">Designation</h5>
                                            <div class="stars">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="review-source">
                                        <h2 class="heading-sm primary-black fw-600">Review Source: Google</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>

@endsection
