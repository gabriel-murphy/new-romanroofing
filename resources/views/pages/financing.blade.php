@extends('layouts.frontend')

@section('content')
    @include('includes.partials.inner-page-banner')

    <div class="section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Finance ]</h6>
                <h1 class="section-heading text-center">Finance Your Project</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center m-b-60">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur labore natus non
                officiis
                quo similique soluta! Animi aperiam consequuntur deserunt dignissimos dolorem eligendi est et fugit
                inventore iure magni neque non omnis ratione reprehenderit temporibus, voluptates! Dolorem ducimus esse
                quae
                repellendus saepe sapiente. Dignissimos facere itaque modi officia praesentium?
            </p>
           <div class="text-center">
               <a class="btn-primary-roman" href="https://prequalification.ygrene.com/prequal" target="_blank">Start Process</a>
           </div>
        </div>
    </div>

    <div class="financing section">
        <div class="container">
            <div class="financing-wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left-img">
                            <img src="{{asset('frontend/images/financing/calculator.jpg')}}" alt="calculator.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="left-finance-img-description">
                            <h2 class="heading-md primary-black mb-4">Reasonable Financing Options from Roman</h2>
                            <p class="section-paragraph text-justify mb-5">Roman has strong relationships with finance
                                companies offering affordable financing solutions for your roofing project, including
                                YGrene and other top lenders. Schedule a no cost evaluation with one of our scheduling
                                coordinators now by calling Roman at (239) 458-7663.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="active-returns section bg-grey">
        <div class="container">
            <div class="active-returns-wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="right-finance-img-description">
                            <h2 class="heading-md primary-black mb-4">Attractive Returns</h2>
                            <p class="section-paragraph text-justify mb-5">Putting money into your home can be an
                                excellent investment with an attractive return. The National Association of REALTORS
                                indicates that a roofing investment can return 105% of the value of the project upon
                                sale of a home.*
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right-img">
                            <img src="{{asset('frontend/images/financing/active-returns.jpg')}}"
                                 alt="active-returns.jpg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="low-payments section">
        <div class="container">
            <div class="low-payments-wrapper">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left-img">
                            <img src="{{asset('frontend/images/financing/low-payments.jpg')}}" alt="low-payments.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="left-finance-img-description">
                            <h2 class="heading-md primary-black mb-4">Low Monthly Payments</h2>
                            <p class="section-paragraph text-justify mb-5">As an example, an unsecured home improvement
                                loan of $10,000 with a term of 60 months with an APR of 10% monthly payment could be as
                                low as $212.47** for applicants with excellent credit. Talk to your contractor about the
                                specifics of your project financing.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="advantages section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Advantages ]</h6>
                <h1 class="section-heading text-center">What You Get</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center m-b-60">
                Roman Roofing Provides financing options that allow property owners to leverage their home's equity with
                fast qualification and
                payments customized to your income and lifestyle.
            </p>
            <div class="advantages-wrapper">
                <div class="row">
                    <div class="col-md-4">
                        <div class="advantage-box">
                            <div class="advantage-icon">
                                <i class="fad fa-home" aria-hidden="true"></i>
                            </div>
                            <div class="advantage-title">
                                <h4>Use Property Equity</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="advantage-box">
                            <div class="advantage-icon">
                                <i class="fad fa-money-check" aria-hidden="true"></i>
                            </div>
                            <div class="advantage-title">
                                <h4>Fast Qualification</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="advantage-box">
                            <div class="advantage-icon">
                                <i class="fad fa-money-bill-wave"></i>
                            </div>
                            <div class="advantage-title">
                                <h4>Easy Payments</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
