@extends('layouts.frontend')

@section('content')

    @include('includes.partials.inner-page-banner')

    <div class="section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Charity ]</h6>
                <h1 class="section-heading text-center">Roman Gives Back</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur labore natus non
                officiis
                quo similique soluta! Animi aperiam consequuntur deserunt dignissimos dolorem eligendi est et fugit
                inventore iure magni neque non omnis ratione reprehenderit temporibus, voluptates! Dolorem ducimus esse
                quae
                repellendus saepe sapiente. Dignissimos facere itaque modi officia praesentium?
            </p>
        </div>
    </div>

    <div class="charity section-1 section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="charity-img">
                        <img class="img-fluid" src="{{ asset('frontend/images/charity.jpg') }}" alt="charity.jpg">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="charity-info">
                        <h2 class="heading-md primary-black mb-4">Roman Cares</h2>
                        <p class="section-paragraph text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium blanditiis consequatur
                            corporis earum impedit in ipsam laborum minima minus modi nemo neque numquam optio
                            praesentium quasi quidem quo rerum, suscipit tempore temporibus. A, adipisci aliquam cumque
                            debitis doloribus earum fugit, in ipsa neque nobis provident quasi quis tempore voluptate
                            voluptatem.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="charity section-2 section bg-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="charity-info">
                        <h2 class="heading-md primary-black mb-4">Roman Cares</h2>
                        <p class="section-paragraph text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium blanditiis consequatur
                            corporis earum impedit in ipsam laborum minima minus modi nemo neque numquam optio
                            praesentium quasi quidem quo rerum, suscipit tempore temporibus. A, adipisci aliquam cumque
                            debitis doloribus earum fugit, in ipsa neque nobis provident quasi quis tempore voluptate
                            voluptatem.
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="charity-img">
                        <img class="img-fluid" src="{{ asset('frontend/images/charity.jpg') }}" alt="charity.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="charity section-1 section last-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="charity-img">
                        <img class="img-fluid" src="{{ asset('frontend/images/charity.jpg') }}" alt="charity.jpg">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="charity-info">
                        <h2 class="heading-md primary-black mb-4">Roman Cares</h2>
                        <p class="section-paragraph text-justify">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium blanditiis consequatur
                            corporis earum impedit in ipsam laborum minima minus modi nemo neque numquam optio
                            praesentium quasi quidem quo rerum, suscipit tempore temporibus. A, adipisci aliquam cumque
                            debitis doloribus earum fugit, in ipsa neque nobis provident quasi quis tempore voluptate
                            voluptatem.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
