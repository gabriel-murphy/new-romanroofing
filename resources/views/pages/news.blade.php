@extends('layouts.frontend')

@section('content')
    <div class="news-bar m-0 text-center">
        <a href="#"><span class="text-uppercase">Breaking News:</span> An EF-1 tornado riped through Northwest Cape
            Coral Northwest Cape Coral</a>
    </div>
    <div class="news full-width">
        <div class="container-fluid p-0">
            <div class="news-carousel owl-carousel owl-theme">
                <div class="item gradient-black-light">
                    <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-1.jpg') }}"
                         alt="news-banner-1.jpg">
                    <div class="news-content-lg">
                        <a href="#">
                            <h4 class="text-white text-uppercase fw-700 mb-3">An EF-1 tornado riped through Northwest
                                Cape Coral Northwest Cape Coral</h4>
                        </a>
                        <p class="mb-0 text-white fw-600"><span class="mr-2"><i class="far fa-clock"></i></span> August
                            31, 2020</p>
                    </div>
                </div>
                <div class="item gradient-black-light">
                    <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-2.jpg') }}"
                         alt="news-banner-2.jpg">
                    <div class="news-content-lg">
                        <a href="#">
                            <h4 class="text-white text-uppercase fw-700 mb-3">An EF-1 tornado riped through Northwest
                                Cape Coral Northwest Cape Coral</h4>
                        </a>
                        <p class="mb-0 text-white fw-600"><span class="mr-2"><i class="far fa-clock"></i></span> August
                            31, 2020</p>
                    </div>
                </div>
                <div class="item gradient-black-light">
                    <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-1.jpg') }}"
                         alt="news-banner-1.jpg">
                    <div class="news-content-lg">
                        <a href="#">
                            <h4 class="text-white text-uppercase fw-700 mb-3">An EF-1 tornado riped through Northwest
                                Cape Coral Northwest Cape Coral</h4>
                        </a>
                        <p class="mb-0 text-white fw-600"><span class="mr-2"><i class="far fa-clock"></i></span> August
                            31, 2020</p>
                    </div>
                </div>
                <div class="item gradient-black-light">
                    <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-2.jpg') }}"
                         alt="news-banner-2.jpg">
                    <div class="news-content-lg">
                        <a href="#">
                            <h4 class="text-white text-uppercase fw-700 mb-3">An EF-1 tornado riped through Northwest
                                Cape Coral Northwest Cape Coral</h4>
                        </a>
                        <p class="mb-0 text-white fw-600"><span class="mr-2"><i class="far fa-clock"></i></span> August
                            31, 2020</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="news-wrapper section p-t-40">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="news-left-top">
                        <div class="news-bar m-0 m-b-20">
                            <h6 class="mb-0">Latest News</h6>
                        </div>
                        @for($i = 0; $i < 4; $i++)
                            <div class="news-box">
                                <div class="row no-gutters">
                                    <div class="col-md-4 d-flex">
                                        <div class="thumbnail d-flex">
                                            <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-1.jpg') }}"
                                                 alt="news-banner-1.jpg">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="news-summary">
                                            <h6 class="fs-18 fw-600 primary-black">An EF-1 tornado riped through
                                                Northwest Cape Coral Northwest Cape Coral</h6>
                                            <p class="primary-grey fs-14"><span class="mr-2"><i
                                                        class="far fa-clock"></i></span> August 31, 2020</p>
                                            <p class="mb-5 primary-grey news-overview">At almost 7 am on Saturday, October 19, an EF-1 tornado riped
                                                through Northwest Cape Coral, leaving a mile-long path of destruction.
                                                Unlike hurricanes, tornados can literally drop from the sky, with no notice
                                                to homeowners to seek cover. Fortunately, Roman was on the scene within
                                                hours, helping rescue victims of this tragedy and providing volunteer
                                                services for clean-up of the path of destruction.</p>
                                            <a class="btn-primary-roman" href="#">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endfor
                    </div>
                    <hr class="m-y-32">
                    <div class="news-left-bottom">
                        <div class="news-bar m-0 m-b-20">
                            <h6 class="mb-0">Popular Posts</h6>
                        </div>
                        <div class="popular-post__full-width gradient-black-light">
                            <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-2.jpg') }}" alt="news-banner-2.jpg">
                            <div class="news-content-lg">
                                <a href="#">
                                    <h4 class="text-white text-uppercase fw-700 mb-3">An EF-1 tornado riped through Northwest
                                        Cape Coral Northwest Cape Coral</h4>
                                </a>
                                <p class="mb-0 text-white fw-600"><span class="mr-2"><i class="far fa-clock"></i></span> August
                                    31, 2020</p>
                            </div>
                        </div>
                        <div class="popular-post__two-col">
                            <div class="row no-gutters">
                                <div class="col-md-6">
                                    <div class="popular-post__col-half m-r-10">
                                        <div class="popular-post__col-half-img m-b-20 gradient-black-light">
                                            <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-2.jpg') }}" alt="news-banner-2.jpg">
                                        </div>
                                        <div class="popular-post__col-half-content">
                                            <h6 class="primary-black fw-600">An EF-1 tornado riped through Northwest Cape Coral Northwest Cape Coral</h6>
                                            <p class="primary-grey fs-14"><span class="mr-2"><i class="far fa-clock"></i></span> August
                                                31, 2020</p>
                                            <p class="mb-4 primary-grey text-justify fs-14">At almost 7 am on Saturday, October 19, an EF-1 tornado riped
                                                through Northwest Cape Coral, leaving a mile-long path of destruction.
                                                Unlike hurricanes, tornados can literally drop from the sky, with no notice
                                                to homeowners to seek cover. Fortunately, Roman was on the scene within
                                                hours, helping rescue victims of this tragedy and providing volunteer
                                                services for clean-up of the path of destruction.</p>
                                            <a class="btn-primary-roman p-x-15 p-y-5 fs-14" href="#">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="popular-post__col-half m-l-10">
                                        <div class="popular-post__col-half-img m-b-20 gradient-black-light">
                                            <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-2.jpg') }}" alt="news-banner-2.jpg">
                                        </div>
                                        <div class="popular-post__col-half-content">
                                            <h6 class="primary-black fw-600">An EF-1 tornado riped through Northwest Cape Coral Northwest Cape Coral</h6>
                                            <p class="primary-grey fs-14"><span class="mr-2"><i class="far fa-clock"></i></span> August
                                                31, 2020</p>
                                            <p class="mb-4 primary-grey text-justify fs-14">At almost 7 am on Saturday, October 19, an EF-1 tornado riped
                                                through Northwest Cape Coral, leaving a mile-long path of destruction.
                                                Unlike hurricanes, tornados can literally drop from the sky, with no notice
                                                to homeowners to seek cover. Fortunately, Roman was on the scene within
                                                hours, helping rescue victims of this tragedy and providing volunteer
                                                services for clean-up of the path of destruction.</p>
                                            <a class="btn-primary-roman p-x-15 p-y-5 fs-14" href="#">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    @include('includes.partials.news-sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection
