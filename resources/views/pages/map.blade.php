@extends('layouts.frontend')

@section('content')
  <div class="google-map section">
      <div class="container">
          <div class="section-header m-b-100">
              <h6 class="fw-600 clr-primary-maroon text-center">[ Our Jobs ]</h6>
              <h1 class="section-heading text-center">Come See Our Expert Craftsmen in Action.</h1>
              <div class="fancy-border"></div>
          </div>
          <div id="dvMap" style="width: 100%; height: 500px;"></div>
      </div>
  </div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXtxYyNfYhoT5jerD9axaN21QjG9CmFGQ&callback=initMap" type="text/javascript"></script>
    <script>
        var mapIcon = '<?=  asset('frontend/images/logos/roman-helmet.png')  ?>';
        console.log(mapIcon)
    </script>
    <script src="{{ asset('frontend/js/googlemaps.js') }}"></script>
@endsection
