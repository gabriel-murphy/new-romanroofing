@extends('layouts.frontend')

@section('content')
    @include('includes.partials.inner-page-banner')

    <div class="products-wiki section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Our Products ]</h6>
                <h1 class="section-heading text-center">Check Our Quality Products</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequatur labore natus non officiis
                quo similique soluta! Animi aperiam consequuntur deserunt dignissimos dolorem eligendi est et fugit
                inventore iure magni neque non omnis ratione reprehenderit temporibus, voluptates! Dolorem ducimus esse quae
                repellendus saepe sapiente. Dignissimos facere itaque modi officia praesentium?
            </p>
        </div>
    </div>

    {{--Shingle Parallax--}}

    <div class="product">
        <div class="container-fluid full-width">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="product-job-image">
                        <img class="img-fluid" src="{{ asset('frontend/images/Products/product-shingle.jpg') }}"
                             alt="product-shingle">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-job-description sub-section h-100">
                        <h2 class="heading-md primary-black mb-5">Shingle</h2>
                        <p class="section-paragraph m-b-60">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Alias aliquam atque consequatur
                            delectus dolores, eius eligendi enim eveniet explicabo illum inventore iste iusto molestiae
                            omnis quam quasi quia quidem quis rem soluta tempore ullam ut vel, veritatis voluptatibus?
                            Dolore dolorem eaque odio quae rerum! Aspernatur autem, ducimus eveniet, inventore iste
                            minus non odio officiis quia quod ut voluptatem voluptatum.
                        </p>
                        <a class="btn-primary-roman" href="{{ route('portfolio') }}">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="parallax-img" style="background-image: url({{ asset('frontend/images/Products/parallax-shingle.jpg') }})"></div>

    {{--Shingle Parallax End--}}

    {{--Tile Parallax--}}

    <div class="product">
        <div class="container-fluid full-width">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="product-job-image">
                        <img class="img-fluid" src="{{ asset('frontend/images/Products/product-tile.jpg') }}"
                             alt="product-shingle">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-job-description sub-section h-100">
                        <h2 class="heading-md primary-black mb-5">Tile</h2>
                        <p class="section-paragraph m-b-60">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Alias aliquam atque consequatur
                            delectus dolores, eius eligendi enim eveniet explicabo illum inventore iste iusto molestiae
                            omnis quam quasi quia quidem quis rem soluta tempore ullam ut vel, veritatis voluptatibus?
                            Dolore dolorem eaque odio quae rerum! Aspernatur autem, ducimus eveniet, inventore iste
                            minus non odio officiis quia quod ut voluptatem voluptatum.
                        </p>
                        <a class="btn-primary-roman" href="{{ route('portfolio') }}">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="parallax-img" style="background-image: url({{ asset('frontend/images/Products/parallax-tile.jpg') }})"></div>

    {{--Tile Parallax End--}}

    {{--Metal Parallax--}}

    <div class="product">
        <div class="container-fluid full-width">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="product-job-image">
                        <img class="img-fluid" src="{{ asset('frontend/images/Products/product-metal.jpg') }}"
                             alt="product-shingle">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-job-description sub-section h-100">
                        <h2 class="heading-md primary-black mb-5">Metal</h2>
                        <p class="section-paragraph m-b-60">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Alias aliquam atque consequatur
                            delectus dolores, eius eligendi enim eveniet explicabo illum inventore iste iusto molestiae
                            omnis quam quasi quia quidem quis rem soluta tempore ullam ut vel, veritatis voluptatibus?
                            Dolore dolorem eaque odio quae rerum! Aspernatur autem, ducimus eveniet, inventore iste
                            minus non odio officiis quia quod ut voluptatem voluptatum.
                        </p>
                        <a class="btn-primary-roman" href="{{ route('portfolio') }}">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="parallax-img" style="background-image: url({{ asset('frontend/images/Products/parallax-metal.jpg') }})"></div>

    {{--Metal Parallax End--}}

    {{--Solar Parallax--}}

    <div class="product">
        <div class="container-fluid full-width">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="product-job-image">
                        <img class="img-fluid" src="{{ asset('frontend/images/Products/product-solar.jpg') }}"
                             alt="product-shingle">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-job-description sub-section h-100">
                        <h2 class="heading-md primary-black mb-5">Solar</h2>
                        <p class="section-paragraph m-b-60">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Alias aliquam atque consequatur
                            delectus dolores, eius eligendi enim eveniet explicabo illum inventore iste iusto molestiae
                            omnis quam quasi quia quidem quis rem soluta tempore ullam ut vel, veritatis voluptatibus?
                            Dolore dolorem eaque odio quae rerum! Aspernatur autem, ducimus eveniet, inventore iste
                            minus non odio officiis quia quod ut voluptatem voluptatum.
                        </p>
                        <a class="btn-primary-roman" href="{{ route('portfolio') }}">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="parallax-img" style="background-image: url({{ asset('frontend/images/Products/parallax-solar.jpg') }})"></div>

    {{--Solar Parallax End--}}

@endsection
