@extends('layouts.frontend')

@section('content')

    <div class="news-bar m-0 text-center">
        <a href="#"><span class="text-uppercase">Breaking News:</span> An EF-1 tornado riped through Northwest Cape
            Coral Northwest Cape Coral</a>
    </div>

    <div class="full-news section">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="full-news-wrapper">
                        <div class="news-bar mt-0">
                            <a href="#"><span class="text-uppercase">Breaking News:</span> An EF-1 tornado riped through
                                Northwest Cape
                                Coral Northwest Cape Coral</a>
                        </div>
                        <div class="full-news-img gradient-black-light mb-4">
                            <img class="img-fluid" src="{{ asset('frontend/images/news/news-banner-1.jpg') }}"
                                 alt="news-banner-1.jpg">
                        </div>
                        <div class="section-paragraph">
                            <p class="fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam commodi consequuntur
                                ducimus eveniet expedita fugit harum, id illo illum modi non nulla rem sed veniam. Ab
                                adipisci cum delectus eos et incidunt, neque nulla officiis perferendis quam qui quo,
                                sequi sit unde, veritatis. Accusantium ad aperiam atque aut autem, culpa distinctio
                                dolorem eos error eum eveniet illum in ipsa iste iusto laudantium libero modi molestiae,
                                mollitia natus nemo odit ratione repellat repudiandae sunt totam vel voluptatum? Eos,
                                libero, veritatis. Autem culpa in nulla odio veniam. Aliquid, atque cum exercitationem
                                explicabo modi nam nisi, omnis perspiciatis rem, sapiente sed ut.
                            </p>
                            <p class="fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam commodi consequuntur
                                ducimus eveniet expedita fugit harum, id illo illum modi non nulla rem sed veniam. Ab
                                adipisci cum delectus eos et incidunt, neque nulla officiis perferendis quam qui quo,
                                sequi sit unde, veritatis. Accusantium ad aperiam atque aut autem, culpa distinctio
                                dolorem eos error eum eveniet illum in ipsa iste iusto laudantium libero modi molestiae,
                                mollitia natus nemo odit ratione repellat repudiandae sunt totam vel voluptatum? Eos,
                                libero, veritatis. Autem culpa in nulla odio veniam. Aliquid, atque cum exercitationem
                                explicabo modi nam nisi, omnis perspiciatis rem, sapiente sed ut.
                            </p>
                            <p class="fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam commodi consequuntur
                                ducimus eveniet expedita fugit harum, id illo illum modi non nulla rem sed veniam. Ab
                                adipisci cum delectus eos et incidunt, neque nulla officiis perferendis quam qui quo,
                                sequi sit unde, veritatis. Accusantium ad aperiam atque aut autem, culpa distinctio
                                dolorem eos error eum eveniet illum in ipsa iste iusto laudantium libero modi molestiae,
                                mollitia natus nemo odit ratione repellat repudiandae sunt totam vel voluptatum? Eos,
                                libero, veritatis. Autem culpa in nulla odio veniam. Aliquid, atque cum exercitationem
                                explicabo modi nam nisi, omnis perspiciatis rem, sapiente sed ut.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    @include('includes.partials.news-sidebar')
                </div>
            </div>
        </div>
    </div>

@endsection
