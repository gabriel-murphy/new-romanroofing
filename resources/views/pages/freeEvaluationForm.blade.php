@extends('layouts.frontend')

@section('style')

    <link rel="stylesheet" href="{{ asset('frontend/js/plugins/steptabs/steptabs.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/js/plugins/bootstrap4-toggle-3.6.1/css/bootstrap4-toggle.min.css') }}">

    <style>

    </style>
@endsection

@section('content')
    <div class="free-evaluation-form">

        <div class="container">
            {{--            <form action="{{ route('evaluation.submit') }}" id="evaluation_form" method="POST" enctype="multipart/form-data" novalidate>--}}
            @csrf
            <div class="progress">
                <div class="progress-bar progress-bar-striped"></div>
            </div>
            <div class="steps-wrapper_outer">
                <ul class="nav nav-tabs step-nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="step_1" data-toggle="tab" href="#step_1_tab" role="tab" aria-controls="home" aria-selected="true">Step 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="step_2" data-toggle="tab" href="#step_2_tab" role="tab" aria-controls="profile" aria-selected="false">Step 2</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="step_3" data-toggle="tab" href="#step_3_tab" role="tab" aria-controls="profile" aria-selected="false">Step 3</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="step_4" data-toggle="tab" href="#step_4_tab" role="tab" aria-controls="profile" aria-selected="false">Step 4</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="step_5" data-toggle="tab" href="#step_5_tab" role="tab" aria-controls="profile" aria-selected="false">Step 5</a>
                    </li>
                    {{--                        <li class="nav-item">--}}
                    {{--                            <a class="nav-link" id="step_6" data-toggle="tab" href="#step_6_tab" role="tab" aria-controls="profile" aria-selected="false">Step 6</a>--}}
                    {{--                        </li>--}}
                    <li class="nav-item">
                        <a class="nav-link" id="step_7" data-toggle="tab" href="#step_7_tab" role="tab" aria-controls="profile" aria-selected="false">Step 7</a>
                    </li>
                </ul>

                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="step_1_tab" role="tabpanel" aria-labelledby="step_1">
                        <form id="evaluation_form_1" action="">
                            <div class="section-header mb-5 text-center">
                                <h1 class="heading-md primary-black mb-3">Get a No-Cost Evaluation & Proposal</h1>
                                <p class="primary-grey fs-16">
                                    Roofing projects are our expertise - please provide us with some basic information about your situation so we can schedule a no-cost evaluation where one of our roofing specialists will be dispatched to your job location to offer their guidance.
                                </p>
                            </div>
                            <div class="mb-5">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="address" class="primary-black required-star">Address</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fod fod-map-marker clr-primary-maroon"></i></span>
                                            </div>
                                            <input type="text" id="address" name="address" class="form-control" placeholder="Enter Address">
                                            <div id="address_error" class="error-field"></div>
                                            <input type="hidden" id="formatted_address" name="formatted_address" class="form-control" placeholder="Enter Address">
                                            <input type="hidden" id="city" name="city" class="form-control" >
                                            <input type="hidden" id="state" name="state" class="form-control" >
                                            <input type="hidden" id="county" name="county" class="form-control" >
                                            <input type="hidden" id="country" name="country" class="form-control" >
                                            <input type="hidden" id="zip_code" name="zip_code" class="form-control">
                                            <input type="hidden" id="street" name="street" class="form-control">
                                            <input type="hidden" id="latitude" name="latitude" class="form-control">
                                            <input type="hidden" id="longitude" name="longitude" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-4">
                                        <label for="telephone" class="primary-black required-star">Telephone</label>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <select class="form-control input-group-text m-r-1" name="conutry_code" id="country_code">
                                                    <option value="+1"  selected>USA +1</option>
                                                </select>
                                            </div>
                                            <input type="text" pattern="[0-9]+" maxlength="14" class="form-control border-left-0 border-top-left-radius-0 border-bottom-left-radius-0" id="phone" name="phone">

                                            <div class="input-group-append">
                                                <input type="text" class="form-control border-left-0 border-right-0 rounded-0" id="extension" name="extension" style="width: 65px;">
                                                <span class="input-group-text">Ext</span>
                                            </div>
                                            <div id="phone_error" class="error-field"></div>
                                            <div id="extension_error" class="error-field"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="active_leak" class="primary-black d-block">Active Leak</label>
                                            <input id="active_leak" name="active_leak" value="leaked" type="checkbox" checked data-on="Yes" data-off="No" data-toggle="toggle" onchange="leakChanged()">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="text-center">
                                <button type="button" id="step_1_next" class="btn-primary-roman btnNext">Next</button>
                            </div>
                        </form>
                    </div>


                    <div class="tab-pane fade" id="step_2_tab" role="tabpanel" aria-labelledby="step_2">
{{--                        <form id="evaluation_form_2" action="">--}}
                            <div class="section-header mb-5 text-center">
                                <h1 class="heading-md primary-black mb-3">Confirm Property Location</h1>
                                <p class="primary-grey fs-16">
                                    Confirm property location by moving circle icon if needed.
                                </p>
                            </div>

                            <div id="dvMap" class="mb-3"></div>
                            <div class="roman-user-selection_wrapper d-block mb-5">
                                <input class="roman-checkbox-input" id="include_solar_checkbox" name="include_solar_checkbox" type="checkbox" checked value="1">
                                <label class="roman-checkbox-label user-selection-inline-to-mobile text-center text-md-left d-block" for="solar_checkbox">
                                    <img class="checkbox-img-md" src="{{ asset('frontend/images/evaluationform/base-image-tesla-system@2x.png') }}" alt="base-image-tesla-system image">
                                    <span>Yes, please include Solar Evaluation</span>
                                </label>
                            </div>

                            <div class="text-center">
                                <button type="button" class="btn-secondary-roman btnPrevious mr-3">Previous</button>
                                <button type="button" id="step_2_next" class="btn-primary-roman btnNext">Next</button>
                            </div>
{{--                        </form>--}}
                    </div>

                    <div class="tab-pane fade" id="step_3_tab" role="tabpanel" aria-labelledby="step_3">
                        <form id="evaluation_form_3" action="">
                            <div class="section-header mb-5 text-center">
                                <h1 class="heading-md primary-black mb-3">Structure & Leak</h1>
                                <p class="primary-grey fs-16">
                                    Help us ensure we direct your roofing project to the proper specialist by providing the following optional information.
                                </p>
                            </div>
                            <div class="text-center mb-5">
                                <h1 class="primary-black fs-20 mb-3 fw-600">Structures Needing Evaluation</h1>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input needing_evaulation" id="main_house" name="structure[needing_evaluation][]" type="checkbox" value="main_house">
                                    <label class="roman-checkbox-label" for="main_house">
                                        <i class="fod fod-main-house"></i>
                                        <span>Main House</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0">
                                    <input class="roman-checkbox-input needing_evaulation" id="detached_garage" name="structure[needing_evaluation][]" type="checkbox" value="detached_garage">
                                    <label class="roman-checkbox-label" for="detached_garage">
                                        <i class="fod fod-detached-garage"></i>
                                        <span>Detached Garage</span>
                                    </label>
                                </div>
                            </div>
                            <div id="structure_error" class="error-field"></div>

                            <div class="text-center mb-5" id="active_leak_div">
                                <h1 class="primary-black fs-20 mb-3 fw-600">Active Leak?</h1>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-radio-input" id="active-leak-spotting" type="radio" name="active-leak" value="spotting">
                                    <label class="roman-radio-label roman-radio-vertical" for="active-leak-spotting">
                                        <img src="{{ asset('frontend/images/evaluationform/icons/icon-spotting-leak.svg') }}" alt="spotting leak icon">
                                        <span>Spotting</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-radio-input" id="active-leak-light" type="radio" name="active-leak" value="light">
                                    <label class="roman-radio-label roman-radio-vertical" for="active-leak-light">
                                        <img src="{{ asset('frontend/images/evaluationform/icons/icon-light-leak.svg') }}" alt="spotting leak icon">
                                        <span>Light</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-radio-input" id="active-leak-steady" type="radio" name="active-leak" value="steady">
                                    <label class="roman-radio-label roman-radio-vertical" for="active-leak-steady">
                                        <img src="{{ asset('frontend/images/evaluationform/icons/icon-steady-leak.svg') }}" alt="spotting leak icon">
                                        <span>Steady</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0">
                                    <input class="roman-radio-input" id="active-leak-stream" type="radio" name="active-leak" value="stream">
                                    <label class="roman-radio-label roman-radio-vertical" for="active-leak-stream">
                                        <img src="{{ asset('frontend/images/evaluationform/icons/icon-stream-leak.svg') }}" alt="spotting leak icon">
                                        <span>Steam</span>
                                    </label>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="button" class="btn-secondary-roman btnPrevious mr-3">Previous</button>
                                <button type="button"  id="step_3_next"  class="btn-primary-roman btnNext">Next</button>
                            </div>
                        </form>

                    </div>


                    <div class="tab-pane fade" id="step_4_tab" role="tabpanel" aria-labelledby="step_4">
                        <form id="evaluation_form_4" action="">
                            <div class="section-header mb-5 text-center">
                                <h1 class="heading-md primary-black mb-3">About You</h1>
                                <p class="primary-grey fs-16">
                                    Please enter your name details
                                </p>
                            </div>

                            <div class="mb-5">
                                <div class="row">
                                    <div class="col-md-4 offset-md-4">
                                        <div class="form-group mb-3">
                                            <label for="first_name" class="primary-black required-star">First Name</label>
                                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Enter First Name">
                                            <div id="first_name_error" class="error-field"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 offset-md-4">
                                        <div class="form-group mb-3">
                                            <label for="last_name" class="primary-black required-star">Last Name</label>
                                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter Last Name">
                                            <div id="last_name_error" class="error-field"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 offset-md-4">
                                        <div class="form-group mb-3">
                                            <label for="email" class="primary-black required-star">Email</label>
                                            <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email Address">
                                            <div id="email_error" class="error-field"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="button" class="btn-secondary-roman btnPrevious mr-3">Previous</button>
                                <button type="button"  id="step_4_next"  class="btn-primary-roman btnNext">Next</button>
                            </div>
                        </form>
                    </div>


                    <div class="tab-pane fade" id="step_5_tab" role="tabpanel" aria-labelledby="step_5">
{{--                        <form id="evaluation_form_5" action="">--}}
                            <div class="section-header mb-5 text-center">
                                <h1 class="heading-md primary-black mb-3">Roofing Service</h1>
                                <p class="primary-grey fs-16">
                                    Select any of the following option to proceed with you No-Cost Evaluation & Proposal
                                </p>
                            </div>

                            <div class="text-center mb-5">
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input roofing_service" id="reroof_service" type="checkbox" name="roofing_service[]" value="reroof">
                                    <label class="roman-checkbox-label" for="reroof_service">
                                        <i class="fod fod-re-roof"></i>
                                        <span>Replacement</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input roofing_service" id="new_roof_service" type="checkbox" name="roofing_service[]" value="new_construction">
                                    <label class="roman-checkbox-label" for="new_roof_service">
                                        <i class="fod fod-new-roof"></i>
                                        <span>New Roof</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0">
                                    <input class="roman-checkbox-input roofing_service" id="repair_service" type="checkbox" name="roofing_service[]" value="repair">
                                    <label class="roman-checkbox-label" for="repair_service">
                                        <i class="fod fod-repair-roof"></i>
                                        <span>Repair</span>
                                    </label>
                                </div>
                            </div>

                            <div class="text-center mb-5">
                                <div class="section-header mb-5 text-center">
                                    <h1 class="heading-md primary-black mb-3">Materials of Interest</h1>
                                    <p class="primary-grey fs-16">
                                        Help us ensure we direct your roofing project to the proper specialist by providing the following optional information.                                </p>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input materials_interest" id="interest_shingle" type="checkbox" name="materials_interest[]" value="shingle">
                                    <label class="roman-checkbox-label" for="interest_shingle">
                                        <i class="fod fod-shingle"></i>
                                        <span>Shingle</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input materials_interest" id="interest_tile" type="checkbox" name="materials_interest[]" value="tile">
                                    <label class="roman-checkbox-label" for="interest_tile">
                                        <i class="fod fod-tile"></i>
                                        <span>Tile</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0">
                                    <input class="roman-checkbox-input materials_interest" id="interest_metal" type="checkbox" name="materials_interest[]" value="metal">
                                    <label class="roman-checkbox-label" for="interest_metal">
                                        <i class="fod fod-metal"></i>
                                        <span>Metal</span>
                                    </label>
                                </div>
                            </div>

                            <div class="text-center mb-5">
                                <h1 class="primary-black fs-20 mb-3 fw-600">Related Services</h1>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input related_services" id="gutter_downspout_service" type="checkbox" name="related_services[]" value="gutters_downspouts">
                                    <label class="roman-checkbox-label-sm" for="gutter_downspout_service">
                                        <i class="fod fod-re-roof"></i>
                                        <span>Gutters & Downspouts</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input related_services" id="skylight_service" type="checkbox" name="related_services[]" value="skylight">
                                    <label class="roman-checkbox-label-sm" for="skylight_service">
                                        <i class="fod fod-new-roof"></i>
                                        <span>Skylights</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0 mr-3">
                                    <input class="roman-checkbox-input related_services" id="solar_tube_service" type="checkbox" name="related_services[]" value="solar_tube">
                                    <label class="roman-checkbox-label-sm" for="solar_tube_service">
                                        <i class="fod fod-repair-roof"></i>
                                        <span>Solar Tubes</span>
                                    </label>
                                </div>
                                <div class="roman-user-selection_wrapper mb-3 mb-lg-0">
                                    <input class="roman-checkbox-input related_services" id="soffit_fascia_work_service" type="checkbox" name="related_services[]" value="soffit_fascia">
                                    <label class="roman-checkbox-label-sm" for="soffit_fascia_work_service">
                                        <i class="fod fod-repair-roof"></i>
                                        <span>Soffit / Fascia Work</span>
                                    </label>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="button" class="btn-secondary-roman btnPrevious mr-3">Previous</button>
                                <button type="button"   id="step_5_next"  class="btn-primary-roman btnNext mr-3">Next</button>
                                <button type="button"   id="step_6_next"   class="btn-tertiary-roman btnNext"><i class="fod fod-double-angle-right"></i> Skip</button>
                            </div>
{{--                        </form>--}}
                    </div>

                    {{-- <div class="tab-pane fade" id="step_6_tab" role="tabpanel" aria-labelledby="step_6">



                         <div class="text-center mb-5">
                             <h1 class="primary-black fs-20 fw-600 mb-3">Shingle Profiles</h1>
                             <div class="row">
                                 <div class="col-md-4">
                                     <div class="roman-user-selection_wrapper mb-3 mb-lg-0 d-block">
                                         <input class="roman-radio-input" id="designer_shingle_profile" type="radio" name="material-profile">
                                         <label class="roman-radio-label text-left roman-radio-vertical d-block" for="designer_shingle_profile">
                                             <img class="w-100 img-fluid" src="{{ asset('frontend/images/evaluationform/designer-vector.svg') }}" alt="designer shingle image">
                                             <span>Designer</span>
                                             <span class="fs-16 fw-400">Thicker | Multiple Color & Paterns</span>
                                         </label>
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="roman-user-selection_wrapper mb-3 mb-lg-0 d-block">
                                         <input class="roman-radio-input" id="architectural_shingle_profile" type="radio" name="material-profile">
                                         <label class="roman-radio-label text-left roman-radio-vertical d-block" for="architectural_shingle_profile">
                                             <img class="w-100 img-fluid" src="{{ asset('frontend/images/evaluationform/architectural-vector.svg') }}" alt="architectural shingle image">
                                             <span>Architectural</span>
                                             <span class="fs-16 fw-400">Thicker | Multiple Color & Paterns</span>
                                         </label>
                                     </div>
                                 </div>
                                 <div class="col-md-4">
                                     <div class="roman-user-selection_wrapper mb-3 mb-lg-0 d-block">
                                         <input class="roman-radio-input" id="3tab_shingle_profile" type="radio" name="material-profile">
                                         <label class="roman-radio-label text-left roman-radio-vertical d-block" for="3tab_shingle_profile">
                                             <img class="w-100 img-fluid" src="{{ asset('frontend/images/evaluationform/3tab-vector.svg') }}" alt="3tab shingle image">
                                             <span>3-Tab</span>
                                             <span class="fs-16 fw-400">Thiner | Single Paterns</span>
                                         </label>
                                     </div>
                                 </div>
                             </div>
                         </div>

                         <div class="text-center">
                             <button type="button" class="btn-secondary-roman btnPrevious mr-3">Previous</button>
                             <button type="button" class="btn-primary-roman btnNext mr-3">Next</button>
                             <button type="button" class="btn-tertiary-roman btnNext"><i class="fod fod-double-angle-right"></i> Skip</button>
                         </div>
                     </div>--}}

                    <div class="tab-pane fade" id="step_7_tab" role="tabpanel" aria-labelledby="step_7">
                        <form id="evaluation_form_7" action="">
                            <div class="text-center">
                                <img class="mb-5" src="{{ asset('frontend/images/evaluationform/icons/icon-check.svg') }}" alt="icon-check.svg">
                                <div class="section-header">
                                    <h1 class="heading-md primary-black mb-3">Success</h1>
                                    <p class="primary-grey fs-16">
                                        Thanks your form submitted in 24 hours our team will contact your on priority.
                                    </p>
                                </div>
                            </div>

                            <div class="text-center">
                                <button id="finish_evaluation" class="btn-primary-roman">Finish</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--            </form>--}}
        </div>
    </div>


@endsection


@section('scripts')

    <script src="{{ asset('frontend/js/plugins/bootstrap4-toggle-3.6.1/js/bootstrap4-toggle.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" integrity="sha512-37T7leoNS06R80c8Ulq7cdCDU5MNQBwlYoy1TX/WUsLFC2eYNqtKlV0QjH7r8JpG/S0GUMZwebnVFLPd6SU5yg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js" integrity="sha512-XZEy8UQ9rngkxQVugAdOuBRDmJ5N4vCuNXCh8KlniZgDKTvf7zl75QBtaVG1lEhMFe2a2DuA22nZYY+qsI2/xA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXtxYyNfYhoT5jerD9axaN21QjG9CmFGQ&callback=initMap" type="text/javascript"></script> --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtsoWEUduR1G8IylC7M1YefQO1b4REHNc&libraries=places"></script>

    <script>
        var mapIcon = '<?=  asset('frontend/images/logos/roman-helmet.png')  ?>';
        let token = '{{ csrf_token() }}';
    </script>
    <script src="{{ asset('frontend/js/googlemaps.js') }}"></script>
    <script src="{{ asset('frontend/js/evaluation_form.js') }}"></script>

    <script>
        google.maps.event.addDomListener(window, 'load', initialize);
        function initialize() {
            var input = document.getElementById('address');
            const options = {
                // componentRestrictions: { country: "us" },
                // fields: ["address_components", "geometry", "icon", "name","city","state","county","country","zip_code",],
                // strictBounds: false,
                types: ["establishment"],
            };

            var autocomplete = new google.maps.places.Autocomplete(input,options);
            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();
                console.log(place,'place');
                place.address_components.forEach(elem => {
                    console.log(elem);
                    elem.types.forEach(el=>{
                        if(el=="administrative_area_level_3"){
                            $('#city').val(elem.long_name);
                        }
                        if(el=="administrative_area_level_1"){
                            $('#state').val(elem.long_name);
                        }
                        if(el=="administrative_area_level_2"){
                            $('#county').val(elem.long_name);
                        }
                        if(el=="country"){
                            $('#country').val(elem.long_name);
                        }
                        if(el=="postal_code"){
                            $('#zip_code').val(elem.long_name);
                        }
                        if(el=="route"){
                            $('#street').val(elem.long_name);
                        }
                    });
                });
                $('#latitude').val(place.geometry['location'].lat());
                $('#longitude').val(place.geometry['location'].lng());
                $('#formatted_address').val(place.formatted_address);

                dropIcon('title',place.geometry['location'].lat(),place.geometry['location'].lng())
            });
        }

        function leakChanged(){
            let chk = document.getElementById("active_leak");
            if(chk.checked){
                document.getElementById("active_leak_div").classList.remove("d-none");
            }else{
                document.getElementById("active_leak_div").classList.add("d-none");
            }

        }
    </script>
@endsection

