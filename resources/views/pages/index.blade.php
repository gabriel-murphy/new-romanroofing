@extends('layouts.frontend')

@section('content')

    <div class="banner" style="background-image: url('{{asset('frontend/images/banner/bg-banner.jpg')}}')">
        <div class="banner-content">
            <div class="container-fluid">
                <div class="banner-carousel owl-carousel owl-theme">
                    <div class="item">
                        <div class="col-md-8 mx-auto">
                            <h1 class="banner-title text-center">Professional Roofing</h1>
                            <div class="fancy-border"></div>
                            <p class="banner-description text-center">
                                Over 200+ years of roofing experience stands behind the leadership of the Roman name.
                            </p>
                            <div class="banner-btns text-center">
                                <a href="#" class="btn-primary-roman">Case Studies</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-8 mx-auto">
                            <h1 class="banner-title text-center">Professional Roofing</h1>
                            <div class="fancy-border"></div>
                            <p class="banner-description text-center">
                                Over 200+ years of roofing experience stands behind the leadership of the Roman name.
                            </p>
                            <div class="banner-btns text-center">
                                <a href="#" class="btn-primary-roman">Case Studies</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-8 mx-auto">
                            <h1 class="banner-title text-center">Professional Roofing</h1>
                            <div class="fancy-border"></div>
                            <p class="banner-description text-center">
                                Over 200+ years of roofing experience stands behind the leadership of the Roman name.
                            </p>
                            <div class="banner-btns text-center">
                                <a href="#" class="btn-primary-roman">Case Studies</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-8 mx-auto">
                            <h1 class="banner-title text-center">Professional Roofing</h1>
                            <div class="fancy-border"></div>
                            <p class="banner-description text-center">
                                Over 200+ years of roofing experience stands behind the leadership of the Roman name.
                            </p>row no-gutters m-b-80
                            <div class="banner-btns text-center">
                                <a href="#" class="btn-primary-roman">Case Studies</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-us section bg-grey">
        @include('includes.partials.header-fixed')
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ About Us ]</h6>
                <h1 class="section-heading text-center">Who We Are</h1>
                <div class="fancy-border"></div>
            </div>
            <p class="section-paragraph text-center m-b-60">
                Roman is Southwest Florida's trusted experts at roofing - residential or commercial projects and of all
                sizes.  We are a multi year award recipient from the most trusted and reputable roofing manufacturers in
                North America, including GAF and Owens Corning, to name a few. Our growth is a consequence of our
                commitment to project management excellence and consistent training. Roman is also one of a few
                recipients of <strong>GAF's Triple Excellence Award and President Club recipients in Southwest
                    Florida.</strong>
            </p>
            <div class="text-center">
                <a class="btn-primary-roman" href="#">Learn More</a>
            </div>
        </div>
    </div>

    <div class="why-us section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header m-b-100">
                        <h6 class="fw-600 clr-primary-maroon text-center">[ Why Us ]</h6>
                        <h1 class="section-heading text-center">It's Roman to the Rescue</h1>
                        <div class="fancy-border"></div>
                    </div>
                    <div class="text-center mb-5">
                        <img class="img-fluid" src="{{asset('frontend/images/van.png')}}" alt="van.png">
                    </div>
                </div>
                <div class="col-md-12">
                    <p class="section-paragraph text-center">
                        Picking the wrong roofing contractor will inevitably lead to major problems for you and your
                        property. It may lead to substantial water damage to your property and subsequent costly
                        roofing repairs. As our customers know, Roman Roofing completes the job right the first time
                        and on time. We are licensed, insured and most importantly a trusted, proven name. Our
                        roofing jobs are backed by our satisfaction guarantee. Call Roman to rescue your roofing
                        project today!
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="experience section bg-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-7 order-2 order-md-1 my-auto">
                    <h2 class="heading-md primary-black mb-4">200+ Years of Roofing Experience</h2>
                    <p class="section-paragraph text-justify mb-5">The Roman team consists of subject matter experts in
                        roofing materials,
                        engineering and applications. Unlike other contractors, our business is devoted exclusively to
                        perfecting the art of roofing. We take exceptional pride in our craft - our customer reviews and
                        accolades validate that Roman is Southwest Florida's go-to roofing expert.
                    </p>
                    <div class="text-center text-md-left">
                        <a class="btn-primary-roman" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-5 order-1 order-md-2">
                    <div class="text-right">
                        <img class="img-fluid" src="{{asset('frontend/images/jeff.png')}}" alt="jeff.png">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="trusted-contractor section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img class="img-fluid" src="{{asset('frontend/images/roman-instagram.png')}}"
                         alt="roman-instagram.png">
                </div>
                <div class="col-md-7 my-auto">
                    <h2 class="heading-md primary-black mb-4">We Are Southwest Florida's Go-To Roofing Contractor</h2>
                    <p class="section-paragraph text-justify">A family owned roofing company backed by crews who are
                        true craftsmen of the trade, Roman is the name to trust for your roofing project - small or
                        large, residential or commercial. services. If there are issues, our Customer Guarantee ensures
                        that we will resolve them to your satisfaction.
                    </p>
                    <p class="section-paragraph text-justify mb-5">
                        <strong>
                            Our 5-star reputation with the BBB and on-line review sites ensure you are working with only
                            the best.
                        </strong>
                    </p>
                    <div class="text-center text-md-left">
                        <a class="btn-primary-roman" href="#">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="finance-project section bg-grey">
        <div class="container">
            <div class="row">
                <div class="col-md-7 my-auto">
                    <h2 class="heading-md primary-black mb-4">Finance Your Roofing Project with Roman</h2>
                    <p class="section-paragraph text-justify mb-5">Let's face it...a new roof isn't always a luxury.
                        More
                        often than not, replacing your old roof ends up being a necessity or even an emergency, and due
                        to budgets and cash flow, you might put off getting your roof done. Roman Roofing offers a
                        variety of financing options tailored to fit your budget and lifestyle.
                    </p>
                    <div class="text-center text-md-left">
                        <a class="btn-primary-roman" href="#">Read More</a>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="text-right">
                        <img class="img-fluid" src="{{asset('frontend/images/finance.png')}}" alt="finance.png">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="services section">
        <div class="container">
            <div class="section-header m-b-90">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Our Services ]</h6>
                <h1 class="section-heading text-center">What We Can Offer</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="total-services">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="service-card">
                            <div class="service-card-header">
                                <i class="fas fa-home-lg"></i>
                            </div>
                            <div class="service-card-body">
                                <h4 class="service-card-title text-center">Re-Roofing</h4>
                                <p class="service-card-description text-center mb-5">
                                    Over 4,000 happy homeowners in Southwest Florida trust us for their roofing needs.
                                </p>
                                <div class="text-center">
                                    <a class="service-card-btn" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="service-card">
                            <div class="service-card-header">
                                <i class="fas fa-hammer"></i>
                            </div>
                            <div class="service-card-body">
                                <h4 class="service-card-title text-center">Roof Repairs</h4>
                                <p class="service-card-description text-center mb-5">
                                    From the duplexes, churches and multi-story - we have always been there and done
                                    that.
                                </p>
                                <div class="text-center">
                                    <a class="service-card-btn" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="service-card">
                            <div class="service-card-header">
                                <i class="fas fa-tools"></i>
                            </div>
                            <div class="service-card-body">
                                <h4 class="service-card-title text-center">Maintenance</h4>
                                <p class="service-card-description text-center mb-5">
                                    Leaking roofs is a common scenario which our trained technicians can identify and
                                    quickly resolve.
                                </p>
                                <div class="text-center">
                                    <a class="service-card-btn" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-3">
                        <div class="service-card">
                            <div class="service-card-header">
                                <i class="fas fa-house-return"></i>
                            </div>
                            <div class="service-card-body">
                                <h4 class="service-card-title text-center">New Roof</h4>
                                <p class="service-card-description text-center mb-5">
                                    A creative roof design can give your home a beautiful appearance
                                    as well as create some additional space inside.
                                </p>
                                <div class="text-center">
                                    <a class="service-card-btn" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="portfolio section bg-grey">
        <div class="container">
            <div class="section-header m-b-90">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Portfolio ]</h6>
                <h1 class="section-heading text-center">Recent Projects</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="row no-gutters m-b-80">
                <div class="col-md-3">
                    <div class="project">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/home/portfolio-shingle.jpg')}}"
                             alt="portfolio-shingle.jpg">
                        <div class="project-description">
                            <div class="text-center text-white">
                                <h4 class="heading-sm">Shingle Roof</h4>
                                <p class="paragraph-sm">CAPE CORAL MIAMI FLORIDA</p>
                                <a class="btn-primary-roman p-y-5 p-x-30 mt-5" href="#">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="project">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/home/portfolio-tile.jpg')}}"
                             alt="portfolio-tile.jpg">
                        <div class="project-description">
                            <div class="text-center text-white">
                                <h4 class="heading-sm">Tile Roof</h4>
                                <p class="paragraph-sm">CAPE CORAL MIAMI FLORIDA</p>
                                <a class="btn-primary-roman p-y-5 p-x-30 mt-5" href="#">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="project">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/home/portfolio-metal.jpg')}}"
                             alt="portfolio-metal.jpg">
                        <div class="project-description">
                            <div class="text-center text-white">
                                <h4 class="heading-sm">Metal Roof</h4>
                                <p class="paragraph-sm">CAPE CORAL MIAMI FLORIDA</p>
                                <a class="btn-primary-roman p-y-5 p-x-30 mt-5" href="#">View</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="project">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/home/portfolio-solar.jpg')}}"
                             alt="portfolio-solar.jpg">
                        <div class="project-description">
                            <div class="text-center text-white">
                                <h4 class="heading-sm">Solar Roof</h4>
                                <p class="paragraph-sm">CAPE CORAL MIAMI FLORIDA</p>
                                <a class="btn-primary-roman p-y-5 p-x-30 mt-5" href="#">View</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a class="btn-primary-roman" href="#">View All Projects</a>
            </div>
        </div>
    </div>

    <div class="testimonials section">
        <div class="container">
            <div class="section-header m-b-100">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Testimonials ]</h6>
                <h1 class="section-heading text-center">Our Customers Love The Roman Experience</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="testimonials-carousel owl-carousel owl-theme">
                <div class="item">
                    @include('includes.partials.testimonials')
                </div>
                <div class="item">
                    @include('includes.partials.testimonials')
                </div>
                <div class="item">
                    @include('includes.partials.testimonials')
                </div>
            </div>

            <div class="text-center mt-5">
                <a href="{{route('testimonials')}}" class="btn-primary-roman">View All</a>
            </div>
        </div>
    </div>

    <div class="stats p-y-40 bg-grey">
        <div class="container">
            <div class="stats-wrapper">
                <div class="stat-box">
                    <span class="stat-icon"><i class="fas fa-clipboard-check"></i></span>
                    <p class="stat-count">2862</p>
                    <h3 class="stat-label">Jobs Completed</h3>
                </div>
                <div class="stat-box">
                    <span class="stat-icon"><i class="fas fa-award"></i></span>
                    <p class="stat-count">5</p>
                    <h3 class="stat-label">Awards</h3>
                </div>
                <div class="stat-box">
                    <span class="stat-icon"><i class="fas fa-at"></i></span>
                    <p class="stat-count">2948</p>
                    <h3 class="stat-label">Mentions</h3>
                </div>
                <div class="stat-box">
                    <span class="stat-icon"><i class="fas fa-star"></i></span>
                    <p class="stat-count">2796</p>
                    <h3 class="stat-label">5 Star Reviews</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="awards bg-primary-maroon p-y-40">
        <div class="container">
            <div class="awards-carousel owl-carousel owl-theme">
                <div class="item">
                    <img src="{{asset('frontend/images/awards/award-angles.png')}}" alt="award-angles.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/gaf-solar.png')}}" alt="gaf-solar.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/award-bbb.png')}}" alt="award-bbb.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/award-chamber.png')}}" alt="award-chamber.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/award-gaftriple.png')}}" alt="award-gaftriple.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/award-gahelite.png')}}" alt="award-gahelite.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/award-inc5000.png')}}" alt="award-inc5000.png">
                </div>
                <div class="item">
                    <img src="{{asset('frontend/images/awards/awards-gaf.png')}}" alt="awards-gaf.png">
                </div>
            </div>
        </div>
    </div>

@endsection
