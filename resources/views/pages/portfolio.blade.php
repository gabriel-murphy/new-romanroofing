@extends('layouts.frontend')

@section('links')
    <link rel="stylesheet" href="{{asset('frontend/css/venobox.min.css')}}">
@endsection

@section('content')

    @include('includes.partials.inner-page-banner')

    <!--Solar Mosdal -->

    <div id="solar_modal" class="modal fade" tabindex="-1" aria-labelledby="solar_modal" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="video mb-5">
                        <iframe width="100%" height="624" src="https://www.youtube.com/embed/tLva2RjaEr4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Solar Modal End-->

    <div class="portfolio-videos section bg-grey">
        <div class="container">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Portfolio ]</h6>
                <h1 class="section-heading text-center">What We Have Done</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="portfolio-description m-b-100">
                <p class="section-paragraph text-center">
                    The crews of Roman Roofing have adorned over 14,000,000 square feet of Southwest Florida with only the
                    top of the line roofing materials on the market. We have assembled a small collection of our notable
                    roofing projects, which we feature as our job site of the month and the materials selected range from
                    dimensional shingle to tile, metal and other varieties. Take a look at our portfolio of roofing projects
                    below.
                </p>
            </div>
            <div class="portfolio-videos-wrapper">
                <div class="video mb-5">
                    <iframe width="100%" height="624" src="https://www.youtube.com/embed/tLva2RjaEr4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="video-1 mb-5">
                    <iframe width="100%" height="624" src="https://www.youtube.com/embed/7hCsuWT1wCo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="video-2 mb-5">
                    <iframe width="100%" height="624" src="https://www.youtube.com/embed/cF25lPzEpXU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="video-2 mb-5">
                    <iframe width="100%" height="624" src="https://www.youtube.com/embed/NPlmi0bAwaE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="portfolio-gallery section">
        <div class="container-fluid full-width">
            <div class="section-header">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Gallery ]</h6>
                <h1 class="section-heading text-center">Preparing a Master Piece</h1>
                <div class="fancy-border"></div>
            </div>
            <ul class="filter-list-menu">
                <li>
                    <button class="active" data-name="*">All</button>
                </li>
                <li>
                    <button data-name=".shingle">Shingle</button>
                </li>
                <li>
                    <button data-name=".tile">Tile</button>
                </li>
                <li>
                    <button data-name=".metal">Metal</button>
                </li>
                <li>
                    <button data-name=".solar">Solar</button>
                </li>
            </ul>

            <div class="row no-gutters grid">
                {{--Shingle Roof Loop--}}

                @for($i = 1; $i <= 4 ; $i++)
                    <div class="col-md-3 grid-item shingle">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/shingle-'."{$i}".'.jpg')}}" alt="shingle-{{$i}}.jpg">
                        <a class="venobox full-image-btn" data-gall="shingle" title="Shingle {{$i}}" href="{{asset('frontend/images/portfolio/shingle-'."{$i}".'.jpg')}}">
                            <div class="grid-item-detail">
                                <h4 class="heading-sm text-white">Shingle Roof</h4>
                                <p class="paragraph-sm text-white">CAPE CORAL MIAMI FLORIDA</p>
                            </div>
                        </a>
                    </div>
                @endfor

                {{--Shingle Roof Loop End--}}

                {{--Tile Roof Loop--}}

                @for($i = 1; $i <= 6 ; $i++)
                    <div class="col-md-3 grid-item tile">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/tile-'."{$i}".'.jpg')}}" alt="tile-{{$i}}.jpg">
                        <a class="venobox full-image-btn" data-gall="tile" title="Tile {{$i}}" href="{{asset('frontend/images/portfolio/tile-'."{$i}".'.jpg')}}">
                            <div class="grid-item-detail">
                                <h4 class="heading-sm text-white">Tile Roof</h4>
                                <p class="paragraph-sm text-white">CAPE CORAL MIAMI FLORIDA</p>
                            </div>
                        </a>
                    </div>
                @endfor

                {{--Tile Roof Loop End--}}

                {{--Metal Roof Loop--}}

                @for($i = 1; $i <= 10 ; $i++)
                    <div class="col-md-3 grid-item metal">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/metal-'."{$i}".'.jpg')}}" alt="metal-{{$i}}.jpg">
                        <a class="venobox full-image-btn" data-gall="metal" title="Metal {{$i}}" href="{{asset('frontend/images/portfolio/metal-'."{$i}".'.jpg')}}">
                            <div class="grid-item-detail">
                                <h4 class="heading-sm text-white">Metal Roof</h4>
                                <p class="paragraph-sm text-white">CAPE CORAL MIAMI FLORIDA</p>
                            </div>
                        </a>
                    </div>
                @endfor

                {{--Metal Roof Loop End--}}

                {{--Solar Roof Loop--}}

                @for($i = 1; $i <= 3 ; $i++)
                    <div class="col-md-3 grid-item solar">
                        <img class="img-fluid" src="{{asset('frontend/images/portfolio/solar-'."{$i}".'.jpg')}}" alt="solar-{{$i}}.jpg">
                        <a class="venobox full-image-btn" data-gall="solar" title="Solar {{$i}}" href="{{asset('frontend/images/portfolio/solar-'."{$i}".'.jpg')}}">
                            <div class="grid-item-detail">
                                <h4 class="heading-sm text-white">Solar Roof</h4>
                                <p class="paragraph-sm text-white">CAPE CORAL MIAMI FLORIDA</p>
                            </div>
                        </a>
                    </div>
                @endfor

                {{--Solar Roof Loop End--}}
            </div>
        </div>
    </div>



@endsection



@section('scripts')
    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
    <script src="{{asset('frontend/js/venobox.min.js')}}"></script>
    <script src="{{asset('frontend/js/isotope.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#solar_modal').modal('show');
            $('.venobox').venobox();
        });
    </script>


@endsection
