@extends('layouts.frontend')

@section('links')
    <link rel="stylesheet" href="{{ asset('frontend/css/lightslider.css') }}">
@endsection

@section('content')

    @include('includes.partials.inner-page-banner')

    <div class="roof-service section p-b-40">
        <div class="container">
            <ul id="lightSlider">
                @for($i = 1; $i <= 6; $i++)
                    <li data-thumb="{{ asset('frontend/images/roof-services/roof-service-'."{$i}".'.jpg') }}">
                        <img class="img-fluid"
                             src="{{ asset('frontend/images/roof-services/roof-service-'."{$i}".'.jpg') }}"
                             alt="roof-service-{{$i}}.jpg">
                    </li>
                @endfor
            </ul>
        </div>
    </div>

    <div class="roof-service-detail p-b-80">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="roof-service-left">
                        <div class="mb-5">
                            <p class="section-paragraph fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam dolor inventore obcaecati
                                officiis optio quibusdam vero vitae. Accusamus blanditiis commodi corporis culpa itaque labore
                                maxime molestias omnis ratione? A aut commodi distinctio dolor, doloremque earum eligendi est
                                facilis fuga fugiat incidunt magnam nobis nostrum obcaecati qui, recusandae tempora voluptas.
                                Accusamus alias, beatae blanditiis dolor dolorum error facilis libero magnam, minima natus nulla
                                numquam omnis quisquam quos similique velit vitae voluptas. Commodi odio odit perspiciatis quaerat
                                quas quasi reiciendis similique?
                            </p>
                            <p class="section-paragraph fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam dolor inventore obcaecati
                                officiis optio quibusdam vero vitae. Accusamus blanditiis commodi corporis culpa itaque labore
                                maxime molestias omnis ratione? A aut commodi distinctio dolor, doloremque earum eligendi est
                                facilis fuga fugiat incidunt magnam nobis nostrum obcaecati qui, recusandae tempora voluptas.
                                Accusamus alias, beatae blanditiis dolor dolorum error facilis libero magnam, minima natus nulla
                                numquam omnis quisquam quos similique velit vitae voluptas. Commodi odio odit perspiciatis quaerat
                                quas quasi reiciendis similique?
                            </p>
                            <p class="section-paragraph fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam dolor inventore obcaecati
                                officiis optio quibusdam vero vitae. Accusamus blanditiis commodi corporis culpa itaque labore
                                maxime molestias omnis ratione? A aut commodi distinctio dolor, doloremque earum eligendi est
                                facilis fuga fugiat incidunt magnam nobis nostrum obcaecati qui, recusandae tempora voluptas.
                                Accusamus alias, beatae blanditiis dolor dolorum error facilis libero magnam, minima natus nulla
                                numquam omnis quisquam quos similique velit vitae voluptas. Commodi odio odit perspiciatis quaerat
                                quas quasi reiciendis similique?
                            </p>
                            <p class="section-paragraph fs-16">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam dolor inventore obcaecati
                                officiis optio quibusdam vero vitae. Accusamus blanditiis commodi corporis culpa itaque labore
                                maxime molestias omnis ratione? A aut commodi distinctio dolor, doloremque earum eligendi est
                                facilis fuga fugiat incidunt magnam nobis nostrum obcaecati qui, recusandae tempora voluptas.
                                Accusamus alias, beatae blanditiis dolor dolorum error facilis libero magnam, minima natus nulla
                                numquam omnis quisquam quos similique velit vitae voluptas. Commodi odio odit perspiciatis quaerat
                                quas quasi reiciendis similique?
                            </p>
                        </div>

                        <div class="roof-service-benefits">
                            <h3 class="heading-md mb-4">Benefit of Service</h3>
                            <div class="row">
                                <div class="col-md-5">
                                    <img class="img-fluid" src="{{ asset('frontend/images/roof-services/roof-service-benefits.jpg') }}" alt="roof-service-benefits.jpg">
                                </div>
                                <div class="col-md-7">
                                    <div class="media mb-4 pr-5">
                                        <span><i class="far fa-check"></i></span>
                                        <div class="media-body">
                                            <h5 class="primary-black fw-600 mt-0">All Roof-Top Solutions</h5>
                                            <p class="primary-grey">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                        </div>
                                    </div>
                                    <div class="media mb-4 pr-5">
                                        <span><i class="far fa-check"></i></span>
                                        <div class="media-body">
                                            <h5 class="primary-black fw-600 mt-0">Foremost Roofing Contractors</h5>
                                            <p class="primary-grey">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                        </div>
                                    </div>
                                    <div class="media pr-5">
                                        <span><i class="far fa-check"></i></span>
                                        <div class="media-body">
                                            <h5 class="primary-black fw-600 mt-0">Fair Installation Pricing</h5>
                                            <p class="primary-grey">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="roof-service-sidebar">
                        <div class="all-services-list mb-5">
                            <h3 class="heading-md mb-3">Our Services</h3>
                            <ul class="mb-4">
                                <li>
                                    <a href="{{ route('service') }}"><i class="fas fa-house-damage"></i> Re-Roofing Projects</a>
                                </li>
                                <li>
                                    <a href="{{ route('service') }}"><i class="fas fa-truck"></i> New Construction</a>
                                </li>
                                <li>
                                    <a href="{{ route('service') }}"><i class="fas fa-tint-slash"></i> Roof Maintenance</a>
                                </li>
                                <li>
                                    <a href="{{ route('service') }}"><i class="fas fa-tools"></i> Leaks & Repairs</a>
                                </li>
                            </ul>
                        </div>

                        <div class="get-in-touch mb-5">
                            <h3 class="heading-md text-white mb-4">Get in Touch</h3>
                            <ul>
                                <li>
                                    <a href="tel:2394587663">
                                        <i class="fas fa-phone-alt" aria-hidden="true"></i>
                                        (239) 458-7663
                                    </a>
                                </li>
                                <li>
                                    <a href="mailto:help@roman-roofing.com">
                                        <i class="fas fa-envelope" aria-hidden="true"></i>
                                        help@roman-roofing.com
                                    </a>
                                </li>
                                <li>
                                    <i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                                    <p class="m-0">921 NE 27th Ln, <span class="d-block">Cape Coral, FL 33909</span></p>
                                </li>
                            </ul>
                        </div>
                        <div class="appointment">
                            <img class="img-fluid" src="{{ asset('frontend/images/roof-services/roof-service-sidebar.jpg') }}" alt="roof-service-sidebar.jpg">
                            <div class="appointment-content">
                                <h4 class="heading-sm text-white">Available</h4>
                                <p class="text-white">for any type of Roof Construction</p>
                                <a class="btn-primary-roman-white fw-600" href="{{ route('estimate') }}">Free Estimate</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('frontend/js/lightslider.js')}}"></script>
    <script src="{{asset('frontend/js/lightslider-custom.js')}}"></script>
@endsection
