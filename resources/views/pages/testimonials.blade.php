@extends('layouts.frontend')

@section('style')
    <style>
        div#background > div.menu{
            display: none !important;
        }
    </style>


    <link rel="stylesheet" href="{{ asset('frontend/js/plugins/bootstrap4-toggle-3.6.1/css/bootstrap4-toggle.min.css') }}">

@endsection

@section('links')
    <link rel="stylesheet" href="{{ asset('frontend/js/plugins/galleryjs/reset.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/js/plugins/galleryjs/common.css') }}">
@endsection

@section('content')
    @include('includes.partials.inner-page-banner')

    <div class="testimonials all-feedback section">
        <div class="container">
            <div class="section-header m-b-100">
                <h6 class="fw-600 clr-primary-maroon text-center">[ Testimonials ]</h6>
                <h1 class="section-heading text-center">Our Customers Love The Roman Experience</h1>
                <div class="fancy-border"></div>
            </div>
            <div class="row">
                @for($i = 1; $i < 20; $i++)
                    <div class="col-md-6">
                        <div class="feedback-wrapper">
                            @include('includes.partials.testimonials')
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="gallery group-gallery">
                            <section>
                                @for($j = 1; $j <=2; $j++)
                                    <div id="damageRoofPhotos">
                                        <a class="damage-photos" href="{{ asset('frontend/images/roof-job/damage-photos/before-job-'."{$j}".'.jpg') }}" rel="damage-photo-{{$i}}" title="Damage Photo {{$j}}">
                                            <img class="img-fluid" src="{{ asset('frontend/images/roof-job/damage-photos/before-job-'."{$j}".'.jpg') }}" alt="before-job-{{$j}}">
                                        </a>
                                    </div>
                                @endfor
                                <h2 class="heading-md primary-black text-center">Before Job</h2>
                            </section>
                            <section>
                                @for($j = 1; $j <=2; $j++)
                                    <div id="newRoofPhotos">
                                        <a class="new-photos" href="{{ asset('frontend/images/roof-job/new-roof-photos/after-job-'."{$j}".'.jpg') }}" rel="new-roof-photo-{{$i}}" title="New Roof Photo {{$j}}">
                                            <img class="img-fluid" src="{{ asset('frontend/images/roof-job/new-roof-photos/after-job-'."{$j}".'.jpg') }}" alt="after-job-{{$j}}">
                                        </a>
                                    </div>
                                @endfor
                                <h2 class="heading-md primary-black text-center">After Job</h2>
                            </section>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('frontend/js/plugins/galleryjs/gallery.js') }}"></script>
    <script src="{{ asset('frontend/js/plugins/bootstrap4-toggle-3.6.1/js/bootstrap4-toggle.min.js') }}"></script>
    <script>
        var i;
        for(i=1;i<20;i++){
            var gallery_init = {
                group : ['damage-photo-'+i,'new-roof-photo-'+i],
            };
            $(gallery.construct(gallery_init));
        }

    </script>
@endsection
