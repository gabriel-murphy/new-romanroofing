<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

## Routes for Static Pages
// Route::get('/', 'PagesController@homepage');
// Route::get('/about', 'PagesController@about');
// Route::get('/about/portfolio', 'PagesController@portfolio');
// Route::get('/about/news', 'PagesController@news');
// Route::get('/about/jobs', 'PagesController@jobs');
// Route::get('/residential-roofing', 'PagesController@residentialroofing');
// Route::get('/commercial-roofing', 'PagesController@commercialroofing');
// Route::get('/re-roof', 'PagesController@reroof');
// Route::get('/new-roof', 'PagesController@newroof');
// Route::get('/roof-repairs', 'PagesController@roofrepairs');
// Route::get('/roof-maintenance', 'PagesController@roofmaintenance');
// Route::get('/portfolio', 'PagesController@portfolio');
// Route::get('/financing', 'PagesController@financing');

## News Controller
// Route::get('/about/news/2019/october/roman-rescues-homeowners-from-tornado-in-northwest-cape-coral', 'NewsController@tornado');
// Route::get('/about/news/2019/august/roman-roofing-named-fastest-growing-roofing-contractor', 'NewsController@inc5000');

// ## Estimate Controller
// Route::get('/estimate', 'EstimateController@index');
// Route::post('/estimate', 'EstimateController@store');

// ## Contact Controller
// Route::get('/contact', 'ContactController@index');
// Route::post('/contact', 'ContactController@store');

// ## Reviews Controller
// Route::resource('reviews', 'ReviewsController');

// Route::group(['prefix'=>'frontend'],function (){
    Route::view('/','pages.index')->name('home');

    Route::get('/roof-service/service', function () {
        return view('pages.service', ['banner_title' => 'Service','banner_description' => 'Service Tagline']);
    })->name('service');

    Route::get('/testimonials', function () {
        return view('pages.testimonials', ['banner_title' => 'Testimonials','banner_description' => 'Testimonials Tagline']);
    })->name('testimonials');

    Route::get('/products', function () {
        return view('pages.products', ['banner_title' => 'Products','banner_description' => 'Products Tagline']);
    })->name('products');

    Route::get('/estimate', function () {
        return view('pages.estimate', ['banner_title' => 'Estimate','banner_description' => 'Estimate Tagline']);
    })->name('estimate');

    Route::get('/contact', function () {
        return view('pages.contact', ['banner_title' => 'Contact','banner_description' => 'Contact Tagline']);
    })->name('contact');

    Route::get('/about/history', 'AboutController@history')->name('history');


    Route::get('/about/portfolio', function () {
        return view('pages.portfolio', ['banner_title' => 'Portfolio','banner_description' => 'Portfolio Tagline']);
    })->name('portfolio');

    Route::get('/about/news', function () {
        return view('pages.news', ['banner_title' => 'News','banner_description' => 'News Tagline']);
    })->name('news');

    Route::get('/about/news/full-news', function () {
        return view('pages.full-news', ['banner_title' => 'Full News','banner_description' => 'Full News Tagline']);
    })->name('full-news');

    Route::get('/about/jobs', function () {
        return view('pages.jobs', ['banner_title' => 'Jobs','banner_description' => 'Job Tagline']);
    })->name('jobs');

    Route::get('/about/charity', function () {
        return view('pages.charity', ['banner_title' => 'Roman Charity','banner_description' => 'Charity Tagline']);
    })->name('charity');

    Route::get('/sanibel', function () {
        return view('pages.city', ['banner_title' => 'Sanibel','banner_description' => 'A Barrier Island Sanctuary']);
    })->name('city');

    Route::get('/financing', function () {
        return view('pages.financing', ['banner_title' => 'Financing','banner_description' => 'Financing Tagline']);
    })->name('financing');
    Route::get('/evaluation', 'EvaluationController@index')->name('evaluation');
    Route::post('/evaluation', 'EvaluationController@submit')->name('evaluation.submit');



    Route::view('/googlemap','pages.map')->name('googlemap');
// });


Route::post('jobs/add', 'JobController@addjob');
