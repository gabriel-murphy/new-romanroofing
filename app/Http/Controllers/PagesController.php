<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function homepage() 
    {
    	return view('homepage', [
    		'foo' => 'bar'
    	]);
   	}
	public function about() 
    {
    	return view('about');
   	}
  public function portfolio() 
    {
      return view('portfolio');
    }
  public function news() 
    {
      return view('news');
    }
  public function jobs() 
    {
      return view('jobs');
    }
  public function estimate() 
    {
      return view('estimate');
    }
  public function commercialroofing() 
    {
      return view('commercialroofing');
    }
  public function residentialroofing() 
    {
      return view('residentialroofing');
    }
  public function newroof() 
    {
      return view('newroof');
    }
  public function reroof() 
    {
      return view('reroof');
    }
  public function roofmaintenance() 
    {
      return view('maintenance');
    }
  public function roofrepairs() 
    {
      return view('roofrepairs');
    }
  public function financing() 
    {
      return view('financing');
    }
  public function contact() 
    {
      return view('contact.index');
    }
  public function contactprocess(Request $request) 
    {
      dd($request);

      #print('here');
      exit;
        $review = new Review();
        $review->username = request('username');
#        $review->comment = request('comment');
#        $review->save();
#        return redirect('/reviews');



      return view('contact.thanks');
    }
}

