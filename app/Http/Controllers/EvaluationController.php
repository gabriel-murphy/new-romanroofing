<?php

namespace App\Http\Controllers;

use App\Client;
use App\Jobs\ComplimentaryRoofEvaluationJob;
use App\Jobs\FreeEvaluationJob;
use App\Lead;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Snowplow\RefererParser\Parser;

class EvaluationController extends Controller
{
    public function get_browser_name($user_agent){
        $t = strtolower($user_agent);
        $t = " " . $t;
        if     (strpos($t, 'opera'     ) || strpos($t, 'opr/')     ) return 'Opera'            ;
        elseif (strpos($t, 'edge'      )                           ) return 'Edge'             ;
        elseif (strpos($t, 'chrome'    )                           ) return 'Chrome'           ;
        elseif (strpos($t, 'safari'    )                           ) return 'Safari'           ;
        elseif (strpos($t, 'firefox'   )                           ) return 'Firefox'          ;
        elseif (strpos($t, 'msie'      ) || strpos($t, 'trident/7')) return 'Internet Explorer';
        return 'Unkown';
    }

    public function get_platform_name($user_agent){
        $t = strtolower($user_agent);
        $t = " " . $t;
        if     (strpos($t, 'windows'     )      ) return 'Windows'            ;
        elseif (strpos($t, 'ubuntu'    )                           ) return 'Ubuntu'           ;
        elseif (strpos($t, 'linux'      )                           ) return 'Linux'             ;
        elseif (strpos($t, 'macintosh'    )                           ) return 'Macintosh'           ;
        elseif (strpos($t, 'iPhone'   )                           ) return 'iPhone'          ;
        return 'Unkown';
    }

    public function index(){



        // info(User::all());
//        $browser = $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);
//        $os = $this->get_platform_name($_SERVER['HTTP_USER_AGENT']);
//
//        $parser = new Parser();
//            $referer = $parser->parse(
//                'https://www.romanroofing.com/'
//            );
//            // info(var_dump($referer));
//            if ($referer->isKnown()) {
//                info($referer->getMedium()); // "Search"
//                info($referer->getSource()); // "Google"
//                info($referer->getSearchTerm());   // "gateway oracle cards denise linn"
//            }

        return view('pages.freeEvaluationForm');
    }

    public function submit(Request $request){


        $property_id = DB::table('properties')->insertGetId(
            array(
                'street' => $request['street'],
                'full_address' => $request['address'],
                'formatted_address' => $request['formatted_address'],
                'city' => $request['city'],
                'state' => $request['state'],
                'county' => $request['county'],
                'country' => $request['country'],
                'zip_code' => $request['zip_code'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
        ));

        // $code = preg_split($request["country_code"]," ");
        // info($code);
        $active_leak = $request['active_leak'];
        $include_solar = $request['include_solar_checkbox'];
        $structures = [];
        $services = [];
        $interests = [];
        $related = [];

        if(isset($request->structure_needing_evalaution) && is_array($request->structure_needing_evalaution)){
            $structures = $request->structure_needing_evalaution;
        }
        if(isset($request->roofing_service) && is_array($request->roofing_service)){
            $services = $request->roofing_service;
        }
        if(isset($request->materials_interest) && is_array($request->materials_interest)){
            $interests = $request->materials_interest;
        }
        if(isset($request->related_services) && is_array($request->related_services)){
            $related = $request->related_services;
        }
        if(isset($request->active_leak_type)){
            $leak_type = $request->active_leak_type;
        }
        else{
            $leak_type = "no_leak";
        }

        $structure_info = [
            "active-leak"=>$active_leak,
            "leak_type"=>$leak_type,
            "include_solar"=>$include_solar,
            "structures"=>$structures,
            "services"=>$services,
            "interests"=>$interests,
            "related"=>$related
        ];
        $lead_id = DB::table('leads')->insertGetId(
            array(
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'property_id' => $property_id,
                'phone' => $request['country_code'].$request['phone'],
                'country_code' => $request['country_code'],
                'phone_type' => 'cell',
                'extension' => $request['extension'],
                'email'=>$request['email'],
                'structure_info'=>json_encode($structure_info)
        ));

        $lead = Lead::find($lead_id);
        $client = Client::find(1);

        ComplimentaryRoofEvaluationJob::dispatch($lead, $client);

        return response()->json([
            'success' =>true,
        ],200);



    }
}
