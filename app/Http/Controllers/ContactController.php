<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Contact;

#use Request;

class ContactController extends Controller
{
	public function index() 
    {
    	return view('contact.index', [
    		'title' => 'Contact!!!'
    	]);
   	}

    public function store(Request $request)
    {
        ## First, we send the emails to Roman and the Visitor
        \Mail::send('emails.contact.company', array('full_name' => request('full_name'), 'email_address' => request('email_address'),
        'telephone' => request('telephone'), 'comment' => request('comment')),  function($message)
        {
            $message->to('office@romanroofinginc.com', 'Roman Roofing Website')->subject('Website Contact');
        });
        \Mail::send('emails.contact.visitor', array('full_name' => request('full_name'), 'email_address' => request('email_address'),
        'telephone' => request('telephone'), 'comment' => request('comment')),  function($message)
        {
            $message->to(request('email_address'), request('full_name'))->subject('Thank You For Contacting Roman Roofing');
        });
        
        ## Store the contact in the database
        $contact = new Contact();
        $contact->full_name = request('full_name');
        $contact->email_address = request('email_address');
        $contact->telephone = request('telephone');
        $contact->comment = request('comment');
        $contact->newsletter = request('newsletter');
        $contact->save();
        return view('contact.thanks');

    }
}
