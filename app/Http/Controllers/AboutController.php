<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    private $client;
    public function __construct(Client $client)
    {
        $this->client = $client;
        //$this->url = config('services.datalab.person.url');
        //$this->apiKey = config('services.datalab.person.api_key');
    }

    public function history(){
        $api_response = User::all();
    //    try {
    //        $response = $this->client->request('GET', "https://odie.romanroofing.com/api/roman/users", [
    //            'verify' =>false,
    //            //'query' => $data
    //        ]);
    //        $api_response = json_decode($response->getBody(), true);
    //    } catch (ClientException $exception) {
    //        //dd($exception);
    //    }

       return view('pages.about', ['banner_title' => 'History','banner_description' => 'History Tagline','api_response'=>$api_response]);
   }
}
