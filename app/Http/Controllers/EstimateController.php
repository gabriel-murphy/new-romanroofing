<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;

class EstimateController extends Controller
{
	public function index()
    {
    	return view('estimate', [
    		'title' => 'Obtain An Estimate from Roman Roofing'
    	]);
   	}

    public function store(Request $request)
    {
        ## First, we send the emails to Roman and the Visitor
        ## Need to put this code in the right place.
        Mail::send('emails.estimate.company', array('name' => request('name'), 'email_address' => request('email_address'),
        'telephone' => request('telephone'), 'comment' => request('comment')),  function($message)
        {
            $message->to('romanroofingoffice@gmail.com', 'Roman Roofing Website')->bcc('gabriel@romanroofing.com')->subject('Website Request for Roofing Evaluation!');
        });
        Mail::send('emails.estimate.visitor', array('name' => request('name'), 'email_address' => request('email_address'),
        'telephone' => request('telephone'), 'comment' => request('comment')),  function($message)
        {
            $message->to(request('email_address'), request('name'))->bcc('gabriel@romanroofing.com')->subject('Your Request for a Roof Evaluation by Roman Roofing');
        });

        ## Store the contact in the database
        #$contact = new Contact();
        #$contact->full_name = request('full_name');
        #$contact->email_address = request('email_address');
        #$contact->telephone = request('telephone');
        #$contact->comment = request('comment');
        #$contact->newsletter = request('newsletter');
        #$contact->save();
        return view('contact.thanks');

    }
}
