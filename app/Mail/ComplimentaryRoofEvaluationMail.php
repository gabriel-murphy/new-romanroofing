<?php

namespace App\Mail;

use App\Client;
use App\EmailSection;
use App\Template;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComplimentaryRoofEvaluationMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $lead, $client;
    public  $mailable, $template,$forwardTo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($lead, $client)
    {
        info('lead');
        $this->lead = $lead;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */


    private function _parse_template($template, $models, $cc ,$subject = false)
    {
        $replace_data = [];
        foreach ($template->placeholders as $placeholder){
            $model = $models[$placeholder->model] ?? null;
            $key = "%%$placeholder->name%%";
            if ($model) $replace_data[$key] = $model->{$placeholder->attribute};
        }
        // info($replace_data);
        $content = $subject ? $template->subject : $cc;
        return strtr($content, $replace_data);
    }

    private function _parse_template2($template, $models ,$subject = false)
    {
        $replace_data = [];
        foreach ($template->placeholders as $placeholder){
            $model = $models[$placeholder->model] ?? null;
            $key = "%%$placeholder->name%%";
            if ($model) $replace_data[$key] = $model->{$placeholder->attribute};
        }
        // info($replace_data);
        $content = $subject ? $template->subject :  $template->content;
        return strtr($content, $replace_data);
    }


    public function build()
    {
        info('in build');
        $template = Template::type('email')->key('complimentary_roof_evaluation')->first();
        $this->template = $template;
        $implodedIds = implode(',',json_decode($template->sections));
        $email_section = EmailSection::whereIn('id',json_decode($template->sections))->orderByRaw(\DB::raw("FIELD(id, $implodedIds)"))->get();
        $content='';
        $this->mailable = $this->lead;
        // Setting Models
        // info('$template');
        // info($template);
        // info($email_section);

        $lead_array = array();
        $lead_array['name'] =  $this->lead->name;
//        $lead_array['when'] =  'Tomorrow';
        $lead_array['when'] =  $this->_getWhenValue();
            $models['lead']     =  $this->lead;
        $models['client']   =  $this->client;
        $models['property'] = $this->lead->property;

        $template->subject = $this->modifications['subject'] ?? $template->subject;
        $template->content = $this->modifications['content'] ?? $template->content;

//        $this->template->content = "";
        foreach ($email_section as $value) {
            if($value->name=='content'){
                $content .= $this->_parse_template($template, $models,$template->content );
                $this->template->content .= $this->_parse_template($template, $models,$template->content);
            }
            else{
                $content .= $this->_parse_template($template, $models,$value->html_content );
                $this->template->content .= $this->_parse_template($template, $models,$value->html_content);
            }
        }
        $subject = $this->_parse_template($template, $models,$template->content, true);

        $email = $this->view('emails.email_layout', compact('content'));
        $email->subject($subject);
            // info($email);
//        $is_sandbox = config('constants.sandbox.is_sandbox');
        $email->to( 'mahrfarooq33@gmail.com');
        /*if(isset($is_sandbox) && $is_sandbox==1){
            $sandbox_email = config('constants.sandbox.email');
            $email->to( $sandbox_email);
        }
        else {
            $email->to( $this->lead->email);
        }*/

        return $email;
    }

  private  function _getWhenValue()
    {
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        if($dayOfTheWeek === 6)
        {
            $when = 'on Monday';
        }else
        {
            $when = 'Tomorrow';
        }
        return $when;
    }

}
