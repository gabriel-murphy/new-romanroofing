<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExceptionMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $exception,$section_subject;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    //public function __construct(MaximusClaim $claim, $files, $modifications = array())
    public function __construct($exception,$subject)
    {
        $this->exception = $exception;
        $this->section_subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $people_to_send = array('ashar@disruptive-effects.com','saqibaziz99@gmail.com','mahrfarooq33@gmail.com');
        $content = $this->exception->getMessage();

        $email = $this->view('emails.exception_template', compact('content'));
        $email->subject($this->section_subject);

        $email->to($people_to_send[0]);
        $email->bcc([$people_to_send[1],$people_to_send[2]]);

        return $email;
    }
}
