<?php

namespace App;

// use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;

class Placeholder extends Model
{
    // use Searchable;

    protected $fillable = ['model', 'attribute', 'name'];

    protected $not_allowable = [];

    public $timestamps = false;

    private function getAllModels($dir)
    {
        $data = array();
        $classes = \File::files($dir);
        foreach ($classes as $class) {
            array_push($data, str_replace('.php', '', $class->getBasename()));
        }
        return $data;
    }

    public function getAllowableModelsAttribute()
    {
        $classes = self::getAllModels(app_path());
        return array_diff($classes, $this->not_allowable );
    }


}
