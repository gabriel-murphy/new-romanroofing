<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailSection extends Model
{
    protected $table = 'email_sections';
}
