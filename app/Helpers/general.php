<?php

use App\Constant;
use App\Crew;
use Carbon\Carbon;
use Pixelpeter\Genderize\Facades\Genderize;

function _active_menu($route, $parent = false)
{
    if ($parent)
        return request()->is($route) || request()->is($route.'/*') ? ' open' : '';
    else
        return request()->is($route) ? ' active' : '';
}

function _extractBuildSheet($arr)
{
    $collection = collect($arr);
    $collection->mapWithKeys(function ($item) {
        return $item !== 'external_charges';
    });
}

function _parse_template($template, $models, $cc ,$subject = false)
{
    $replace_data = [];
    foreach ($template->placeholders as $placeholder){
        $model = $models[$placeholder->model] ?? null;
        $key = "%%$placeholder->name%%";
        if ($model) $replace_data[$key] = $model->{$placeholder->attribute};
    }
    // info($replace_data);
    $content = $subject ? $template->subject : $cc;
     return strtr($content, $replace_data);
}

function _parse_template2($template, $models ,$subject = false)
{
    $replace_data = [];
    foreach ($template->placeholders as $placeholder){
        $model = $models[$placeholder->model] ?? null;
        $key = "%%$placeholder->name%%";
        if ($model) $replace_data[$key] = $model->{$placeholder->attribute};
    }
    // info($replace_data);
    $content = $subject ? $template->subject :  $template->content;
     return strtr($content, $replace_data);
}


function _sidebar_menu($text, $route = 'javascript:void(0)')
{
    $class = $route == request()->url() ? 'active' : '';
    return '<li><a class="' . $class . '" href="' . $route . '">' . $text . '</a></li>';
}

function _sidebar_menu_group($text, $icon)
{
    return
        '<a class="nav-submenu" data-toggle="nav-submenu" href="#">
            <i class="'.$icon.'"></i><span class="sidebar-mini-hide">'.$text.'</span>
        </a>';
}

function _storage_url($path, $append_storage = false)
{
    if ($append_storage)
        return asset('storage/' . $path);
    else return asset($path);
}

function _active_tab($property, $string, $empty_case = false)
{
    if ($empty_case) {
        $state = request()->query($property) == $string || empty(request()->query()) ? 'active' : '';
    } else {
        $state = request()->query($property) == $string ? 'active' : '';
    }
    return $state;
}

function _constantFindById($id)
{
    return Constant::where('id', $id)->first();
}
function _constantFindByGroupId($groupId)
{
 return Constant::where('constant_group_id', $groupId)->get();
}
function _constant($key)
{
 return Constant::where('key', $key)->get('name')[0]->name;
}

function _crewName($id)
{
        $crew = Crew::where('id', $id)->with('lead')->first();
        if ($crew && $crew->lead)
       return $crew->lead->name;
}

function _properStringFormate($value)
{
  $string =  str_replace('_', ' ', $value );
  return ucwords($string);
}


function _wordAfterUnderScore($value)
{
  $string =  substr($value, strpos($value, "_") + 1);
  return ucwords($string);
}

function _replaceCharsAfterLength($value){

    $length = strlen($value);

    if($length >37){
        $value = substr($value, 0, 37);

        $value = $value . ". . .";
        return $value;
    }
    else {
        return $value;
    }
}

function _customDateTimeFormate($date)
{
    return Carbon::parse($date)->format('l j F Y h:i A');
}

function _customDateFormate($date)
{
    return Carbon::parse($date)->format('j F Y');
}
function _customTimeFormate($date)
{
    return Carbon::parse($date)->format('h:i A');
}

function _removeDevisor($id)
{
    return  explode("/", $id)[0];
}

function titleCase($value){

    return Str::title($value);
}

function _getBooleanResult($value){

    return $value ? 'Signed' : 'Not Signed';
}
function _formatPhoneNumber($phoneNumber) {
    $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

    if(strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        //$phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
        $phoneNumber =  '+'.$countryCode .' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}

function _remove_quotations($phoneNumber) {
    $phone = explode('(',$phoneNumber);
    $phone = explode(')',$phone[1]);
    $first_format  = $phone[0];
    $phone = explode(' ',$phone[1]);
    $phone = explode('-',$phone[1]);
    $second_merge = $phone[0];
    $third_merge = $phone[1];
    $phone = $first_format.$second_merge.$third_merge;
    return $phone;
}
function array_swap_assoc($sorted,$key1,$key2)
{
    $temp = $sorted[$key1];
    $sorted[$key1] = $sorted[$key2];
    $sorted[$key2] = $temp;
    return $sorted;
}
function _generate_unique_code($limit)
{
    return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
}

function _base64UrlEncode($text)
{
    return str_replace(
        ['+', '/', '='],
        ['-', '_', ''],
        base64_encode($text)
    );
}
function _getWhenValue()
{
    $dayOfTheWeek = Carbon::now()->dayOfWeek;
    if($dayOfTheWeek === 6)
    {
        $when = 'on Monday';
    }else
    {
        $when = 'Tomorrow';
    }
    return $when;
}

function _getGender($name)
{
    $response = Genderize::name($name)->get();
    if ($response->result->first()->isFemale())
    {
        $gender = 'She';
    }else
    {
        $gender = 'He';
    }
    return $gender;
}



