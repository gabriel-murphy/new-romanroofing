<?php

namespace App;

use App\Traits\HasClient;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
//    use HasClient;

    protected $fillable = [
        'customer_id',
        'classification_id',
        'project_id',
        'arial_report_provider',
        'project_type_id',
        'street',
        'city',
        'state',
        'country',
        'county',
        'zip_code',
        'latitude',
        'longitude',
        'harvester_data',
        //'estateed',
        //'atom',
        'secondary_contacts',
        'relationship_with_property',
        'lead_source_ids',
        'lead_source_people_id',
        'full_address',
        'sub_division',
        'gate_code',
        'is_maximus_claim',
        'is_policy_holder',
        'rr_order_id',
        'rr_order_expected_at',
        'rr_order_placed_at',
        'rr_response',
        'rr_gallery',
        'is_gated',
        'formatted_address'
    ];

    protected $casts = [
        'raw_data' => 'array',
        'enhanced_data' => 'array',
        'secondary_contacts' => 'array',
        'harvester_data' => 'json',
        'rr_response' => 'array',
        'rr_gallery' => 'array',
        //'connections' => 'array',
    ];

//    public function address()
//    {
//        return $this->morphOne(Address::class, 'addressable')->withDefault();
//    }

    public function claim()
    {
        return $this->hasOne(MaximusClaim::class);
    }

    public function lead(){
        return $this->hasOne(Lead::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

//    public function getFullAddressAttribute()
//    {
//        return $this->address ? implode(', ', $this->address->only(['street', 'city', 'state'])) : '';
//    }
    public function structures()
    {
        return $this->hasMany(Structure::class);
    }

    public function quickbids()
    {
        return $this->hasMany(QuickBid::class);
    }

    public function media()
    {
        return $this->hasMany(PropertyMedia::class, 'property_id');
    }
    public function getProjectTypeAttribute()
    {
        return app('constants')->getName($this->project_type_id, 'project_types');
    }
    public function project(){
        return $this->belongsTo(Project::class,'project_id');
    }
    public function zeroProject(){
        return $this->belongsTo(Project::class,'project_id')->where('is_project',0);
    }

    public function oneProject(){
        return $this->belongsTo(Project::class,'project_id')->where('is_project',1);
    }

    public function getRelationshipName()
    {
        return app('constants')->getName($this->property->relationship_id);
    }
    public function appointments(){
        return $this->hasMany(Appointment::class);
    }
    // public function appointment(){
    //     return $this->hasOne(Appointment::class);
    // }

    /************* Attributes **************/
    public function getTitleAttribute()
    {
        return $this->customer->last_name .'-'. $this->city;
    }

    public function lastStructure(){
        return $this->hasOne(Structure::class)->orderBy('created_at', 'desc');
    }

    public function lastAppointment(){
        return $this->hasOne(Appointment::class)->orderBy('created_at', 'desc');
    }

    public function subDivision(){
        return $this->belongsTo(SubDivision::class, 'sub_division');
    }
}
