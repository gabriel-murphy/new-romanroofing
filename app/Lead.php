<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;
    protected $fillable = ['first_name','last_name','referral_name','referral_id','added_by','phone','phone_type','extension','country_code','email','email_type', 'property_id'];

        protected $appends = ['name'];

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function setAmountAttribute($value)
    {
        return $this->attributes['amount'] = $value;
    }
    public function getAmountAttribute()
    {
        return $this->amount;
    }

    public function addedBy(){
        return $this->hasOne(User::class,'id','added_by');
    }

    public function referral(){
        return $this->hasOne(People::class,'id','referral_id');
    }

    public function property(){
        return $this->belongsTo(Property::class,'property_id');
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = titleCase($value);
    }
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = titleCase($value);
    }
    public function setReferalNameAttribute($value)
    {
        $this->attributes['referal_name'] = titleCase($value);
    }
    public function mails()
    {
        return $this->morphMany(Odmail::class, 'mailable');
    }
}
