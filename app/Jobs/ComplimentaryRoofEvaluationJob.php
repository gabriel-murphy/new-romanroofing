<?php

namespace App\Jobs;

use App\Mail\ComplimentaryRoofEvaluationMail;
use App\Mail\ExceptionMail;
use App\Template;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class ComplimentaryRoofEvaluationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $lead, $client;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($lead, $client)
    {
        $this->lead = $lead;
        $this->client = $client;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
                Mail::send(new ComplimentaryRoofEvaluationMail($this->lead, $this->client));
                $this->setComplimentaryRoofEvaluationSmsSetupForCustomer();
    }

    private function setComplimentaryRoofEvaluationSmsSetupForClient()
    {
        $template = Template::type('sms')->key('another_website_opportunity')->first();

        $models['client']     =  $this->client;
        $models['lead']       = $this->lead;
        $gender = _getGender($this->lead->first_name);
        $user_arr = array();
        $user_arr['gender']      =  $gender;
        $models['user']          = (object)$user_arr;
        $template->content = $this->modifications['content'] ?? $template->content;
        $content = _parse_template($template, $models,$template->content);
        info('$content clinet');
        info($content);
        $this->sendSms($content, '+923037509668');
    }

    private function setComplimentaryRoofEvaluationSmsSetupForCustomer(){
        info('sdfsdf');
        $template = Template::type('sms')->key('complimentary_roof_evaluation')->first();
        $models['client']     =  $this->client;
        $models['lead']       = $this->lead;
        $models['lead']['when'] = $this->_getWhenValue();

        $template->content = $this->modifications['content'] ?? $template->content;

        $content = $this->_parse_template($template, $models,$template->content);
        info('content');
        info($this->lead->phone);
        info($content);
        $this->sendSms($content, '+923037509668');
    }

    public function failed(\Exception $exception = null){
        info($exception->getMessage());
        info($exception);

        // Mail::send(new ExceptionMail($exception,'Free Evaluation Job Exception'));
    }

    private function _parse_template($template, $models, $cc ,$subject = false)
    {
        $replace_data = [];
        foreach ($template->placeholders as $placeholder){
            $model = $models[$placeholder->model] ?? null;
            $key = "%%$placeholder->name%%";
            if ($model) $replace_data[$key] = $model->{$placeholder->attribute};
        }
        // info($replace_data);
        $content = $subject ? $template->subject : $cc;
        return strtr($content, $replace_data);
    }

    private function sendSms($content, $phone){

        $app= App::getFacadeRoot();
        $twilio = $app->make('twilio');
        $message =  $twilio->messages
            ->create($phone, [
                "body" => $content,
                "from" => config('global.from_number'),
            ]);
        info('$message');
        info($message);
    }

   private function _getWhenValue()
    {
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        if($dayOfTheWeek === 6)
        {
            $when = 'on Monday';
        }else
        {
            $when = 'Tomorrow';
        }
        return $when;
    }

}
