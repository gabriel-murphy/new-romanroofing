<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Twilio\Rest\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->app->singleton('twilio', function($app)
        {
//            return new Client('AC1740bce0e6d2ab2478bb579c42f55abf','1491566ffe9c2cc6d68e2844227b39de');
            return new Client(config('global.twilio_sid'), config('global.twilio_token'));
        });
    }
}
