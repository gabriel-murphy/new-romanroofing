<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table = 'contacts';
	public $fillable = ['full_name','email_address','telephone','comment'];
}
